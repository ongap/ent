-- phpMyAdmin SQL Dump
-- version 4.3.7
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mer 28 Octobre 2015 à 21:45
-- Version du serveur :  10.0.21-MariaDB-log
-- Version de PHP :  5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `ent`
--

-- --------------------------------------------------------

--
-- Structure de la table `acos`
--

CREATE TABLE IF NOT EXISTS `acos` (
  `id` int(10) NOT NULL,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `aros`
--

CREATE TABLE IF NOT EXISTS `aros` (
  `id` int(10) NOT NULL,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `aros_acos`
--

CREATE TABLE IF NOT EXISTS `aros_acos` (
  `id` int(10) NOT NULL,
  `aro_id` int(10) NOT NULL,
  `aco_id` int(10) NOT NULL,
  `_create` varchar(2) NOT NULL DEFAULT '0',
  `_read` varchar(2) NOT NULL DEFAULT '0',
  `_update` varchar(2) NOT NULL DEFAULT '0',
  `_delete` varchar(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ateliers`
--

CREATE TABLE IF NOT EXISTS `ateliers` (
  `id` int(11) NOT NULL,
  `jour` date NOT NULL,
  `horaire_id` int(11) NOT NULL,
  `salle_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL COMMENT 'animateurs',
  `matiere_id` int(11) NOT NULL,
  `nom` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `nombre_place` int(11) NOT NULL,
  `hebdomadaire` int(11) NOT NULL,
  `periode_id` int(11) DEFAULT NULL,
  `suspendu` tinyint(1) DEFAULT NULL,
  `verrouille` tinyint(1) DEFAULT NULL,
  `atelier_id` int(11) DEFAULT '0',
  `rubrique_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ateliers_divisions`
--

CREATE TABLE IF NOT EXISTS `ateliers_divisions` (
  `id` int(11) NOT NULL,
  `atelier_id` int(11) NOT NULL,
  `division_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2829 DEFAULT CHARSET=utf8;
-- --------------------------------------------------------

--
-- Structure de la table `cake_sessions`
--

CREATE TABLE `cake_sessions` (
  `id` varchar(255) NOT NULL DEFAULT '',
  `data` text,
  `expires` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------

--
-- Structure de la table `creditations`
--

CREATE TABLE IF NOT EXISTS `creditations` (
  `id` int(11) NOT NULL,
  `atelier_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `presence` tinyint(1) NOT NULL,
  `valid` int(11) NOT NULL,
  `commentaire` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `divisions`
--

CREATE TABLE IF NOT EXISTS `divisions` (
  `id` int(11) NOT NULL,
  `nom` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `divisions`
--

INSERT INTO `divisions` (`id`, `nom`) VALUES
(1, 'Animateur');

-- --------------------------------------------------------

--
-- Structure de la table `horaires`
--

CREATE TABLE IF NOT EXISTS `horaires` (
  `id` int(11) NOT NULL,
  `parametre_id` int(11) NOT NULL,
  `heureDebut` time NOT NULL,
  `heureFin` time NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `horaires`
--

INSERT INTO `horaires` (`id`, `parametre_id`, `heureDebut`, `heureFin`) VALUES
(1, 1, '07:30:00', '08:45:00'),
(2, 1, '09:00:00', '12:00:00'),
(3, 1, '12:00:00', '12:30:00'),
(4, 1, '12:30:00', '13:30:00'),
(5, 1, '13:30:00', '15:45:00'),
(6, 1, '15:45:00', '17:15:00'),
(7, 1, '17:15:00', '18:30:00');

-- --------------------------------------------------------

--
-- Structure de la table `inscriptions`
--

CREATE TABLE IF NOT EXISTS `inscriptions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `atelier_id` int(11) NOT NULL,
  `date_inscript` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `validation` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=157 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `matieres`
--

CREATE TABLE IF NOT EXISTS `matieres` (
  `id` int(11) NOT NULL,
  `nom` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `matieres`
--

INSERT INTO `matieres` (`id`, `nom`) VALUES
(1, 'Mathématiques'),
(2, 'Informatique'),
(3, 'Cuisine'),
(4, 'Anglais'),
(5, 'Français'),
(6, 'Allemand'),
(7, 'Espagnol');

-- --------------------------------------------------------

--
-- Structure de la table `parametres`
--

CREATE TABLE `parametres` (
  `id` int(11) NOT NULL,
  `nomEtablissement` varchar(250) DEFAULT NULL,
  `nombreHeureAtelier` int(11) DEFAULT NULL,
  `nombreAtelierAfficher` int(11) DEFAULT NULL,
  `nomImageLogo` varchar(250) DEFAULT NULL,
  `nombreNewAffiche` int(11) DEFAULT NULL,
  `maintenance` tinyint(1) NOT NULL DEFAULT '0',
  `desinEleve` tinyint(1) NOT NULL,
  `placeEleve` tinyint(1) NOT NULL,
  `dateAtlEleve` tinyint(1) NOT NULL,
  `comAtlEleve` tinyint(1) NOT NULL DEFAULT '0',
  `needPhone` tinyint(1) NOT NULL DEFAULT '0',
  `modules` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `parametres`
--

INSERT INTO `parametres` (`id`, `nomEtablissement`, `nombreHeureAtelier`, `nombreAtelierAfficher`, `nomImageLogo`, `nombreNewAffiche`, `maintenance`, `desinEleve`, `placeEleve`, `dateAtlEleve`, `comAtlEleve`, `needPhone`, `modules`) VALUES
(1, 'ONGAP', 50, 30, 'logo.png', 5, 0, 1, 1, 1, 0, 0, '');
-- --------------------------------------------------------

--
-- Structure de la table `periodes`
--

CREATE TABLE IF NOT EXISTS `periodes` (
  `id` int(11) NOT NULL,
  `debut` DATETIME NOT NULL,
  `fin` DATETIME NOT NULL,
  `nom` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Actualités`
--

CREATE TABLE IF NOT EXISTS `actualites` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `titre` varchar(255) NOT NULL,
  `contenu` text NOT NULL,
  `created` datetime NOT NULL,
  `visibilite` tinyint(1) NOT NULL DEFAULT '1',
  `expiration_date` date NOT NULL DEFAULT '0000-00-00',
  `suspendre` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL,
  `nom` varchar(20) NOT NULL,
  `nom_code` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `roles`
--

INSERT INTO `roles` (`id`, `nom`, `nom_code`) VALUES
(1, 'WebMaster', 'WEBMASTER'),
(2, 'Administrateur', 'ADMIN'),
(3, 'Elève', 'ELEVE'),
(4, 'Professeur', 'PROF'),
(5, 'Vie Scolaire', 'VIE_SCOLAIRE'),
(6, 'Vacataire', 'VACATAIRE');

-- --------------------------------------------------------

--
-- Structure de la table `rubriques`
--

CREATE TABLE `rubriques` (
  `id` int(11) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `nom_canonique` varchar(100) NOT NULL,
  `position` int(11) DEFAULT '0',
  `label_gestion` varchar(100) NOT NULL,
  `label_gestion_inscription` varchar(100) NOT NULL,
  `label_suivi` varchar(100) NOT NULL,
  `label_inscription` varchar(100) NOT NULL,
  `label_recapitulation` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `rubriques`
--

INSERT INTO `rubriques` (`id`, `nom`, `nom_canonique`, `position`, `label_gestion`, `label_gestion_inscription`, `label_suivi`, `label_inscription`, `label_recapitulation`) VALUES
(1, 'Ateliers', 'atelier', 1, 'Gestion des ateliers', 'Gestion des inscriptions', 'Suivi des ateliers', 'Inscription', 'Récapitulatif inscription'),
(2, 'Périscolaires', 'periscolaire', 1, 'Gestion des ateliers périscolaires', 'Gestion des inscriptions périscolaires', 'Suivi des ateliers périscolaires', 'Inscription ', 'Récapitulatif inscription');

-- --------------------------------------------------------

--
-- Structure de la table `salles`
--

CREATE TABLE IF NOT EXISTS `salles` (
  `id` int(11) NOT NULL,
  `nom` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role_id` int(11) NOT NULL,
  `division_id` int(11) NOT NULL,
  `nbHeure` int(11) NOT NULL DEFAULT '0',
  `nbConnexion` int(11) NOT NULL DEFAULT '0',
  `civilite` varchar(5) NOT NULL,
  `phone` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1235 DEFAULT CHARSET=utf8;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `acos`
--
ALTER TABLE `acos`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `aros`
--
ALTER TABLE `aros`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `aros_acos`
--
ALTER TABLE `aros_acos`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `ARO_ACO_KEY` (`aro_id`,`aco_id`);

--
-- Index pour la table `ateliers`
--
ALTER TABLE `ateliers`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `ateliers_divisions`
--
ALTER TABLE `ateliers_divisions`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `cake_sessions`
--
ALTER TABLE `cake_sessions`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `creditations`
--
ALTER TABLE `creditations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `divisions`
--
ALTER TABLE `divisions`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `horaires`
--
ALTER TABLE `horaires`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `inscriptions`
--
ALTER TABLE `inscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `matieres`
--
ALTER TABLE `matieres`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `parametres`
--
ALTER TABLE `parametres`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `periodes`
--
ALTER TABLE `periodes`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `actualites`
--
ALTER TABLE `actualites`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `rubriques`
--
ALTER TABLE `rubriques`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `salles`
--
ALTER TABLE `salles`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `acos`
--
ALTER TABLE `acos`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `aros`
--
ALTER TABLE `aros`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `aros_acos`
--
ALTER TABLE `aros_acos`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `ateliers`
--
ALTER TABLE `ateliers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `ateliers_divisions`
--
ALTER TABLE `ateliers_divisions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `creditations`
--
ALTER TABLE `creditations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `divisions`
--
ALTER TABLE `divisions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT pour la table `horaires`
--
ALTER TABLE `horaires`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `inscriptions`
--
ALTER TABLE `inscriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `matieres`
--
ALTER TABLE `matieres`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `parametres`
--
ALTER TABLE `parametres`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `periodes`
--
ALTER TABLE `periodes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `actualites`
--
ALTER TABLE `actualites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `rubriques`
--
ALTER TABLE `rubriques`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `salles`
--
ALTER TABLE `salles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1235;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
