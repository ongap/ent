﻿Espace Numérique de Travail
===========================

L'ONGAP ouvre le code source de son application web modulaire reposant sur un ENT (espace numérique de travail).

L'ENT comprend les fonctionnalités suivantes :
 * La gestion d'un fil d'actualité privé.
 * Un gestionnaire d'utilisateurs adapté au milieu scolaire mais aussi aux communes.
 * Le paramètrage complet de l'ENT avec les horaires, les salles, les classes etc.
 * Gestion des ateliers avec réservation de salle, système d'appel, validations etc.

**IMPORTANT**
Ce projet n'est plus maintenu par l'équipe ONGAP. Ce dépôt est une notice pour installer le site sur son propre serveur.

Pré-requis
----------

 * PHP 5.6
 * MySQL 5.5 ou MariaDB 10.3
 * Apache 2.2 ou 2.4

Apache
------

Exemple de configuration :

```
<VirtualHost *:80>
    ServerName www.accueil-periscolaire.fr
	
    ServerAdmin contact@accueil-periscolaire.fr
    DocumentRoot "/var/www/html/ent/app/webroot"
	
    ErrorLog "error.log"
    CustomLog "access.log" combined
    <Directory "/var/www/html/ent/app/webroot">
        AllowOverride All
        Require all granted
    </Directory>
	
</VirtualHost>

```

MySQL
-----

Importer tout simplement le base.sql dans votre base mysql.

Site et configuration
---------------------

Copier les fichiers de configurations distribués.


```bash
cd /var/www/html/ent
cp app/Config/core.dist.php app/Config/core.php
cp app/Config/database.dist.php app/Config/database.php
```

Dans app/Config/core.php, changer la valeur du *salt* (actuellement 'changeMoiSilTeplait') par une chaine d'au moins 40 caractères aléatoires.
Changer également la valeur du *cipherSeed* (actuellement '123456789') par une suite d'au moins 29 chiffres aléatoires.

Dans app/Config/database.php, remplir le tableau (à la fin du fichier) avec les informations de votre base de données.

Enfin, assurez-vous que le dossier app/tmp soit accésible en écriture depuis le serveur. Si vous utilisez un herbergement mutualisé, vous ne devriez pas avoir de soucis.

Initialisation du site
----------------------

Vous pouvez maintenant vous rendre sur la page d'accueil de votre site et suivre les indications. Ces indications permettent de créer le premier compte et de choisir les modules.

