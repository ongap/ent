<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<?php
					$LabelOptions = array("class" => "col-lg-3 control-label");
					$options = array
					(
						"class" => "form-horizontal",
						"inputDefaults" => array
						(
							"class" => "form-control",
							"div" => array("class" => "form-group"),
							"label" => $LabelOptions,
							"between" => "<div class='col-lg-9'>",
							"after" => "</div>",
							'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block')),
							"format" => array('before', 'label', 'between', 'input', 'error', 'after')
						),
					);
					echo $this->Form->create("Periode", $options);
				?>
				<fieldset>
					<legend>
						Modifier une Période :
					</legend>
					<?php
						$options = array
						(
							"placeholder" => "Nom",
							"label" => array_merge($LabelOptions, array("text" => "Nom"))
						);
						if ($this->Form->error("nom")) { $options["div"] = "form-group has-error"; }
						echo $this->Form->input("nom", $options);

						$options = array
						(
							"class" => "form-control inputDatePicker",
							"type" => "text",
							"between" => "<div class='col-lg-9 toEmpty'>",
							"label" => array_merge($LabelOptions, array("text" => "Première date")),
							"dateFormat" => "YMD"
						);
						if ($this->Form->error("debut")) { $options["div"] = "form-group has-error"; }
						echo $this->Form->input("debut", $options);

						$options = array
						(
							"class" => "form-control inputDatePicker",
							"type" => "text",
							"between" => "<div class='col-lg-9 toEmpty'>",
							"label" => array_merge($LabelOptions, array("text" => "Dernière date")),
							"dateFormat" => "YMD"
						);
						if ($this->Form->error("fin")) { $options["div"] = "form-group has-error"; }
						echo $this->Form->input("fin", $options);

						$options = array
						(
							"class" => "btn btn-success col-xs-12 col-sm-12 ",
							"between" => "<div class='col-lg-3 col-lg-offset-9'>",
							"after" => "</div>",
							"label" => false,
							"type" => "button"
						);
						echo $this->Form->input("Modifier", $options);

						echo $this->Form->end();
					?>
				</fieldset>
			</div>
			<script>
				$(document).ready(function() {
					$.datepicker.setDefaults( $.datepicker.regional[ "fr" ] );
					$(".inputDatePicker").datepicker({dateFormat: "yy-mm-dd"});
					tinymce.init({
						selector: "textarea",
						plugins: [
						"advlist autolink lists link image charmap print preview anchor",
						"searchreplace visualblocks code fullscreen",
						"insertdatetime media table contextmenu paste"
						],
						toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
					});
					$(".toEmpty").contents().filter(function(){ return this.nodeType === 3; }).remove();
				});
			</script>
		</div>
	</div>
</div>