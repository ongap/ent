<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<fieldset>
					<legend>
						Paramètres :
					</legend>
					<?php echo $this->element('menu.parametres'); ?>
					<hr/>

					<?= $this->Html->link(
							'Ajouter',
							array('controller' => 'periodes', 'action' => 'add'),
							array('class' => 'btn btn-success pull-right', 'style' => 'position:relative;top:-8px;')
						);
					?>
					<h4>Configuration des périodes : </h4>

					<table class="table table-bordered table-striped table-hover table-condensed">
						<thead>
							<?= $this->Html->TableHeaders(array('Nom', 'Début', 'Fin', array('Action' => array('class' => 'text-center')))) ?>
						</thead>
						<tbody>
							<?php
								$periodes = array();
								foreach ($toutesLesPeriodes as $key => $periode) {

									$btn  = $this->Html->link ( 
										"Modifier",
										array ( 'controller' => 'periodes', 'action' => 'update', $periode['Periode']['id']),
										array ( 'class' => 'btn btn-success btn-sm', 'escape' => false)
									);
									$btn .= " ";
									$btn .= $this->Html->link ( 
										"Supprimer",
										array ( 'controller' => 'periodes', 'action' => 'remove', $periode['Periode']['id']),
										array ( 'class' => 'btn btn-danger btn-sm', 'escape' => false)
									);

									$periodes[] = array(
										$periode['Periode']['nom'],
										$this->Time->format($periode['Periode']['debut'], "%d %B %Y"),
										$this->Time->format($periode['Periode']['fin'], "%d %B %Y"),
										array($btn, array('style' => 'vertical-align:middle', 'class' => 'text-center')),
									);
								}

								echo $this->Html->TableCells($periodes);
							?>
						</tbody>
					</table>
				</fieldset>
			</div>
		</div>
	</div>
</div>
