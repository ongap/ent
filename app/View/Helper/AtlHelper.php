<?php
App::uses('AppHelper', 'View/Helper');

/**
 * La classe helper pour générer les tableaux et calendriers des ateliers.
 */
class AtlHelper extends AppHelper {

	/**
	 * @var string|array Helper(s) utilisé(s)
	 */
	public $helpers = array('Html', 'Time', 'Text');

	/**
	 * Fonction raccourci pour couper des chaines de caractères
	 *
	 * @param string $texte texte à couper 
	 * @param int $nombreCaractere à combien la fonction doit couper
	 * @return La chaine coupé
	 */
	public function truncate($texte, $nombreCaractere = 16) {
		return $this->Text->truncate(
			$texte,
			$nombreCaractere,
			array(
				'ellipsis' => '...',
				'exact' => true,
				'html' => true
			)
		);
	}

	/**
   	 * Générer une info bulle
   	 *
   	 * @param string $texte texte à afficher
   	 * @return le code html avec l'info bulle
   	 */
	public function infoBulle($texte) {
		return '<span class="tooltipspan" data-toggle="tooltip" title="'.strip_tags(str_replace(array("'", "\""), array("&apos;", "&quot;"),$texte)).'">'.$this->truncate($texte).'</span>';
	}

	/**
	 * Créer l'en-tête du tableau pour les ateliers pour la navigation
	 *
	 * @param string $action action du controller.
	 * @param mixed $valeur lié à l'action du controller.
	 * @param int $annee
	 * @param int $numSemaine numéro de semaine selectionné
	 * @return Code html du tableau généré
	 */
	public function enteteTableau($action, $valeur, $annee, $numSemaine){
		$rubrique 	= $this->request->params['rubrique'];
		$styleBtn 	= array('class' => 'btn btn-primary btn-xs', 'style' => 'width:100%;font-weight:800;');
		$urlLien  	= array (
						'controller' => 'ateliers',
						'action' => $action,
						'rubrique' => $rubrique,
						$valeur,
						$annee,
						$numSemaine-1
					);

		$dto = new DateTime();
		$dto->setISODate($annee, $numSemaine);
		$dateDebut = $dto->format('d M Y');
		$dto->modify('+6 days');
		$dateFin = $dto->format('d M Y');
		$texteCentral = "Ateliers du lundi ".$dateDebut." au dimanche ".$dateFin;

		$lienAvant  = $this->Html->link('«', $urlLien, $styleBtn);
		$urlLien[2] = $numSemaine+1;
		$lienApres  = $this->Html->link('»', $urlLien, $styleBtn);

		//Génération du tableau en-tête
		$tableauCommande  = "<table class=\"table table-striped table-bordered table-hover table-condensed\" style=\"table-layout:fixed; margin-bottom:0px;\">\n";
		$tableauCommande .= $this->Html->tableHeaders ( array (
			$lienAvant,
			array($texteCentral => array('colspan' => '7', 'class' => 'text-center')),
			$lienApres
		));
		$tableauCommande .= '</table>';

		return $tableauCommande;
	}

	/**
	 * Créer le tableau qui liste les ateliers
	 *
	 * @param array $ateliers un tableau avec les ateliers
	 * @return Code html du tableau généré
	 */
	public function listeTableau($ateliers)	{
		$tableau  = "<table class=\"table table-bordered table-striped table-hover table-condensed\">\n";
		$tableau .= "	<thead\">\n";
		$tableau .= $this->Html->tableHeaders ( array (
						'Nom',
						'Matière',
						'Description',
						'Date et Heure',
						'Animateur',
						'Lieux',
						'Classe(s)',
						'Places',
						array('Actions' => array('class' => 'text-center'))
					));
		$tableau .= "	</thead\">\n";
		$tableau .= "	<tbody\">\n";
		$tableau .= $this->Html->tableCells ($ateliers);
		$tableau .= "	</tbody\">\n";
		$tableau .= "</table>\n";

		return $tableau;
	}

	/**
	 * Générer un bouton simple
	 * @param string $texte contenu afficher du bouton
	 * @param array() $options tableau d'attributs pour le bouton
	 */
	public function simpleBouton($texte, $options = array()) {
		$button  = '<button type="button"';
		foreach ($options as $attribute => $value) {
			$button .= $attribute.'="'.$value.'"';
		}
		$button .= '>';
		$button .= $texte;
		$button .= '</button>';
		return $button;
	}

	/**
	 * Génére un item d'un menu dropdown
	 *
	 * @param string $action nom de l'action du controller à pointer
	 * @param string $texte texte sur le bouton
	 * @param int $idAtelier l'id de l'atelier
	 * @param string $css nom de la class CSS à utiliser
	 * @return string une entrée li
	 */
	public function itemMenu($action, $texte, $idAtelier, $css = 'info') {
		$rubrique 	= $this->request->params['rubrique'];

		$item  = '<li class="list-group-item-'.$css.'">';
		$item .= $this->Html->link ( 
			$texte,
			array (
				'controller' => 'ateliers',
				'rubrique' => $rubrique,
				'action' => $action,
				$idAtelier
			),
			array('escape' => false, 'title' => $texte));
		$item .= '</li>';
		return $item;
	}


	/**
	 * Génére une ligne pour la liste d'atelier
	 *
	 * @param array $atelier un atelier
	 * @param array $btn le bouton à afficher en bout de ligne
	 * @return array de la ligne pour être inséré dans le tableau
	 */
	public function genererLigne($atelier, $btn, $options = array()){
		$nomJour = array('Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi');

		if (isset($options['style'])) {
			$options['style'] .= 'vertical-align:middle;';
		}
		else{
			$options['style']  = 'vertical-align:middle;';
		}

		$optionsBtn = $options;

		if (isset($optionsBtn['class'])) {
			$optionsBtn['class'] .= 'text-center';
		}
		else{
			$optionsBtn['class']  = 'text-center';
		}

		//Écriture de la date et de l'heure avec les labels pour les périodes
		$dateHeure = $nomJour[$this->Time->format( $atelier['Date et Heure'], '%w')].' '.$this->Time->format($atelier['Date et Heure'], '%d/%m/%Y %H:%M');
		if ($atelier['Periodique'] != false) {
			$dateHeure .= ' <span class="label label-primary">'.$atelier['PositionRep'].'</span> <br>';
			$dateHeure .= ' <span class="label label-default">'.$atelier['Periodique'].'</span>';
		}

		$ligne = array (
			array($this->infoBulle($atelier['Nom']), $options),
			array($this->infoBulle($atelier['Matière']), $options),
			array($this->infoBulle($atelier['Description']), $options),
			array($dateHeure, $options),
			array($atelier['Animateur'], $options),
			array($atelier['Lieu'], $options),
			array($this->infoBulle($atelier['Classes']), $options),
			array($atelier['Inscriptions'].'/'.$atelier['Places'], $options),
			array($btn, $optionsBtn),
		);
		return $ligne;
	}

}