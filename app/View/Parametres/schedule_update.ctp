<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <fieldset>
          <legend>
            Modifier l'horaire :
          </legend>
          <?php
          $LabelOptions = array("class" => "col-lg-3 control-label");
          $options = array (
            "class" => "form-horizontal",
            "inputDefaults" => array (
              "class" => "form-control",
              "div" => array("class" => "form-group"),
              "label" => $LabelOptions,
              "between" => "<div class='col-lg-9'>",
              "after" => "</div>",
              'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block')),
              "format" => array('before', 'label', 'between', 'input', 'error', 'after')
              ),
            );
          echo $this->Form->create("Horaire", $options);

          $options = array (
            "type" => "text",
            "placeholder" => "**h**",
            "value" => $this->Time->format($this->request->data['Horaire']['heureDebut'], '%Hh%M'),
            "label" => array_merge($LabelOptions, array("text" => "Heure de début"))
            );
          if ($this->Form->error("heureDebut")) { $options["div"] = "form-group has-error"; }
          echo $this->Form->input("heureDebut", $options);

          $options = array (
            "type" => "text",
            "placeholder" => "**h**",
            "value" => $this->Time->format($this->request->data['Horaire']['heureFin'], '%Hh%M'),
            "label" => array_merge($LabelOptions, array("text" => "Heure de fin"))
            );
          if ($this->Form->error("heureFin")) { $options["div"] = "form-group has-error"; }
          echo $this->Form->input("heureFin", $options);

          $options = array (
            "class"  => "btn btn-success",
            "div"    => array("class" => "form-group"),
            "before" => "<div class='col-lg-3 col-lg-offset-3'>",
            "after"  => "</div>",
            "label"  => false,
            );
            ?>

            <?= $this->Form->submit("Modifier", $options) ?>

            <?= $this->Form->end() ?>

          </fieldset>
        </div>
      </div>
    </div>
  </div>