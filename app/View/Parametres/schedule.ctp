<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <fieldset>
          <legend>
            Paramètres :
          </legend>
          <?php echo $this->element('menu.parametres'); ?>
          <hr/>

          <?php
          $LabelOptions = array("class" => "col-lg-3 control-label");
          $options = array (
            "class" => "form-horizontal",
            "inputDefaults" => array (
              "class" => "form-control",
              "div" => array("class" => "form-group"),
              "label" => $LabelOptions,
              "between" => "<div class='col-lg-9'>",
              "after" => "</div>",
              'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block')),
              "format" => array('before', 'label', 'between', 'input', 'error', 'after')
              ),
            );
          echo $this->Form->create("Horaire", $options);

          $options = array (
            "type" => "text",
            "placeholder" => "**h**",
            "label" => array_merge($LabelOptions, array("text" => "Heure de début"))
            );
          if ($this->Form->error("heureDebut")) { $options["div"] = "form-group has-error"; }
          echo $this->Form->input("heureDebut", $options);

          $options = array (
            "type" => "text",
            "placeholder" => "**h**",
            "label" => array_merge($LabelOptions, array("text" => "Heure de fin"))
            );
          if ($this->Form->error("heureFin")) { $options["div"] = "form-group has-error"; }
          echo $this->Form->input("heureFin", $options);

          $options = array (
            "class"  => "btn btn-success",
            "div"    => array("class" => "form-group"),
            "before" => "<div class='col-lg-3 col-lg-offset-3'>",
            "after"  => "</div>",
            "label"  => false,
          );
          ?>

          <?= $this->Form->submit("Ajouter", $options) ?>

          <?= $this->Form->end() ?>

          <hr/>

          <table class="table table-bordered table-striped table-hover table-condensed">
            <thead>
              <tr>
                <th class="text-center">Heure de début</th>
                <th class="text-center">Heure de fin</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($horaires as $key => $horaire): ?>
                <tr>
                  <td style="vertical-align:middle" class="text-center">
                    <?= $this->Time->format($horaire['Horaire']['heureDebut'], '%Hh%M') ?>
                  </td>
                  <td style="vertical-align:middle" class="text-center">
                    <?= $this->Time->format($horaire['Horaire']['heureFin'], '%Hh%M') ?>
                  </td>
                  <td style="vertical-align:middle" class="text-center">
                    <?= $this->Html->link("Modifier", array('controller' => 'parametres', 'action' => 'scheduleUpdate', $horaire['Horaire']['id']), array('class' => 'btn btn-success btn-sm', 'escape' => false)); ?>
                    <?= $this->Html->link("Supprimer", array('controller' => 'parametres', 'action' => 'scheduleRemove', $horaire['Horaire']['id']), array('class' => 'btn btn-danger btn-sm', 'escape' => false), "Voulez-vous vraiment supprimer cet horaire ?"); ?>
                  </td>
                </tr>
              <?php endforeach ?>
            </tbody>
          </table>
        </fieldset>
      </div>
    </div>
  </div>
</div>
