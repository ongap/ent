<?php
	$role = $this->Session->read('Auth.User.Role.nom_code');
?>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<fieldset>
					<legend>
						Paramètres :
					</legend>

					<?php echo $this->element('menu.parametres'); ?>

					<hr/>
					<?php if ($role == 'WEBMASTER'): ?>
						<?php if ($maintenance == true): ?>
							<?= $this->Html->link(
								'Désactiver le mode maintenance',
								array('controller' => 'parametres', 'action' => 'toggleMaintenance'),
								array('class' => "btn btn-warning")
							); ?>
						<?php else: ?>
							<?= $this->Html->link(
								'Activer le mode maintenance',
								array('controller' => 'parametres', 'action' => 'toggleMaintenance'),
								array('class' => "btn btn-info")
							); ?>
						<?php endif ?>
					<?php endif ?>
					<hr/>
					<?php
					$LabelOptions = array("class" => "col-lg-3 control-label");
					$options = array
					(
						"class" => "form-horizontal",
						"action" => "update",
						"inputDefaults" => array
						(
							"class" => "form-control",
							"div" => array("class" => "form-group"),
							"label" => $LabelOptions,
							"between" => "<div class='col-lg-9'>",
							"after" => "</div>",
							'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block')),
							"format" => array('before', 'label', 'between', 'input', 'error', 'after')
							),
						);
					echo $this->Form->create("Parametre", $options);

					$options = array
					(
						"placeholder" => "Nom de l'établissement",
						"label" => array_merge($LabelOptions, array("text" => "Nom de l'établissement"))
						);
					if ($this->Form->error("nomEtablissement")) { $options["div"] = "form-group has-error"; }
					echo $this->Form->input("nomEtablissement", $options);

					echo $this->Form->input('id', array('type' => 'hidden'));

					$options = array
					(
						"class" => "btn btn-success col-xs-12 col-sm-12 ",
						"before" => "<div class='col-lg-3 col-lg-offset-9'>",
						"after" => "</div>"
						);
					echo $this->Form->submit("Modifier", $options);

					echo $this->Form->end();
					?>
				</fieldset>
			</div>
		</div>
	</div>
</div>