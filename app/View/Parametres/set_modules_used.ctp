<div class="panel panel-default">
 <div class="panel-body">
    <?= $this->Session->flash('flash', array('element' => 'failure')); ?>

    <?php if (isset($supprimer) && $supprimer != ""): ?>
      Fichier à supprimer : 
      <pre>
        <?= $supprimer ?>
      </pre>
    <?php endif ?>

    <?php if (isset($exclure) && $exclure != ""): ?>
      Fichier à exclure : 
      <pre>
        <?= $exclure ?>
      </pre>
    <?php endif ?>


    <?php if (!(isset($supprimer) || isset($exclure))): ?>

    <?php
  		$LabelOptions = array("class" => "col-lg-3 control-label");
  		$options = array
  		(
  			"class" => "form-horizontal",
  			"inputDefaults" => array
  			(
  				"class" => "form-control",
  				"div" => array("class" => "form-group"),
  				"label" => $LabelOptions,
  				"between" => "<div class='col-lg-9'>",
  				"after" => "</div>",
  				'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block')),
  				"format" => array('before', 'label', 'between', 'input', 'error', 'after')
  			),
  		);
  		echo $this->Form->create("Parametre", $options);
    ?>
     <fieldset>
      <legend>
        Choix des modules :
      </legend>
      <?php
       $options = array
       (
        "label" => array_merge($LabelOptions, array("text" => "Modules")),
        "multiple" => true,
        "after" => "<p class='help-block'>CTRL enfoncé pour selectionner plusieurs modules</p></div>"
       );
       if ($this->Form->error("modules")) { $options["div"] = "form-group has-error"; }
       echo $this->Form->input("modules", $options);
      ?>
      <div class="form-group">
       <?php
        $options = array
        (
         "class" => "btn btn-success col-xs-12 col-sm-12 ",
         "before" => "<div class='col-lg-3 col-lg-offset-3'>",
         "after" => "</div>"
        );
        echo $this->Form->submit("Valider", $options);
       ?>
      </div>
     </fieldset>
     <?= $this->Form->end(); ?>

    <?php else: ?>

    <?= $this->Html->link("Continuer", array('controller' => 'parametres', 'action' => 'genererPermission'), array('class' => 'btn btn-info btn-sm', 'escape' => false)); ?>

    <?php endif ?>



 </div>
</div>