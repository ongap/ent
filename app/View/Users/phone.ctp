<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <?php
        echo $this->Session->flash('flash', array('element' => 'failure'));

        $LabelOptions = array("class" => "col-lg-3 control-label");
        $options = array
        (
          "url"     => array('controller' => 'users', 'action' => 'phone'),
          "class"   => "form-horizontal",
          "type"    => "put",
          "inputDefaults" => array
          (
            "class"   => "form-control",
            "div"     => array("class" => "form-group"),
            "label"   => $LabelOptions,
            "between" => "<div class='col-lg-9'>",
            "after"   => "</div>",
            'error'   => array('attributes' => array('wrap' => 'span', 'class' => 'help-block')),
            "format"  => array('before', 'label', 'between', 'input', 'error', 'after')
          ),
        );
        echo $this->Form->create("User", $options);
        ?>
        <fieldset>
          <legend>
            Profil :
          </legend>
          <?php
          $options = array
          (
            "placeholder" => "06.00.00.00.00",
            "label" => array_merge($LabelOptions, array("text" => "Votre numéro de téléphone"))
          );
          if ($this->Form->error("phone")) { $options["div"] = "form-group has-error"; }
          echo $this->Form->input("phone", $options);
          ?>

          <div class="form-group col-lg-12">
            <em>vous pouvez en indiquer plusieurs en séparant par une virgule</em><br>
            Ces informations permettent de vous contacter en cas d'urgence.
          </div>

          <div class="form-group col-lg-12">
            <?php
            $options = array
            (
              "class" => "btn btn-success col-xs-12 col-sm-12 ",
              "before" => "<div class='col-lg-3 col-lg-offset-3'>",
              "after" => "</div>"
            );
            echo $this->Form->submit("Modifier", $options);     
            ?>
          </div>


        </fieldset>
        <?= $this->Form->end(); ?>
      </div>
    </div>
  </div>
</div>