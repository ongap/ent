<?php
	$role = $this->Session->read('Auth.User.Role.nom_code');
?>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<fieldset>
					<legend>
						Changement de classe :
					</legend>

					Transférer les élèves de leur classe vers une autre : <br>

					<?php
					$classes = array_merge(array(0 => "Ne rien faire"), $retourBDD);
					$classes[] = "Vider la classe";

					$LabelOptions = array("class" => "col-lg-3 control-label");
					$options = array(
						"class" => "form-horizontal",
						"action" => "changeClasse",
						"inputDefaults" => array(
							"class" => "form-control",
							"div" => array("class" => "form-group"),
							"label" => $LabelOptions,
							"between" => "<div class='col-lg-9'>",
							"after" => "</div>",
							'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block')),
							"format" => array('before', 'label', 'between', 'input', 'error', 'after')
						),
					);
					echo $this->Form->create("User", $options);

					$attributes['legend'] = false;
					$options_select['options'] = $classes;
					$options_select['label'] = array("class" => "col-lg-3 control-label");
					$options_select['default'] = 'Vider la classe';
					foreach ($retourBDD as $key => $value) {
						$options_select['label']["text"] = $value;
						echo $this->Form->input('Division.'.$key, $options_select, $attributes);
					}

					$options = array(
						"class" => "btn btn-success col-xs-12 col-sm-12 ",
						"between" => "<div class='col-lg-3 col-lg-offset-9'>",
						"after" => "</div>",
						"label" => false,
						"type" => "button"
					);
					echo $this->Form->input("Appliquer", $options);

					echo $this->Form->end();
					?>

				</fieldset>
			</div>
		</div>
	</div>
</div>