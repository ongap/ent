<?php
	$role = $this->Session->read('Auth.User.Role.nom_code');
?>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<fieldset>
					<legend>
						Paramètres :
					</legend>

					<?php echo $this->element('menu.parametres'); ?>

					<hr/>
					<h4>Importer de nouveau utilisateur : </h4>
					<?= $this->Html->link('Importer', array(
						'controller' => 'users',
						'action' => 'import'
					),
					array(
						'class' => 'btn btn-info',
					)); ?>
					<hr/>

					<h4>Passage aux nouvelles classes : </h4>
					<?= $this->Html->link('Changer', array(
						'controller' => 'users',
						'action' => 'changeClasse'
					),
					array(
						'class' => 'btn btn-info',
					)); ?>
					<hr/>

					<h4>Supprimer tous les comptes élèves : </h4>
					<?= $this->Html->link('Supprimer', array(
						'controller' => 'users',
						'action' => 'supprimeCompte',
						'ELEVE'
					),
					array(
						'class' => 'btn btn-info',
					)); ?>

		          <hr/>
		          <h4>Configuration des utilisateurs : </h4>

		          <div class="col-lg-8 col-lg-offset-2">
		            <?php
		              $options = array (
		                "url"    => array('controller' => 'users', 'action' => 'configuration'),
		                "class"  => "form-horizontal",
		                "type"   => "put",
		                "inputDefaults" => array (
		                  "class"  => "",
		                  "div"    => array("class" => "input-group"),
		                  "after"  => "</div>",
		                  "before" => '<span class="input-group-addon">',
		                  "type"    => "checkbox",
		                  "between" => '</span><span class="form-control" aria-label="...">',
		                  "after"   => '</span>',
		                  "format" => array('before', 'input', 'between', 'label', 'after')
		                ),
		              );
		              echo $this->Form->create(false, $options);

		              $options = array (
		                "label"   => "Demander un numéro de téléphone aux comptes enfants",
		                "checked" => ($this->request->data['Parametre']['needPhone']) ? "checked" : null
		              );
		              echo $this->Form->input("needPhone", $options);

		              $options = array (
		                "class" => "btn btn-success col-xs-12 col-sm-12 ",
		                "before" => "<div class='col-lg-3 col-lg-offset-9'>",
		                "after" => "</div>"
		              );
		              echo $this->Form->submit("Modifier", $options);

		              echo $this->Form->end();
		            ?>
		            
		          </div>


				</fieldset>
			</div>
		</div>
	</div>
</div>