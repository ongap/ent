<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-body">

				<?php if (count($atelierFound) > 0): ?>
					Cette utilisateur est assigné à un ou plusieurs ateliers. Ces ateliers seront égalements supprimés, et les élèves seront alors désinscrits<br>
					<?php foreach ($atelierFound as $key => $unAtelier): ?>
						<ul>
							<li>
								<?= $unAtelier["Atelier"]["nom"] ?> (<?= count($unAtelier["Inscription"]) ?>/<?= $unAtelier["Atelier"]["nombre_place"] ?> places)
								<?= $this->Html->link('voir', array('controller' => 'ateliers', 'action' => 'gestion', 0, 0, 0, 0, $unAtelier["Atelier"]["id"]), array('class' => 'btn btn-info btn-xs')); ?>
							</li>
						</ul>
					<?php endforeach ?>
				<?php endif ?>

				<?php if (count($AtualiteFound) > 0): ?>
			    Cette utilisateur à publié une ou plusieures actualités. Ces actualitées seront égalements supprimées<br>
					<?php foreach ($AtualiteFound as $key => $uneAtualite): ?>
						<ul>
							<li>
								<?= $uneAtualite["Actualite"]["titre"] ?>
								<?= $this->Html->link('voir', array('controller' => 'actualites', 'action' => 'update', $uneAtualite["Actualite"]["id"]), array('class' => 'btn btn-info btn-xs')); ?>
							</li>
						</ul>
					<?php endforeach ?>
				<?php endif ?>

				Etes-vous sure de vouloir supprimer cette utilisateur ?
				<?= $this->Html->link('Oui, tout suppimer maintenant', array('controller' => 'users', 'action' => 'remove', $id, true), array('class' => 'btn btn-danger pull-right', 'style' => 'position:relative;top:-8px;')); ?>

			</div>
		</div>
	</div>
</div>