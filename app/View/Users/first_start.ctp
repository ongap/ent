<div class="panel panel-default">
 <div class="panel-body">
  <?php
    echo $this->Session->flash('flash', array('element' => 'failure'));

   $LabelOptions = array("class" => "col-lg-3 control-label");
   $options = array
   (
    "class" => "form-horizontal",
    "inputDefaults" => array
    (
     "class" => "form-control",
     "div" => array("class" => "form-group"),
     "label" => $LabelOptions,
     "between" => "<div class='col-lg-9'>",
     "after" => "</div>",
     'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block')),
     "format" => array('before', 'label', 'between', 'input', 'error', 'after')
    ),
   );
   echo $this->Form->create("User", $options);
  ?>
   <fieldset>
    <legend>
      Création du compte WebMaster :
    </legend>
    <?php
     $options = array
     (
      "label" => array_merge($LabelOptions, array("text" => "Civilité")),
      "options" => array("M" => "M", "Mme" => "Mme"),
      "empty" => "(choisissez)"
     );
     echo $this->Form->input('civilite', $options);
     $options = array
     (
      "placeholder" => "Nom",
      "label" => array_merge($LabelOptions, array("text" => "Nom"))
     );
     if ($this->Form->error("nom")) { $options["div"] = "form-group has-error"; }
     echo $this->Form->input("nom", $options);
     $options = array
     (
      "placeholder" => "Prénom",
      "label" => array_merge($LabelOptions, array("text" => "Prénom"))
     );
     if ($this->Form->error("prenom")) { $options["div"] = "form-group has-error"; }
     echo $this->Form->input("prenom", $options);
     $options = array
     (
      "placeholder" => "Identifiant",
      "value" => "admin",
      "label" => array_merge($LabelOptions, array("text" => "Identifiant"))
     );
     if ($this->Form->error("username")) { $options["div"] = "form-group has-error"; }
     echo $this->Form->input("username", $options);
     $options = array
     (
      "placeholder" => "Mot de passe",
      "label" => array_merge($LabelOptions, array("text" => "Mot de passe"))
     );
     if ($this->Form->error("password")) { $options["div"] = "form-group has-error"; }
     echo $this->Form->input("password", $options);
    ?>
    <div class="form-group">
     <?php
      $options = array
      (
       "class" => "btn btn-success col-xs-12 col-sm-12 ",
       "before" => "<div class='col-lg-3 col-lg-offset-3'>",
       "after" => "</div>"
      );
      echo $this->Form->submit("Créer", $options);
     ?>
    </div>
   </fieldset>
   <?= $this->Form->end(); ?>
 </div>
</div>