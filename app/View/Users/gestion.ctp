<?php
$role = $this->Session->read('Auth.User.Role.nom_code');
$tablenght = 16;
?>
<div class="row">
 <div class="col-lg-12">
  <div class="panel panel-default">
   <div class="panel-body">
    <fieldset>
     <legend>
       Gestion des utilisateurs :
       <?= $this->Html->link('Nouveau', array('controller' => 'users', 'action' => 'add'), array('class' => 'btn btn-success pull-right', 'style' => 'position:relative;top:-8px;')); ?>

       <?= $this->Html->link('Importer', array('controller' => 'users', 'action' => 'import'), array('class' => 'btn btn-info pull-right', 'style' => 'position:relative;top:-8px; left:-5px')); ?>

       <?php
       if ($utilisateurs_final == true) {
         echo $this->Html->link('Exporter', array('controller' => 'users', 'action' => 'export'), array('class' => 'btn btn-info pull-right', 'style' => 'position:relative;top:-8px; left:-7px'));
       }
        ?>
     </legend>
     <ul class="nav nav-pills listab" style="margin-bottom:8px;">
      <li class="active">
       <a href="#eleve" data-toggle="tab">Élèves</a>
      </li>
      <li>
       <a href="#prof" data-toggle="tab">Enseignants</a>
      </li>
      <li>
       <a href="#vs" data-toggle="tab">Vie-Scolaire</a>
      </li>
      <li>
       <a href="#va" data-toggle="tab">Vacataires</a>
      </li>
      <?php if ($role == 'WEBMASTER'): ?>
      <li>
       <a href="#admin" data-toggle="tab">Administrateurs</a>
      </li>
      <?php endif ?>
      <li>
      </li>
      <li class="pull-right">
       <form class="form-inline" action="javascript:">
        <div class="form-group">
         <input id="inputChercher" type="text" class="form-control" placeholder="Chercher..." />
        </div>
       </form>
      </li>
     </ul>
     <div class="tab-content">
      <div class="tab-pane fade in active" id="eleve">
       <table class="table table-bordered table-striped table-hover table-condensed">
        <thead>
         <tr>
          <th>Nom</th>
          <th>Prénom</th>
          <th>Identifiant</th>
          <th>Classe</th>
          <th class="text-center">Action</th>
         </tr>
        </thead>
        <tbody class="liste">
        <?php foreach ($tousLesUsers as $key => $unUser): ?>
         <?php if ($unUser['Role']['nom_code'] == 'ELEVE' ): ?>
         <tr>
          <td style="vertical-align:middle">
           <?php 
            if (strlen($unUser["User"]["nom"])>$tablenght)
            {
             echo "<span class='tooltipspan' data-toggle='tooltip' title='".$unUser['User']['nom']."'>".substr($unUser['User']['nom'],0,$tablenght)."..."."</span>";
            }
            else
            {
             echo $unUser["User"]["nom"];
            }
           ?>
          </td>
          <td style="vertical-align:middle">
           <?php 
            if (strlen($unUser["User"]["prenom"])>$tablenght)
            {
             echo "<span class='tooltipspan' data-toggle='tooltip' title='".$unUser['User']['prenom']."'>".substr($unUser['User']['prenom'],0,$tablenght)."..."."</span>";
            }
            else
            {
             echo $unUser["User"]["prenom"];
            }
           ?>
          </td>
          <td style="vertical-align:middle">
           <?php 
            if (strlen($unUser["User"]["username"])>$tablenght)
            {
             echo "<span class='tooltipspan' data-toggle='tooltip' title='".$unUser['User']['username']."'>".substr($unUser['User']['username'],0,$tablenght)."..."."</span>";
            }
            else
            {
             echo $unUser["User"]["username"];
            }
           ?>
          </td>
          <td style="vertical-align:middle">
           <?php 
            if (strlen($unUser["Division"]["nom"])>$tablenght)
            {
             echo "<span class='tooltipspan' data-toggle='tooltip' title='".$unUser['Division']['nom']."'>".substr($unUser['Division']['nom'],0,$tablenght)."..."."</span>";
            }
            else
            {
             echo $unUser["Division"]["nom"];
            }
           ?>
          </td>
          <td style="vertical-align:middle" class="text-center">
           <?= $this->Html->link('Modifier', array('controller' => 'users', 'action' => 'update', $unUser['User']['id']), array('class' => 'btn btn-success btn-sm', 'escape' => false)); ?>
           <?= $this->Html->link('Supprimer', array('controller' => 'users', 'action' => 'remove', $unUser['User']['id']), array('class' => 'btn btn-danger btn-sm', 'escape' => false), "Voulez-vous vraiment supprimer cet utilisateur ?"); ?>
          </td>
         </tr>
         <?php endif ?>
        <?php endforeach ?>
        </tbody>
       </table>
      </div>
      <div class="tab-pane fade" id="prof">
       <table class="table table-bordered table-striped table-hover table-condensed">
        <thead>
         <tr>
          <th>Nom</th>
          <th>Prénom</th>
          <th>Identifiant</th>
          <th>Classe</th>
          <th class="text-center">Action</th>
         </tr>
        </thead>
        <tbody class="liste">
        <?php foreach ($tousLesUsers as $key => $unUser): ?>
         <?php if ($unUser['Role']['nom_code'] == 'PROF' ): ?>
         <tr>
          <td style="vertical-align:middle">
           <?php 
            if (strlen($unUser["User"]["nom"])>$tablenght)
            {
             echo "<span class='tooltipspan' data-toggle='tooltip' title='".$unUser['User']['nom']."'>".substr($unUser['User']['nom'],0,$tablenght)."..."."</span>";
            }
            else
            {
             echo $unUser["User"]["nom"];
            }
           ?>
          </td>
          <td style="vertical-align:middle">
           <?php 
            if (strlen($unUser["User"]["prenom"])>$tablenght)
            {
             echo "<span class='tooltipspan' data-toggle='tooltip' title='".$unUser['User']['prenom']."'>".substr($unUser['User']['prenom'],0,$tablenght)."..."."</span>";
            }
            else
            {
             echo $unUser["User"]["prenom"];
            }
           ?>
          </td>
          <td style="vertical-align:middle">
           <?php 
            if (strlen($unUser["User"]["username"])>$tablenght)
            {
             echo "<span class='tooltipspan' data-toggle='tooltip' title='".$unUser['User']['username']."'>".substr($unUser['User']['username'],0,$tablenght)."..."."</span>";
            }
            else
            {
             echo $unUser["User"]["username"];
            }
           ?>
          </td>
          <td style="vertical-align:middle">
           <?php 
            if (strlen($unUser["Division"]["nom"])>$tablenght)
            {
             echo "<span class='tooltipspan' data-toggle='tooltip' title='".$unUser['Division']['nom']."'>".substr($unUser['Division']['nom'],0,$tablenght)."..."."</span>";
            }
            else
            {
             echo $unUser["Division"]["nom"];
            }
           ?>
          </td>
          <td style="vertical-align:middle" class="text-center">
           <?= $this->Html->link('Modifier', array('controller' => 'users', 'action' => 'update', $unUser['User']['id']), array('class' => 'btn btn-success btn-sm', 'escape' => false)); ?>
           <?= $this->Html->link('Supprimer', array('controller' => 'users', 'action' => 'remove', $unUser['User']['id']), array('class' => 'btn btn-danger btn-sm', 'escape' => false), "Voulez-vous vraiment supprimer cet utilisateur ?"); ?>
          </td>
         </tr>
         <?php endif ?>
        <?php endforeach ?>
        </tbody>
       </table>
      </div>
      <div class="tab-pane fade" id="vs">
       <table class="table table-bordered table-striped table-hover table-condensed">
        <thead>
         <tr>
          <th>Nom</th>
          <th>Prénom</th>
          <th>Identifiant</th>
          <th>Classe</th>
          <th class="text-center">Action</th>
         </tr>
        </thead>
        <tbody class="liste">
        <?php foreach ($tousLesUsers as $key => $unUser): ?>
         <?php if ($unUser['Role']['nom_code'] == 'VIE_SCOLAIRE' ): ?>
         <tr>
          <td style="vertical-align:middle">
           <?php 
            if (strlen($unUser["User"]["nom"])>$tablenght)
            {
             echo "<span class='tooltipspan' data-toggle='tooltip' title='".$unUser['User']['nom']."'>".substr($unUser['User']['nom'],0,$tablenght)."..."."</span>";
            }
            else
            {
             echo $unUser["User"]["nom"];
            }
           ?>
          </td>
          <td style="vertical-align:middle">
           <?php 
            if (strlen($unUser["User"]["prenom"])>$tablenght)
            {
             echo "<span class='tooltipspan' data-toggle='tooltip' title='".$unUser['User']['prenom']."'>".substr($unUser['User']['prenom'],0,$tablenght)."..."."</span>";
            }
            else
            {
             echo $unUser["User"]["prenom"];
            }
           ?>
          </td>
          <td style="vertical-align:middle">
           <?php 
            if (strlen($unUser["User"]["username"])>$tablenght)
            {
             echo "<span class='tooltipspan' data-toggle='tooltip' title='".$unUser['User']['username']."'>".substr($unUser['User']['username'],0,$tablenght)."..."."</span>";
            }
            else
            {
             echo $unUser["User"]["username"];
            }
           ?>
          </td>
          <td style="vertical-align:middle">
           <?php 
            if (strlen($unUser["Division"]["nom"])>$tablenght)
            {
             echo "<span class='tooltipspan' data-toggle='tooltip' title='".$unUser['Division']['nom']."'>".substr($unUser['Division']['nom'],0,$tablenght)."..."."</span>";
            }
            else
            {
             echo $unUser["Division"]["nom"];
            }
           ?>
          </td>
          <td style="vertical-align:middle" class="text-center">
           <?= $this->Html->link('Modifier', array('controller' => 'users', 'action' => 'update', $unUser['User']['id']), array('class' => 'btn btn-success btn-sm', 'escape' => false)); ?>
           <?= $this->Html->link('Supprimer', array('controller' => 'users', 'action' => 'remove', $unUser['User']['id']), array('class' => 'btn btn-danger btn-sm', 'escape' => false), "Voulez-vous vraiment supprimer cet utilisateur ?"); ?>
          </td>
         </tr>
         <?php endif ?>
        <?php endforeach ?>
        </tbody>
       </table>
      </div>
      <div class="tab-pane fade" id="va">
       <table class="table table-bordered table-striped table-hover table-condensed">
        <thead>
         <tr>
          <th>Nom</th>
          <th>Prénom</th>
          <th>Identifiant</th>
          <th>Classe</th>
          <th class="text-center">Action</th>
         </tr>
        </thead>
        <tbody class="liste">
        <?php foreach ($tousLesUsers as $key => $unUser): ?>
         <?php if ($unUser['Role']['nom_code'] == 'VACATAIRE' ): ?>
         <tr>
          <td style="vertical-align:middle">
           <?php 
            if (strlen($unUser["User"]["nom"])>$tablenght)
            {
             echo "<span class='tooltipspan' data-toggle='tooltip' title='".$unUser['User']['nom']."'>".substr($unUser['User']['nom'],0,$tablenght)."..."."</span>";
            }
            else
            {
             echo $unUser["User"]["nom"];
            }
           ?>
          </td>
          <td style="vertical-align:middle">
           <?php 
            if (strlen($unUser["User"]["prenom"])>$tablenght)
            {
             echo "<span class='tooltipspan' data-toggle='tooltip' title='".$unUser['User']['prenom']."'>".substr($unUser['User']['prenom'],0,$tablenght)."..."."</span>";
            }
            else
            {
             echo $unUser["User"]["prenom"];
            }
           ?>
          </td>
          <td style="vertical-align:middle">
           <?php 
            if (strlen($unUser["User"]["username"])>$tablenght)
            {
             echo "<span class='tooltipspan' data-toggle='tooltip' title='".$unUser['User']['username']."'>".substr($unUser['User']['username'],0,$tablenght)."..."."</span>";
            }
            else
            {
             echo $unUser["User"]["username"];
            }
           ?>
          </td>
          <td style="vertical-align:middle">
           <?php 
            if (strlen($unUser["Division"]["nom"])>$tablenght)
            {
             echo "<span class='tooltipspan' data-toggle='tooltip' title='".$unUser['Division']['nom']."'>".substr($unUser['Division']['nom'],0,$tablenght)."..."."</span>";
            }
            else
            {
             echo $unUser["Division"]["nom"];
            }
           ?>
          </td>
          <td style="vertical-align:middle" class="text-center">
           <?= $this->Html->link('Modifier', array('controller' => 'users', 'action' => 'update', $unUser['User']['id']), array('class' => 'btn btn-success btn-sm', 'escape' => false)); ?>
           <?= $this->Html->link('Supprimer', array('controller' => 'users', 'action' => 'remove', $unUser['User']['id']), array('class' => 'btn btn-danger btn-sm', 'escape' => false), "Voulez-vous vraiment supprimer cet utilisateur ?"); ?>
          </td>
         </tr>
         <?php endif ?>
        <?php endforeach ?>
        </tbody>
       </table>
      </div>
      <?php if ($role == 'WEBMASTER'): ?>
      <div class="tab-pane fade" id="admin">
       <table class="table table-bordered table-striped table-hover table-condensed">
        <thead>
         <tr>
          <th>Nom</th>
          <th>Prénom</th>
          <th>Identifiant</th>
          <th>Classe</th>
          <th class="text-center">Action</th>
         </tr>
        </thead>
        <tbody class="liste">
        <?php foreach ($tousLesUsers as $key => $unUser): ?>
         <?php if ($unUser['Role']['nom_code'] == 'ADMIN' ): ?>
         <tr>
          <td style="vertical-align:middle">
           <?php 
            if (strlen($unUser["User"]["nom"])>$tablenght)
            {
             echo "<span class='tooltipspan' data-toggle='tooltip' title='".$unUser['User']['nom']."'>".substr($unUser['User']['nom'],0,$tablenght)."..."."</span>";
            }
            else
            {
             echo $unUser["User"]["nom"];
            }
           ?>
          </td>
          <td style="vertical-align:middle">
           <?php 
            if (strlen($unUser["User"]["prenom"])>$tablenght)
            {
             echo "<span class='tooltipspan' data-toggle='tooltip' title='".$unUser['User']['prenom']."'>".substr($unUser['User']['prenom'],0,$tablenght)."..."."</span>";
            }
            else
            {
             echo $unUser["User"]["prenom"];
            }
           ?>
          </td>
          <td style="vertical-align:middle">
           <?php 
            if (strlen($unUser["User"]["username"])>$tablenght)
            {
             echo "<span class='tooltipspan' data-toggle='tooltip' title='".$unUser['User']['username']."'>".substr($unUser['User']['username'],0,$tablenght)."..."."</span>";
            }
            else
            {
             echo $unUser["User"]["username"];
            }
           ?>
          </td>
          <td style="vertical-align:middle">
           <?php 
            if (strlen($unUser["Division"]["nom"])>$tablenght)
            {
             echo "<span class='tooltipspan' data-toggle='tooltip' title='".$unUser['Division']['nom']."'>".substr($unUser['Division']['nom'],0,$tablenght)."..."."</span>";
            }
            else
            {
             echo $unUser["Division"]["nom"];
            }
           ?>
          </td>
          <td style="vertical-align:middle" class="text-center">
           <?= $this->Html->link('Modifier', array('controller' => 'users', 'action' => 'update', $unUser['User']['id']), array('class' => 'btn btn-success btn-sm', 'escape' => false)); ?>
           <?= $this->Html->link('Supprimer', array('controller' => 'users', 'action' => 'remove', $unUser['User']['id']), array('class' => 'btn btn-danger btn-sm', 'escape' => false), "Voulez-vous vraiment supprimer cet utilisateur ?"); ?>
          </td>
         </tr>
         <?php endif ?>
        <?php endforeach ?>
        </tbody>
       </table>
      </div>
      <?php endif ?>      
     </div>
    </fieldset>
   </div>
  </div>
 </div>
</div>
<script>
 $(document).ready(function() {
  var $rows = $(".liste tr");
  $('#inputChercher').keyup(function()
  {
   if ($('#inputChercher').val() != "")
   {
    $($("#inputChercher").parent()[0]).addClass("has-warning");
   }
   else
   {
    $($("#inputChercher").parent()[0]).removeClass("has-warning");    
   }
   var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
   $rows.show().filter(function()
   {
    var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
    return !~text.indexOf(val);
   }).hide();
  });
  $('.listab li a').click(function()
  {
   $('#inputChercher').val("");
   $($("#inputChercher").parent()[0]).removeClass("has-warning");
   var val = $.trim($("").val()).replace(/ +/g, ' ').toLowerCase();
   $rows.show().filter(function()
   {
    var text = $("").text().replace(/\s+/g, ' ').toLowerCase();
    return !~text.indexOf(val);
   }).hide(); 
  });
 });
</script>
