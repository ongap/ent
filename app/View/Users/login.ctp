<?php $this->set('title_for_layout', 'Accueil - Ent'); ?>

<div class="panel panel-default">
 <div class="panel-body">
 <?php
  $LabelOptions = array("class" => "col-lg-3 control-label");
  $options = array
  (
   "class" => "form-horizontal",
   "inputDefaults" => array
   (
    "class" => "form-control",
    "div" => array("class" => "form-group"),
    "label" => $LabelOptions,
    "between" => "<div class='col-lg-9'>",
    "after" => "</div>"
   )
  );
  echo $this->Form->create("User", $options);
 ?>
  <fieldset>
   <legend>Connexion</legend>
   <?php
    $options = array
    (
     "placeholder" => "Identifiant",
     "label" => array_merge($LabelOptions, array("text" => "Identifiant"))
    );
    echo $this->Form->input("username", $options);
    $options = array
    (
     "placeholder" => "Mot de passe",
     "label" => array_merge($LabelOptions, array("text" => "Mot de passe"))
    );
    echo $this->Form->input("password", $options);
    $options = array
    (
     "div" => array("class" => "form-group"),
     "class" => "btn btn-primary col-xs-12 col-sm-12 ",
     "before" => "<div class='col-lg-9 col-lg-offset-3'>",
     "after" => "</div>"
    );
    echo $this->Form->submit("Connexion", $options);
   ?>
  </fieldset>
  <?= $this->Form->end(); ?>
 </div>
</div>