<div class="row">
 <div class="col-lg-12">
  <div class="panel panel-default">
   <div class="panel-body">
    <fieldset>
     <legend>
      Importer des utilisateurs :
     </legend>
     <?php
      $options = array(
       "type" => "file",
       "class" => "form-horizontal"
      );
      echo $this->Form->create("importUser", $options);
      $options = array
      (
       "type" => "checkbox",
       "div" => array("class"=>"form-group"),
       "before" => "<div class='col-lg-offset-3 col-lg-9'><div class='checkbox'><label>",
       "after" => " Générer l'identifiant </label></div></div>",
       "format" => array("before", "input", "error", "after")
      );
      if ($this->Form->error("genereLogin")) { $options["div"] = "form-group has-error"; }
      echo $this->Form->input("genereLogin", $options);
      $options = array
      (
       "type" => "checkbox",
       "div" => array("class"=>"form-group"),
       "before" => "<div class='col-lg-offset-3 col-lg-9'><div class='checkbox'><label>",
       "after" => " Générer le mot de passe </label></div></div>",
       "format" => array("before", "input", "error", "after")
      );
      if ($this->Form->error("genereMotdepasse")) { $options["div"] = "form-group has-error"; }
      echo $this->Form->input("genereMotdepasse", $options);
     ?>
     <div class="form-group">
      <label for="data[importUser][csv]" class="col-lg-3 control-label">Fichier CSV</label>
      <div class="col-lg-9" style="padding-top:4px;">
       <?= $this->Form->file("csv"); ?>
      </div>
     </div>
     <div class="form-group">
      <div class="col-lg-offset-3 col-lg-9">
       <p class="help-block" id="aide">le csv doit avoir cette forme : "nom";"prenom";"login";"motpasse";"classe";"grade"</p>
      </div>
     </div>
     <?php
      $options = array
      (
       "class" => "btn btn-success col-xs-12 col-sm-12 ",
       "before" => "<div class='col-lg-3 col-lg-offset-9'>",
       "after" => "</div>"
      );
      echo $this->Form->submit("Envoyer", $options);
      echo $this->Form->end();
     ?>
    </fieldset>
   </div>
  </div>
 </div>
</div>
<script>
$(document).ready(function(){
 $("input").change(function(){
  var login = $("#importUserGenereLogin").is(":checked");
  var mdp = $("#importUserGenereMotdepasse").is(":checked");
  if (login && mdp) { $("#aide").text('le csv doit avoir cette forme : "civilité";"nom";"prenom";"date de naissance(JJ/MM/AAAA)";"classe";grade"'); }
  else if (login && !mdp) { $("#aide").text('le csv doit avoir cette forme : "civilité";"nom";"prenom";motpasse";"classe";grade"'); }
  else if (!login && mdp) { $("#aide").text('le csv doit avoir cette forme : "civilité";"nom";"prenom";"login";"date de naissance(JJ/MM/AAAA)";"classe";grade"'); }
  else { $("#aide").text('le csv doit avoir cette forme : "civilité";"nom";"prenom";"login";"motpasse";"classe";"grade"'); }
 });
});
</script>