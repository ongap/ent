<div class="panel panel-default">
 <div class="panel-body">
 <?php
  $LabelOptions = array("class" => "col-lg-3 control-label");
  $options = array
  (
   "class" => "form-horizontal",
   "inputDefaults" => array
   (
    "class" => "form-control",
    "div" => array("class" => "form-group"),
    "label" => $LabelOptions,
    "between" => "<div class='col-lg-9'>",
    "after" => "</div>"
   )
  );
  echo $this->Form->create("User", $options);
 ?>
  <fieldset>
   <legend>Modifier votre mot de passe :</legend>
   <?php
    $options = array
    (
     "placeholder" => "Nouveau mot de passe",
     "label" => array_merge($LabelOptions, array("text" => "Nouveau mot de passe"))
    );
    if ($this->Form->error("password")) { $options["div"] = "form-group has-error"; }
    echo $this->Form->input("password", $options);
    $options = array
    (
     "div" => array("class" => "form-group"),
     "class" => "btn btn-primary col-xs-12 col-sm-12 ",
     "before" => "<div class='col-lg-9 col-lg-offset-3'>",
     "after" => "</div>"
    );
    echo $this->Form->input('id', array('type' => 'hidden'));
    echo $this->Form->submit("Appliquer", $options);
   ?>
  </fieldset>
  <?= $this->Form->end(); ?>
 </div>
</div>