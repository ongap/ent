<?php
$active = $this->Session->read('active');
$role = $this->Session->read('Auth.User.Role.nom_code');

//$modePeriscolaire = file_exists('../Controller/PeriscolairesController.php');

$rubriques = $this->requestAction(array('controller' => 'rubriques', 'action' => 'getAllRubriques'));
?>
<div class="row row-bottom">
	<div class="col-lg-12">
		<div class="thumbnail menu_gauche">
			<ul class="nav nav-pills nav-stacked">
				<li class="nav-header disabled">
					<a>Mon compte</a>
				</li>
				<li <?php if ($active == 'actualites'): ?> class="active" <?php endif ?> >
					<?= $this->Html->link('<i class="fa fa-rss"></i> Actualités', '/', array('escape' => false, "style" => "border-radius:0px;")); ?>
				</li>

				<?php foreach ($rubriques as $key => $rubrique): ?>

					<li class="nav-header disabled">
						<a><?= $rubrique['Rubrique']['nom'] ?></a>
					</li>

					<?php if ($role != 'ELEVE'): ?>

						<li <?php if ($active == 'gestion_des_ateliers_'.$rubrique['Rubrique']['nom_canonique']): ?> class="active" <?php endif ?> >
						<?= $this->Html->link('<i class="fa fa-calendar"></i> '.$rubrique['Rubrique']['label_gestion'],
							array('controller' => 'ateliers', 'rubrique' => $rubrique['Rubrique']['nom_canonique'] , 'action' => 'gestion'),
							array('escape' => false, "style" => "border-radius:0px;"));
						?>
						</li>

						<li <?php if ($active == 'gestion_des_inscriptions_'.$rubrique['Rubrique']['nom_canonique']): ?> class="active" <?php endif ?> >
						<?= $this->Html->link('<i class="fa fa-calendar"></i> '.$rubrique['Rubrique']['label_gestion_inscription'],
							array('controller' => 'ateliers', 'rubrique' => $rubrique['Rubrique']['nom_canonique'] , 'action' => 'forcer'),
							array('escape' => false, "style" => "border-radius:0px;"));
						?>
						</li>

						<li <?php if ($active == 'suivi_des_ateliers_'.$rubrique['Rubrique']['nom_canonique']): ?> class="active" <?php endif ?> >
						<?= $this->Html->link('<i class="fa fa-eye"></i> '.$rubrique['Rubrique']['label_suivi'],
							array('controller' => 'ateliers', 'rubrique' => $rubrique['Rubrique']['nom_canonique'] , 'action' => 'suivi'),
							array('escape' => false, "style" => "border-radius:0px;"));
						?>
						</li>

					<?php else://Pour les comptes non ELEVE ?>

						<li <?php if ($active == 'gestion_des_ateliers_'.$rubrique['Rubrique']['nom_canonique']): ?> class="active" <?php endif ?> >
						<?= $this->Html->link('<i class="fa fa-calendar"></i> '.$rubrique['Rubrique']['label_inscription'],
							array('controller' => 'ateliers', 'rubrique' => $rubrique['Rubrique']['nom_canonique'], 'action' => 'inscription'),
							array('escape' => false, "style" => "border-radius:0px;"));
						?>
						</li>

						<li <?php if ($active == 'gestion_des_ateliers_recap_'.$rubrique['Rubrique']['nom_canonique']): ?> class="active" <?php endif ?> >
						<?= $this->Html->link('<i class="fa fa-calendar"></i> '.$rubrique['Rubrique']['label_recapitulation'],
							array('controller' => 'ateliers', 'rubrique' => $rubrique['Rubrique']['nom_canonique'], 'action' => 'recapitulation'),
							array('escape' => false, "style" => "border-radius:0px;"));
						?>
						</li>

					<?php endif ?>
				<?php endforeach ?>

				<?php if ($role == 'ADMIN' || $role == 'WEBMASTER'): ?>
					<li class="nav-header disabled">
						<a>Administration</a>
					</li>
					
					<li <?php if ($active == 'gestion_actualites'): ?> class="active" <?php endif ?> >
					<?= $this->Html->link('<i class="fa fa-pencil"></i> Gestion des actualités',
						array('controller' => 'actualites', 'action' => 'gestion'),
						array('escape' => false, "style" => "border-radius:0px;"));
					?>
					</li>

					<li <?php if ($active == 'gestion_des_matieres'): ?> class="active" <?php endif ?> >
						<?= $this->Html->link('<i class="fa fa-tags"></i> Gestion des matières',
							array('controller' => 'matieres', 'action' => 'gestion'),
							array('escape' => false, "style" => "border-radius:0px;"));
						?>
					</li>

					<li <?php if ($active == 'gestion_des_classes'): ?> class="active" <?php endif ?> >
						<?= $this->Html->link('<i class="fa fa-list"></i> Gestion des classes',
							array('controller' => 'divisions', 'action' => 'gestion'),
							array('escape' => false, "style" => "border-radius:0px;"));
						?>
					</li>

					<li <?php if ($active == 'gestion_des_salles'): ?> class="active" <?php endif ?> >
					<?= $this->Html->link('<i class="fa fa-th"></i> Gestion des salles',
						array('controller' => 'salles', 'action' => 'gestion'),
						array('escape' => false, "style" => "border-radius:0px;"));
					?>
					</li>

					<li <?php if ($active == 'gestion_des_utilisateurs'): ?> class="active" <?php endif ?> >
					<?= $this->Html->link('<i class="fa fa-cogs"></i> Gestion des utilisateurs',
						array('controller' => 'users', 'action' => 'gestion'),
						array('escape' => false, "style" => "border-radius:0px;"));
					?>
					</li>

					<li <?php if ($active == 'gestion_des_parametres'): ?> class="active" <?php endif ?> >
					<?= $this->Html->link('<i class="fa fa-wrench"></i> Paramètres globaux',
						array('controller' => 'parametres', 'action' => 'gestion'),
						array('escape' => false, "style" => "border-radius:0px;"));
					?>
					</li>
				<?php endif ?>

				<li class="nav-divider"></li>
				<li style="text-align:center;">
					<em style="font-size:12px;">Copyright ONGAP</em>
				</li>
			</ul>

		</div>
	</div>
</div>