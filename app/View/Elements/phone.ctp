<?php if (
	$this->Session->read('Auth.User.phone') == "" &&
	$this->Session->read('Auth.User.Role.nom_code') == "ELEVE" &&
	$this->Session->read('Parametre.needPhone')): ?>
	<?= $this->Html->link('Ajouter un numéro de téléphone', array(
		'controller' => 'users',
		'action' => 'phone'
	), array(
		'class' => 'btn btn-warning btn-lg'
		)); ?>   
<?php endif ?>