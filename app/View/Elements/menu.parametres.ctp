<?php 
$active = $this->Session->read('menu.parametres.active');

//@TODO Générer cette liste en fonction des modules

$liens = array(
	"Ateliers" 		=> array('controller' => 'ateliers', 'action' => 'configuration'),
	"Horaires" 		=> array('controller' => 'parametres', 'action' => 'schedule'),
	"Paramètres" 	=> array('controller' => 'parametres', 'action' => 'gestion'),
	"Périodes" 		=> array('controller' => 'periodes', 'action' => 'configuration'),
	"Rubriques" 	=> array('controller' => 'rubriques', 'action' => 'configuration'),
	"Utilisateurs"	=> array('controller' => 'users', 'action' => 'configuration'),
	);
?>

<ul class="nav nav-pills nav-justified">
 	<?php foreach ($liens as $key => $value): ?>
		<?php if ($active == $key): ?>
			<li class="active">
				<a disabled><?= $key ?></a>
			</li>
		<?php else: ?>
			<li>
				<?= $this->Html->link($key,$value); ?>
			</li>
		<?php endif ?>
	<?php endforeach ?>
</ul>
