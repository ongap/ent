<?php
$nomEtalissement = $this->requestAction(array('controller' => 'parametres', 'action' => 'getNomEtalissement'));
?>
<nav class="navbar navbar-inverse" role="navigation">
 <div class="container">
  <div class="navbar-header">
   <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
   </button>
   <?= $this->Html->link($nomEtalissement, '/', array('class' => 'navbar-brand')); ?>
  </div>
  <div class="collapse navbar-collapse" id="navbar-collapse-1">
   <?php if ($this->Session->read('Auth.User.id')): ?>
   <ul class="nav navbar-nav navbar-right">
    <li class="active">
     <a disabled>
      <i class="fa fa-user"></i>
      <b>
				<?= $this->Session->read('Auth.User.civilite'); ?>
				<?= $this->Session->read('Auth.User.nom'); ?>
				<?= $this->Session->read('Auth.User.prenom'); ?>
				(<?= $this->Session->read('Auth.User.Division.nom'); ?>)
      </b>
      <?php $role = $this->Session->read('Auth.User.Role.nom_code');?>
      <?php if ($role == "WEBMASTER"): ?>
       <span class="label label-default role">Webmaster</span>
      <?php elseif ($role == "ADMIN"): ?>
       <span class="label label-important role">Administrateur</span>
      <?php elseif ($role == "VIESCO"): ?>
       <span class="label label-warning role">Vie scolaire</span>
      <?php elseif ($role == "PROF"): ?>
       <span class="label label-info role">Enseignant</span>
      <?php elseif ($role == "ELEVE"): ?>
       <span class="label label-success role">Élève</span>
      <?php endif ?>
     </a>
    </li>
    <li>
     <?= $this->Html->link('<i class="fa fa-sign-out"></i> Déconnexion', array('controller' => 'users', 'action' => 'logout'), array('escape' => false)); ?>     
    </li>
   </ul>
   <?php else: ?>
   <ul class="nav navbar-nav">
    <li class="active">
     <?= $this->Html->link('<i class="fa fa-home"></i> Accueil','/', array('escape' => false)); ?>
    </li>
   </ul>
   <ul class="nav navbar-nav navbar-right">
    <li>
     <?= $this->Html->link('<i class="fa fa-envelope"></i> Contact','/', array('escape' => false)); ?>
    </li>
   </ul>
   <?php endif ?>
  </div> 
 </div>
</nav>