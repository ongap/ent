<!DOCTYPE html>
<html lang="fr">
 <head>
  <title>ENT : <?= $title_for_layout ?></title>
  <?php
   echo $this->Html->charset();
   echo $this->fetch("meta");
   echo $this->Html->meta('favicon.ico','/favicon.ico',array('type' => 'icon'));

   echo $this->Html->css("https://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css");
   echo $this->Html->css("https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css");
   echo $this->Html->css("https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css");
   // echo $this->Html->css("http://bootswatch.com/paper/bootstrap.min.css");
   echo $this->Html->css(array("index", "menu.gauche", "bootstrap-vertical-tabs", "jquery-ui-1.10.3/themes/base/jquery-ui"));

   echo $this->Html->script("http://code.jquery.com/jquery-1.11.0.min.js");
   echo $this->Html->script("https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js");

   echo $this->Html->script(array("index", "jquery-ui-1.10.3/ui/jquery-ui", "jquery-ui-1.10.3/ui/i18n/jquery.ui.datepicker-fr", "tinymce/tinymce.min"));
   echo $this->fetch("css");
   echo $this->fetch("script");
  ?>
  <meta name="description" content="ENT">
  <meta name="author" content="ONGAP">
  <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
 </head>
 <body>
  <div id="wrap">
   <?= $this->element('menu.haut'); ?>
   <div class="container" style="padding-bottom:74px;">
    <div class="row">
     <?php if ($this->Session->read('Auth.User.nbConnexion')): ?>
     <div class="col-lg-3">
      <?= $this->element('menu.gauche');//, array(), array('cache' => array('config' => '_cake_core_')) ?>
     </div>
     <div class="col-lg-9">
      <?= $this->element('phone'); ?>
      <?= $this->Session->flash(); ?>
      <?= $this->fetch("content"); ?>
     </div>
     <?php else: ?>
     <div class="col-lg-6 col-lg-offset-3">
      <?= $this->Session->flash(); ?>
      <?= $this->fetch("content"); ?>
     </div>
     <?php endif ?>
    </div>
   </div>
  </div>
 </body>
</html>
