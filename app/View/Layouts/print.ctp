<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8" />

        <title><?= $title_for_layout ?></title>

        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="description" content="ENT">
        <meta name="author" content="Paul Guillard, Adrien Quillivic">


   <?= $this->Html->css("https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"); ?>
        
  </head>



    <body onLoad="window.print()">

    <div class="container content">

        <div class="row-fluid">
            <?= $this->fetch('content'); ?>
        </div>

    </div>

  </body>
</html>
