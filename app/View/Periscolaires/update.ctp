<div class="row">
 <div class="col-lg-12">
  <div class="panel panel-default">
   <div class="panel-body">
  <?php
   $LabelOptions = array("class" => "col-lg-3 control-label");
   $options = array
   (
    "class" => "form-horizontal",
    "inputDefaults" => array
    (
     "class" => "form-control",
     "div" => array("class" => "form-group"),
     "label" => $LabelOptions,
     "between" => "<div class='col-lg-9'>",
     "after" => "</div>",
     'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block')),
     "format" => array('before', 'label', 'between', 'input', 'error', 'after')
    ),
   );
   echo $this->Form->create("Atelier", $options);
  ?>
   <fieldset>
    <legend>
      Modifier un loisir périscolaire :
    </legend>
    <?php
     $options = array
     (
      "placeholder" => "Nom de l'atelier",
      "label" => array_merge($LabelOptions, array("text" => "Nom de l'atelier"))
     );
     if ($this->Form->error("nom")) { $options["div"] = "form-group has-error"; }
     echo $this->Form->input("nom", $options);
     $options = array
     (
      "label" => array_merge($LabelOptions, array("text" => "Animateur"))
     );
     if ($this->Form->error("user_id")) { $options["div"] = "form-group has-error"; }
     echo $this->Form->input("user_id", $options);
     $options = array
     (
      "label" => array_merge($LabelOptions, array("text" => "Activité"))
     );
     if ($this->Form->error("matiere_id")) { $options["div"] = "form-group has-error"; }
     echo $this->Form->input("matiere_id", $options); 
     $options = array
     (
      "between" => "<div class='col-lg-9 toEmpty'>",
      "label" => array_merge($LabelOptions, array("text" => "Jour (Date du premier atelier pour une période)")),
      "dateFormat" => "DMY"
     );
     if ($this->Form->error("jour")) { $options["div"] = "form-group has-error"; }
     echo $this->Form->input("jour", $options);      
    ?>
    <div class="form-group">
     <div class="col-xs-3">
      <ul class="nav nav-tabs tabs-left">
       <li class="active"><a href="#date" data-toggle="tab"><b>Date</b></a></li>
       <li><a href="#periode" data-toggle="tab"><b>Période</b></a></li>
      </ul>
     </div>
     <div class="col-xs-9">
      <div class="tab-content">
       <div class="tab-pane active" id="date">
       <?php
        $options = array
        (
         "between" => "<div class='col-lg-12'>",
         "after" => "<p class='help-block'>hebdomadaire (nombre de répétition)</p></div>",
         "format" => array('before', 'between', 'input', 'error', 'after')
        );
        if ($this->Form->error("hebdomadaire")) { $options["div"] = "form-group has-error"; }
        echo $this->Form->input('hebdomadaire', $options);
       ?>
       </div>
       <div class="tab-pane" id="periode">
       <?php
        $options = array
        (
         "empty" => "(choisissez)",
         "between" => "<div class='col-lg-12'>",
         "format" => array('before', 'between', 'input', 'error', 'after')
        );
        if ($this->Form->error("periode_id")) { $options["div"] = "form-group has-error"; }
        echo $this->Form->input('periode_id', $options);
       ?>
       </div>
      </div>
     </div>  
    </div>
    <?php
     $options = array
     (
      "label" => array_merge($LabelOptions, array("text" => "Lieux de pratique"))
     );
     if ($this->Form->error("salle_id")) { $options["div"] = "form-group has-error"; }
     echo $this->Form->input("salle_id", $options);
     $options = array
     (
      "label" => array_merge($LabelOptions, array("text" => "Nombre de place")),
     );
     if ($this->Form->error("nombre_place")) { $options["div"] = "form-group has-error"; }
     echo $this->Form->input("nombre_place", $options);
     $options = array
     (
      'required' => false,
      "label" => array_merge($LabelOptions, array("text" => "Description"))
     );
     if ($this->Form->error("description")) { $options["div"] = "form-group has-error"; }
     echo $this->Form->input("description", $options); 
     $options = array
     (
      "label" => array_merge($LabelOptions, array("text" => "Classes")),
      "multiple" => true
     );
     if ($this->Form->error("Atelier.Division")) { $options["div"] = "form-group has-error"; }
     echo $this->Form->input("Atelier.Division", $options);
    ?>



    <div class="form-group">
     <div class="col-lg-3 col-lg-offset-3">
      <?= $this->Html->link("Retour", array("action" => "gestion"), array("class" => "btn btn-danger col-xs-12 col-sm-12", "style" => "margin-bottom:4px;")); ?>
     </div>
     <?php
      $this->Form->input('id', array('type' => 'hidden'));
      $options = array
      (
       "class" => "btn btn-success col-xs-12 col-sm-12 ",
       "before" => "<div class='col-lg-3 col-lg-offset-3'>",
       "after" => "</div>"
      );
      echo $this->Form->submit("Modifier", $options);
     ?>
    </div>
   </fieldset>
   <?= $this->Form->end(); ?>
   <script>
    $(document).ready(function() {
     tinymce.init({
       selector: "textarea",
       plugins: [
       "advlist autolink lists link image charmap print preview anchor",
       "searchreplace visualblocks code fullscreen",
       "insertdatetime media table contextmenu paste"
       ],
       toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
     });
     $("#AtelierJourDay").wrap("<div class='col-lg-4' style='padding:0px;padding-right:4px;'></div>");
     $("#AtelierJourMonth").wrap("<div class='col-lg-4' style='padding:0px;padding-right:4px;'></div>");
     $("#AtelierJourYear").wrap("<div class='col-lg-4' style='padding:0px;'></div>");
     $(".toEmpty").contents().filter(function(){ return this.nodeType === 3; }).remove();
    });
   </script>
   </div>
  </div>
 </div>
</div>