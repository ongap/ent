<?php
$role = $this->Session->read('Auth.User.role');
?>
<script type="text/javascript">
(function( $ ) {
    $.widget( "ui.dp", {
            _create: function() {
                var el = this.element.hide();
                this.options.altField = el;
                var input = this.input = $('<input>').insertBefore( el )
                input.focusout(function(){
                        if(input.val() == ''){
                            el.val('');
                        }
                    });
                input.datepicker(this.options)
                if(convertDate(el.val()) != null){
                    this.input.datepicker('setDate', convertDate(el.val()));
                }
            },
            destroy: function() {
                this.input.remove();
                this.element.show();
                $.Widget.prototype.destroy.call( this );
            }
    });
    var convertDate = function(date){
      if(typeof(date) != 'undefined' && date != null && date != ''){
        return new Date(date);
      } else {
        return null;
      }
    }
})( jQuery );
$(document).ready(function() {
   $(".inputDatePicker").dp({
      dateFormat: 'yy-mm-dd'
   }); 
});
</script>
<div class="row">
 <div class="col-lg-12">
  <?= $this->Session->flash('flash', array('element' => 'failure')); ?>
  <div class="panel panel-default">
   <div class="panel-body">
    <fieldset>
     <legend>
       Suivi des loisirs périscolaires :
     </legend>
     <legend>
      Liste des loisirs périscolaires pour la classe <?= $laClasse['Division']['nom'] ?> :
      <form class="form-inline pull-right" action="javascript:" style="position:relative;top:-8px;">
       <div class="form-group">
        <input id="inputChercher" type="text" class="form-control" placeholder="Chercher..." />
       </div>
      </form>
     </legend>
     <?php
      $LabelOptions = array("class" => "col-lg-3 control-label");
      $options = array
      (
       "id" => "form_ajout_new",
       "class" => "form-horizontal",
       "inputDefaults" => array
       (
        "class" => "form-control",
        "div" => array("class" => "form-group"),
        "label" => $LabelOptions,
        "between" => "<div class='col-lg-9'>",
        "after" => "</div>",
        'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block')),
        "format" => array('before', 'label', 'between', 'input', 'error', 'after')
       ),
      );
      echo $this->Form->create("Atelier", $options);
      $options = array
      (
       "class" => "inputDatePicker form-control",
       "type" => "text",
       "dateFormat" => "YMD",
       "label" => array_merge($LabelOptions, array("text" => "Premier date :"))
      );
      if ($this->Form->error("date1")) { $options["div"] = "form-group has-error"; }
      echo $this->Form->input("date1", $options);
      $options = array
      (
       "class" => "inputDatePicker form-control",
       "type" => "text",
       "dateFormat" => "YMD",
       "label" => array_merge($LabelOptions, array("text" => "Seconde date :"))
      );
      if ($this->Form->error("date2")) { $options["div"] = "form-group has-error"; }
      echo $this->Form->input("date2", $options);
      $options = array
      (
       "label" => false,
       "class" => "btn btn-success col-xs-12 col-sm-12",
       "between" => "<div class='col-lg-3 col-lg-offset-9'>",
       "type" => "button"
      );
      echo $this->Form->input("Rechercher", $options);     
      echo $this->Form->end();
     ?>
     <style type="text/css">
     .cellHead{
       width: 100px;
       text-align: center;
       margin: auto;
     }
     .cellHeadMini{
       width: 10px;
       text-align: center;
       margin: auto;
     }

     .cellMiniOui{
       width: 33px;
       text-align: center;
       color: green;
       margin: auto;
     }
     .cellMiniNon{
       width: 33px;
       text-align: center;
       color: red;
       margin: auto;
     }
     .cellMiniNeu{
       width: 33px;
       text-align: center;
       color: gray;
       margin: auto;
     }
     </style>
     <table id="tableau_liste_eleve" class="table table-bordered table-striped table-hover table-condensed" style="padding-top:24px;">
      <thead>
       <tr>
        <th><div class="cellHead">Élève</div></th>
        <?php foreach ($tousLesAteliers as $key => $unAtelier): ?>
        <?php
         $status = "Ponctuel";
          if ($unAtelier['Atelier']['periode_id'] > 0 or $unAtelier['Atelier']['atelier_id'] > 0) {
           $status = "Périodique";
          }
          elseif ($unAtelier['Atelier']['hebdomadaire'] > 1) {
           $status = "Hebdomadaire";
          }
        ?>
        <th colspan="3">
         <div class="cellHead">
          <?php echo $unAtelier['Atelier']['nom']." le ".$this->Time->format($unAtelier['Atelier']['jour'], '%d/%m/%Y')." (".$status.")"; ?>
         </div>
        </th>
        <?php endforeach ?>
       </tr>
       <tr>
        <th> <div class="cellHead"></div></th>
        <?php foreach ($tousLesAteliers as $key => $unAtelier): ?>
         <th><div class="cellHeadMini"> I </div></th><th><div class="cellHeadMini"> P </div></th><th><div class="cellHeadMini"> V </div></th>
        <?php endforeach ?>
       </tr>
      </thead>
      <tbody style="margin-top:200px;" class="liste">
      <?php foreach ($tousLesEleves as $key => $unEleve): ?>
       <tr>
        <td ><div class="cellHead"><?= $unEleve['User']['nom'].' '.$unEleve['User']['prenom']; ?></div></td>
        <?php foreach ($tousLesAteliers as $key => $unAtelier): ?>
        <?php
         $inscript = "Non";
         $css = "cellMiniNeu";
         foreach ($unEleve['Inscription'] as $key => $uneInscription){
          if ($uneInscription['atelier_id'] == $unAtelier['Atelier']['id']) {
           $inscript = "Oui";
           $css = "cellMiniOui";
          }
         }
         echo '<td> <div class="'.$css.'">'.$inscript."</div> </td>";
        ?>
        <?php
         $present = "Non";
         if ($css != "cellMiniNeu") {
          $css = "cellMiniNon";
         }
         $cssValide = "cellMiniNon";
         $valide = " . ";
         foreach ($unEleve['Creditation'] as $key => $uneCreditation){
          if ($uneCreditation['atelier_id'] == $unAtelier['Atelier']['id']){
           if ($uneCreditation['presence']){
            $present = "Oui";
            $css = "cellMiniOui";
           }
           $valide = $uneCreditation['valid'];
          }
         }
         if ($valide > 0) {
          $cssValide = "cellMiniOui";
         }
         elseif($valide == " . ") {
          $cssValide = "cellMiniNeu";
         }
         echo '<td> <div class="'.$css.'">'.$present.'</div> </td> <td> <div class="'.$cssValide.'">'.$valide."</div> </td>"; 
        ?>
        <?php endforeach ?>
       </tr>
      <?php endforeach ?>
      </tbody>
     </table>
    </fieldset>
   </div>
  </div>
 </div>
</div>
<script>
 $(document).ready(function() {
  var $rows = $(".liste tr");
  $('#inputChercher').keyup(function()
  {
   if ($('#inputChercher').val() != "")
   {
    $($("#inputChercher").parent()[0]).addClass("has-warning");
   }
   else
   {
    $($("#inputChercher").parent()[0]).removeClass("has-warning");    
   }
   var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
   $rows.show().filter(function()
   {
    var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
    return !~text.indexOf(val);
   }).hide();
  });
  $('.listab li a').click(function()
  {
   $('#inputChercher').val("");
   $($("#inputChercher").parent()[0]).removeClass("has-warning");
   var val = $.trim($("").val()).replace(/ +/g, ' ').toLowerCase();
   $rows.show().filter(function()
   {
    var text = $("").text().replace(/\s+/g, ' ').toLowerCase();
    return !~text.indexOf(val);
   }).hide(); 
  });
 });
</script>