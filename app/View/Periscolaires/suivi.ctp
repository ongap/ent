<?php
$role = $this->Session->read('Auth.User.role');
$tablenght = 16;
?>
<div class="row">
 <div class="col-lg-12">
  <div class="panel panel-default">
   <div class="panel-body">
    <fieldset>
     <legend>
       Suivi des loisirs périscolaires :
     </legend>
     <ul class="nav nav-pills nav-justified listab" style="margin-bottom:8px;">
      <li class="active">
       <a href="#jour" data-toggle="tab">Suivi des loisirs périscolaires du jour</a>
      </li>
      <li>
       <a href="#classe" data-toggle="tab">Suivi par classe</a>
      </li>
      <li>
       <a href="#eleve" data-toggle="tab">Suivi par élève</a>
      </li>
      <?php if ($role == 'WEBMASTER' || $role == 'ADMIN'): ?>
      <li>
       <a href="#prof" data-toggle="tab">Suivi par animateurs</a>
      </li>
      <?php endif ?>
      <li>
      </li>
      <li>
       <form class="form-inline" action="javascript:">
        <div class="form-group">
         <input id="inputChercher" type="text" class="form-control" placeholder="Chercher..." />
        </div>
       </form>
      </li>
     </ul>
     <div class="tab-content">
      <div class="tab-pane fade in active" id="jour">
       <table class="table table-bordered table-striped table-hover table-condensed">
        <thead>
         <tr>
          <th>Nom</th>
          <th>Animateur</th>
          <th>Date et Heure</th>
          <th>Place</th>
          <th style="text-align:center;">Action</th>
         </tr>
        </thead>
        <tbody class="liste">
        <?php $nomJour = array('Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'); ?>
        <?php foreach ($tousLesAteliers as $key => $unAtelier): ?>
        <?php if (date("Y-m-d") < $unAtelier['Atelier']['jour']): ?>
         <tr class="success">
        <?php elseif (date("Y-m-d") > $unAtelier['Atelier']['jour']): ?>
         <tr class="danger">
        <?php else: ?>
         <tr class="info">
        <?php endif ?>
          <td style="vertical-align:middle;">
           <?php 
            if (strlen($unAtelier['Atelier']['nom'])>$tablenght)
            {
             echo "<span class='tooltipspan' data-toggle='tooltip' title='".$unAtelier['Atelier']['nom']."'>".substr($unAtelier['Atelier']['nom'],0,$tablenght)."..."."</span>";
            }
            else
            {
             echo $unAtelier['Atelier']['nom'];
            }
           ?>
          </td>
          <td style="vertical-align:middle;">
           <?php 
            if (strlen($unAtelier['User']['nom'])>$tablenght)
            {
             echo "<span class='tooltipspan' data-toggle='tooltip' title='".$unAtelier['User']['nom']."'>".substr($unAtelier['User']['nom'],0,$tablenght)."..."."</span>";
            }
            else
            {
             echo $unAtelier['User']['nom'];
            }
           ?>
          </td>
          <td style="vertical-align:middle;">
           <?= $nomJour[$this->Time->format( $unAtelier['Atelier']['jour'], '%w')] ?>
           <?= $this->Time->format($unAtelier['Atelier']['jour'].' '.$unAtelier['Horaire']['heureDebut'], '%d/%m/%Y %H:%M ') ?>
          </td>
          <td style="vertical-align:middle;">
           <?= count($unAtelier['Inscription'])."/".$unAtelier['Atelier']['nombre_place'] ?>
          </td>
          <td class="text-center">
           <?= $this->Html->link('Imprimer', array('controller' => 'periscolaires', 'action' => 'impression', $unAtelier['Atelier']['id']), array('class' => 'btn btn-info btn-sm', 'escape' => false)); ?>
          </td>
         </tr>
        <?php endforeach ?>
        </tbody>
       </table>
      </div>
      <div class="tab-pane fade" id="classe">

       <table class="table table-bordered table-striped table-hover table-condensed">
        <thead>
         <tr>
          <th>Classe</th>
          <th class="text-center">Action</th>
         </tr>
        </thead>
        <tbody class="liste">
        <?php foreach ($toutesLesClasses as $key => $uneClasse): ?>
         <tr>
          <td style="vertical-align:middle;"><?= $uneClasse['Division']['nom']; ?></td>
          <td class="text-center">
           <?= $this->Html->link('Voir', array('controller' => 'periscolaires', 'action' => 'suiviClasse', $uneClasse['Division']['id']), array('class' => 'btn btn-info btn-sm', 'escape' => false)); ?>
           <?= $this->Html->link('Voir date à date', array('controller' => 'periscolaires', 'action' => 'suiviClasseDateToDate', $uneClasse['Division']['id']), array('class' => 'btn btn-info btn-sm', 'escape' => false)); ?>

          </td>
         </tr>
        <?php endforeach ?>        
        </tbody>
       </table>

      </div>
      <div class="tab-pane fade" id="eleve">
       <table class="table table-bordered table-striped table-hover table-condensed">
        <thead>
         <tr>
          <th>Nom</th>
          <th>Prénom</th>
          <th>Nombre de loisir périscolaire inscrit</th>
          <th>Classe</th>
          <th class="text-center">Action</th>
         </tr>
        </thead>
        <tbody class="liste">
        <?php foreach ($tousLesEleves as $key => $unEleve): ?> 
         <tr>
          <td style="vertical-align:middle;">
           <?php 
            if (strlen($unEleve['User']['nom'])>$tablenght)
            {
             echo "<span class='tooltipspan' data-toggle='tooltip' title='".$unEleve['User']['nom']."'>".substr($unEleve['User']['nom'],0,$tablenght)."..."."</span>";
            }
            else
            {
             echo $unEleve['User']['nom'];
            }
           ?>
          </td>
          <td style="vertical-align:middle;">
           <?php 
            if (strlen($unEleve['User']['prenom'])>$tablenght)
            {
             echo "<span class='tooltipspan' data-toggle='tooltip' title='".$unEleve['User']['prenom']."'>".substr($unEleve['User']['prenom'],0,$tablenght)."..."."</span>";
            }
            else
            {
             echo $unEleve['User']['prenom'];
            }
           ?>
          </td>
          <?php 
          $nbLoisirsPourUnEleve = 0;
          foreach ($unEleve['Inscription'] as $key2 => $uneInscription) {
           foreach ($tousLesAteliers as $key3 => $unAtelier) {
            if ($unAtelier["Atelier"]["periscolaire"] == 1 && $unAtelier["Atelier"]["id"] == $uneInscription["atelier_id"]) {
             $nbLoisirsPourUnEleve++;
            }
           }
          }
          ?>
          <td style="vertical-align:middle;"><?= $nbLoisirsPourUnEleve; ?></td>
          <td style="vertical-align:middle;"><?= $unEleve['Division']['nom']; ?></td>
          <td class="text-center">
            <?= $this->Html->link('Inscrire', array('controller' => 'periscolaires', 'action' => 'forceinscription', $unEleve['User']['id']), array('class' => 'btn btn-success btn-sm', 'escape' => false)); ?>
            <?= $this->Html->link('Voir', array('controller' => 'periscolaires', 'action' => 'suiviEleve', $unEleve['User']['id']), array('class' => 'btn btn-info btn-sm', 'escape' => false)); ?>
          </td>
         </tr>
        <?php endforeach ?>        
        </tbody>
       </table>
      </div>
      <?php if ($role == 'WEBMASTER' || $role == 'ADMIN'): ?>
      <div class="tab-pane fade" id="prof">
       <table class="table table-bordered table-striped table-hover table-condensed">
        <thead>
         <tr>
          <th>Nom</th>
          <th>Prénom</th>
          <th>Nombre de loisir périscolaire</th>
          <th class="text-center">Action</th>
         </tr>
        </thead>
        <tbody class="liste">
        <?php foreach ($tousLesProfs as $key => $unUser): ?>
         <tr>
          <td style="vertical-align:middle;">
           <?php 
            if (strlen($unUser['User']['nom'])>$tablenght)
            {
             echo "<span class='tooltipspan' data-toggle='tooltip' title='".$unUser['User']['nom']."'>".substr($unUser['User']['nom'],0,$tablenght)."..."."</span>";
            }
            else
            {
             echo $unUser['User']['nom'];
            }
           ?>
          </td>
          <td style="vertical-align:middle;">
           <?php 
            if (strlen($unUser['User']['prenom'])>$tablenght)
            {
             echo "<span class='tooltipspan' data-toggle='tooltip' title='".$unUser['User']['prenom']."'>".substr($unUser['User']['prenom'],0,$tablenght)."..."."</span>";
            }
            else
            {
             echo $unUser['User']['prenom'];
            }
           ?>
          </td>
          <?php 
          $nbLoisirsPourUnUser = 0;
          foreach ($unUser['Atelier'] as $key2 => $unAtelier) {
           if ($unAtelier["periscolaire"]) { $nbLoisirsPourUnUser++; }
          } 
          ?>

          <td style="vertical-align:middle;"><?= $nbLoisirsPourUnUser ?></td>
          <td class="text-center">
            <?= $this->Html->link('Voir', array('controller' => 'periscolaires', 'action' => 'suiviProf', $unUser['User']['id']), array('class' => 'btn btn-info btn-sm', 'escape' => false)); ?>
          </td>
         </tr>
        <?php endforeach ?>        
        </tbody>
       </table>
      </div>
      <?php endif ?>      
     </div>
    </fieldset>
   </div>
  </div>
 </div>
</div>
<script>
 $(document).ready(function() {
  var $rows = $(".liste tr");
  $('#inputChercher').keyup(function()
  {
   if ($('#inputChercher').val() != "")
   {
    $($("#inputChercher").parent()[0]).addClass("has-warning");
   }
   else
   {
    $($("#inputChercher").parent()[0]).removeClass("has-warning");    
   }
   var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
   $rows.show().filter(function()
   {
    var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
    return !~text.indexOf(val);
   }).hide();
  });
  $('.listab li a').click(function()
  {
   $('#inputChercher').val("");
   $($("#inputChercher").parent()[0]).removeClass("has-warning");
   var val = $.trim($("").val()).replace(/ +/g, ' ').toLowerCase();
   $rows.show().filter(function()
   {
    var text = $("").text().replace(/\s+/g, ' ').toLowerCase();
    return !~text.indexOf(val);
   }).hide(); 
  });
 });
</script>