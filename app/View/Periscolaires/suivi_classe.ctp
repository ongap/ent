<?php
$role = $this->Session->read('Auth.User.role');
$tablenght = 16;
?>
<div class="row">
 <div class="col-lg-12">
  <div class="panel panel-default">
   <div class="panel-body">
    <fieldset>
     <legend>
      Liste des loisirs périscolaires pour la classe <?= $laClasse['Division']['nom'] ?>
      <form class="form-inline pull-right" action="javascript:" style="position:relative;top:-8px;">
       <div class="form-group">
        <input id="inputChercher" type="text" class="form-control" placeholder="Chercher..." />
       </div>
      </form>
     </legend>
     <table class="table table-bordered table-striped table-hover table-condensed">
      <thead>
       <tr>
        <th>Nom</th>
        <th>Prénom</th>
        <th>Nb périscolaire inscrit</th>
        <th>Nb périscolaire validé</th>
        <th>Classe</th>
        <th class="text-center">Action</th>
       </tr>
      </thead>
      <tbody class="liste">
      <?php foreach ($tousLesEleves as $key => $unEleve): ?>
       <tr>
        <td style="vertical-align:middle;">
         <?php 
          if (strlen($unEleve['User']['nom'])>$tablenght)
          {
           echo "<span class='tooltipspan' data-toggle='tooltip' title='".$unEleve['User']['nom']."'>".substr($unEleve['User']['nom'],0,$tablenght)."..."."</span>";
          }
          else
          {
           echo $unEleve['User']['nom'];
          }
         ?>
        </td>
        <td style="vertical-align:middle;">
         <?php 
          if (strlen($unEleve['User']['prenom'])>$tablenght)
          {
           echo "<span class='tooltipspan' data-toggle='tooltip' title='".$unEleve['User']['prenom']."'>".substr($unEleve['User']['prenom'],0,$tablenght)."..."."</span>";
          }
          else
          {
           echo $unEleve['User']['prenom'];
          }
         ?>
        </td>
        <?php 
        $nbLoisirsPourUnEleve = 0;
        foreach ($unEleve['Inscription'] as $key2 => $uneInscription) {
         foreach ($tousLesAteliers as $key3 => $unAtelier) {
          if ($unAtelier["Atelier"]["periscolaire"] == 1 && $unAtelier["Atelier"]["id"] == $uneInscription["atelier_id"]) {
           $nbLoisirsPourUnEleve++;
          }
         }
        }
        ?>
        <td style="vertical-align:middle;"><?= $nbLoisirsPourUnEleve; ?></td>
        <?php
        $nbCreditationsPourUnEleve = 0;
        foreach ($unEleve['Creditation'] as $key2 => $uneCreditation) {
         foreach ($tousLesAteliers as $key3 => $unAtelier) {
          if ($unAtelier["Atelier"]["periscolaire"] == 1 && $unAtelier["Atelier"]["id"] == $uneCreditation["atelier_id"]) {
           $nbCreditationsPourUnEleve++;
          }
         }
        }
        ?>
        <td style="vertical-align:middle;"><?= $nbCreditationsPourUnEleve; ?></td>
        <td style="vertical-align:middle;"><?= $unEleve['Division']['nom']; ?></td>
        <td class="text-center">
         <?= $this->Html->link('Inscrire', array('controller' => 'periscolaires', 'action' => 'forceinscription', $unEleve['User']['id']), array('class' => 'btn btn-success btn-sm', 'escape' => false)); ?>
         <?= $this->Html->link('Voir', array('controller' => 'periscolaires', 'action' => 'suiviEleve', $unEleve['User']['id']), array('class' => 'btn btn-info btn-sm', 'escape' => false)); ?>
        </td>
       </tr>
      <?php endforeach ?>
      </tbody>
     </table>
    </fieldset>
   </div>
  </div>
 </div>
</div>
<script>
 $(document).ready(function() {
  var $rows = $(".liste tr");
  $('#inputChercher').keyup(function()
  {
   if ($('#inputChercher').val() != "")
   {
    $($("#inputChercher").parent()[0]).addClass("has-warning");
   }
   else
   {
    $($("#inputChercher").parent()[0]).removeClass("has-warning");    
   }
   var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
   $rows.show().filter(function()
   {
    var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
    return !~text.indexOf(val);
   }).hide();
  });
  $('.listab li a').click(function()
  {
   $('#inputChercher').val("");
   $($("#inputChercher").parent()[0]).removeClass("has-warning");
   var val = $.trim($("").val()).replace(/ +/g, ' ').toLowerCase();
   $rows.show().filter(function()
   {
    var text = $("").text().replace(/\s+/g, ' ').toLowerCase();
    return !~text.indexOf(val);
   }).hide(); 
  });
 });
</script>