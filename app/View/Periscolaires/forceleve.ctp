<?php
$role = $this->Session->read('Auth.User.role');
$tablenght = 16;
?>
<div class="row">
 <div class="col-lg-12">
  <div class="panel panel-default">
   <div class="panel-body">
    <fieldset>
     <legend>
       Gestion des inscriptions
     </legend>
     <ul class="nav nav-pills nav-justified listab" style="margin-bottom:8px;">
      <li class="active">
       <a href="#eleve" data-toggle="tab">Par élèves</a>
      </li>
      <li>
       <a href="#classe" data-toggle="tab">Par classes</a>
      </li>
      <li>
      </li>
      <li>
       <form class="form-inline" action="javascript:">
        <div class="form-group">
         <input id="inputChercher" type="text" class="form-control" placeholder="Chercher..." />
        </div>
       </form>
      </li>
     </ul>
     <div class="tab-content">
      <div class="tab-pane fade in active" id="eleve">
       <table class="table table-bordered table-striped table-hover table-condensed">
        <thead>
         <tr>
          <th>Nom</th>
          <th>Prénom</th>
          <th>Classe</th>
          <th class="text-center">Action</th>
         </tr>
        </thead>
        <tbody class="liste">
        <?php foreach ($tousLesUsers as $key => $unUser): ?>
         <?php if ($unUser['Role']['nom_code'] == 'ELEVE' ): ?>
         <tr>
          <td style="vertical-align:middle">
           <?php 
            if (strlen($unUser["User"]["nom"])>$tablenght)
            {
             echo "<span class='tooltipspan' data-toggle='tooltip' title='".$unUser['User']['nom']."'>".substr($unUser['User']['nom'],0,$tablenght)."..."."</span>";
            }
            else
            {
             echo $unUser["User"]["nom"];
            }
           ?>
          </td>
          <td style="vertical-align:middle">
           <?php 
            if (strlen($unUser["User"]["prenom"])>$tablenght)
            {
             echo "<span class='tooltipspan' data-toggle='tooltip' title='".$unUser['User']['prenom']."'>".substr($unUser['User']['prenom'],0,$tablenght)."..."."</span>";
            }
            else
            {
             echo $unUser["User"]["prenom"];
            }
           ?>
          </td>
          <td style="vertical-align:middle">
           <?php 
            if (strlen($unUser["Division"]["nom"])>$tablenght)
            {
             echo "<span class='tooltipspan' data-toggle='tooltip' title='".$unUser['Division']['nom']."'>".substr($unUser['Division']['nom'],0,$tablenght)."..."."</span>";
            }
            else
            {
             echo $unUser["Division"]["nom"];
            }
           ?>
          </td>
          <td style="vertical-align:middle" class="text-center">
           <?= $this->Html->link('Inscrire', array('controller' => 'periscolaires', 'action' => 'forceinscription', $unUser['User']['id']), array('class' => 'btn btn-success btn-sm', 'escape' => false)); ?>
          </td>
         </tr>
         <?php endif ?>
        <?php endforeach ?>
        </tbody>
       </table>
      </div>
      <div class="tab-pane fade" id="classe">
       <table class="table table-bordered table-striped table-hover table-condensed">
        <thead>
         <tr>
          <th>Nom</th>
          <th class="text-center">Action</th>
         </tr>
        </thead>
        <tbody class="liste">
        <?php foreach ($toutesLesClasses as $key => $uneClasse): ?>
         <tr>
          <td style="vertical-align:middle">
           <?php 
            if (strlen($uneClasse['Division']['nom'])>$tablenght)
            {
             echo "<span class='tooltipspan' data-toggle='tooltip' title='".$uneClasse['Division']['nom']."'>".substr($uneClasse['Division']['nom'],0,$tablenght)."..."."</span>";
            }
            else
            {
             echo $uneClasse['Division']['nom'];
            }
           ?>
          </td>
          <td style="vertical-align:middle" class="text-center">
           <?= $this->Html->link('Inscrire', array('controller' => 'periscolaires', 'action' => 'forceInscriptionClasse', $uneClasse['Division']['id']), array('class' => 'btn btn-success btn-sm', 'escape' => false)); ?>
          </td>
         </tr>
        <?php endforeach ?>
        </tbody>
       </table>
      </div>
     </div>
    </fieldset>
   </div>
  </div>
 </div>
</div>
<script>
 $(document).ready(function() {
  var $rows = $(".liste tr");
  $('#inputChercher').keyup(function()
  {
   if ($('#inputChercher').val() != "")
   {
    $($("#inputChercher").parent()[0]).addClass("has-warning");
   }
   else
   {
    $($("#inputChercher").parent()[0]).removeClass("has-warning");    
   }
   var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
   $rows.show().filter(function()
   {
    var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
    return !~text.indexOf(val);
   }).hide();
  });
  $('.listab li a').click(function()
  {
   $('#inputChercher').val("");
   $($("#inputChercher").parent()[0]).removeClass("has-warning");
   var val = $.trim($("").val()).replace(/ +/g, ' ').toLowerCase();
   $rows.show().filter(function()
   {
    var text = $("").text().replace(/\s+/g, ' ').toLowerCase();
    return !~text.indexOf(val);
   }).hide(); 
  });
 });
</script>