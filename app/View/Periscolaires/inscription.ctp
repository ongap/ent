<?php
  $x=0;
?>

<div class="row">
 <?= $this->Session->flash('flash', array('element' => 'failure')) ?>
 <?php $nomJour = array('Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'); ?>
 <?php foreach ($tousLesAteliers as $key => $unAtelier): ?>
 <div class="col-lg-6">
 <?php 
  $cleInscription = -1;
  $type = "default";
  $c = "";
  if (count($InscriptionCourante) != 0) {
   foreach ($InscriptionCourante as $key => $value) {
    if ($value['Atelier']['jour'].' '.$value['Horaire']['heureDebut'] == $unAtelier['Atelier']['jour'].' '.$unAtelier['Horaire']['heureDebut']) {
     $c = 1;
    }
   }
  }
  foreach ($unAtelier['Inscription'] as $key => $value) {
   if ($value['user_id'] == $this->Session->read('Auth.User.id')) {
    $cleInscription = $key;
    $type = "success";
   }
  }
 ?>
  <div class="panel panel-<?= $type; ?>">
   <div class="panel-heading"><b><?= $unAtelier['Atelier']['nom'] ?></b></div>
   <ul class="list-group">
    <li class="list-group-item">
      <span class="label label-info"><?= $unAtelier['Matiere']['nom'] ?></span>
      <span class="label label-warning"><?= $unAtelier['Salle']['nom'] ?></span>
      <span class="label label-default">Nombre de places occupées : <?= count($unAtelier['Inscription'])."/".$unAtelier['Atelier']['nombre_place']; ?></span>
    </li>
   </ul>
   <div class="panel-body">
    <p><?= $unAtelier['Atelier']['description'] ?></p>
   </div>
   <ul class="list-group">
    <li class="list-group-item">
     <img src="<?php echo $this->webroot; ?>img/blue-user-icon.png" width="35px" class="img-polaroid img-rounded" />
     <?= $unAtelier['User']['civilite'] ?>. <?= $unAtelier['User']['nom'] ?>
     <span class="pull-right" style="font-size:14px;position:relative;top:8px;font-style:italic;margin-right:5px;">
      <?= $nomJour[$this->Time->format( $unAtelier['Atelier']['jour'], '%w')] ?>
      <?= $this->Time->format( $unAtelier['Atelier']['jour'].' '.$unAtelier['Horaire']['heureDebut'], '%d/%m/%Y %H:%M ') ?>
     </span>
    </li>
    <?php if ($c == "" && count($unAtelier['Inscription']) < $unAtelier['Atelier']['nombre_place'] AND $unAtelier['Atelier']['jour'] > date('Y-m-d')) { ?>
    <li class="list-group-item">
     <?= $this->Html->link('Inscription', array('controller' => 'periscolaires', 'action' => 'inscription', $unAtelier['Atelier']['id']), array('class' => 'btn btn-success', "style" => "width:100%")); ?>
    </li>
    <?php } else if ($cleInscription == -1) { ?>
     <?= "<!-- Pas de bouton -->"; ?>
    <?php } else { if($parametre['Parametre']['desinEleve']) { ?>
    <li class="list-group-item">
      <?= $this->Html->link('Désinscription', array('controller' => 'periscolaires', 'action' => 'desinscription', $unAtelier['Inscription'][$cleInscription]['id']), array('class' => 'btn btn-danger', "style" => "width:100%")); ?>
    </li>
    <?php } } ?>
   </ul>
  </div>
 </div>
 <?php endforeach ?>
</div>