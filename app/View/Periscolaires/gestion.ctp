<?php
$tablenght = 12;
$nomJour = array('Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi');
?>
<div class="row">
 <div class="col-lg-12">
  <div class="panel panel-default">
   <div class="panel-body">
    <fieldset>
     <legend>
      <?php if(!isset($unAtelier)): ?>   
       Gestion des loisirs périscolaire :
       <?= $this->Html->link('Ajouter', array('controller' => 'periscolaires', 'action' => 'add'), array('class' => 'btn btn-success pull-right', 'style' => 'position:relative;top:-8px;')); ?>
      <?php else: ?>
       Gestion de l'activité periscolaire  <?= $unAtelier["Atelier"]["nom"]; ?>
      <?php endif; ?>
     </legend>
     <table class="table table-bordered table-condensed">
      <tbody>
       <tr>
        <td>Pas encore validé</td>
        <td class="warning">Partiellement validé</td>
        <td class="info">Complétement validé</td>
       </tr>
       <tr>
        <td colspan="3">Périodique : <i class="fa fa-calendar-o"></i></td>
       </tr>
       <tr>
        <td colspan="3">Hebdomadaire : <i class="fa fa-refresh"></i></td>
       </tr>
       <tr>
        <td colspan="3">Ponctuel : <i class="fa fa-exclamation"></i></td>
       </tr>
      </tbody>
     </table>     
     <?php if(!isset($unAtelier)): ?>   
     <div>
      <ul class="nav nav-tabs" role="tablist">
       <li role="presentation" <?php if($type == 'tableau') echo 'class="active"'; ?>><a href="#tableau" aria-controls="tableau" role="tab" data-toggle="tab">Tableau</a></li>
       <li role="presentation" <?php if($type == 'calendrier') echo 'class="active"'; ?>><a href="#calendrier" aria-controls="calendrier" role="tab" data-toggle="tab">Calendrier</a></li>
      </ul>
      <div class="tab-content">
       <div role="tabpanel" class="tab-pane <?php if($type == 'tableau') echo 'active'; ?>" id="tableau">  
        <table class="table table-striped table-bordered table-hover table-condensed" style="table-layout:fixed;margin-bottom:0px;">
         <tr>
          <th><a href="<?= $this->Html->url(array('controller' => 'periscolaires', 'action' => 'gestion', strftime('%Y' ,strtotime('-1 day', strtotime($semaine[0]))), strftime('%m' ,strtotime('-1 day', strtotime($semaine[0]))), strftime('%d' ,strtotime('-1 day', strtotime($semaine[0]))))) ?>" class="btn btn-primary btn-xs" style="width:100%;font-weight:800;">&laquo;</a></th>
          <th colspan="7" class="text-center">Périscolaires du <?= strftime('%A %e %B %Y', strtotime($semaine[0])); ?> au <?= strftime('%A %e %B %Y', strtotime($semaine[6])); ?></th>
          <th><a href="<?= $this->Html->url(array('controller' => 'periscolaires', 'action' => 'gestion', strftime('%Y' ,strtotime('+1 day', strtotime($semaine[6]))), strftime('%m' ,strtotime('+1 day', strtotime($semaine[6]))), strftime('%d' ,strtotime('+1 day', strtotime($semaine[6]))))) ?>" class="btn btn-primary btn-xs" style="width:100%;font-weight:800;">&raquo;</a></th>
         </tr>
        </table>
        <table class="table table-bordered table-hover table-condensed">
         <thead>
          <tr>
           <th>Nom</th>
           <th>Matière</th>
           <th>Description</th>
           <th>Date et Heure</th>
           <th>Animateur</th>
           <th>Lieux</th>
           <th>Classe(s)</th>
           <th>Places</th>
           <th class="text-center">Actions</th>
          </tr>
         </thead>

         <tbody>
         <?php foreach ($tousLesAteliers as $key => $unAtelier): ?>

         <?php if (count($unAtelier['Inscription']) > count($unAtelier['Creditation'])): ?>
          <tr class="warning">
         <?php elseif (count($unAtelier['Inscription']) == count($unAtelier['Creditation']) && count($unAtelier['Inscription']) != 0): ?>
          <tr class="info">
         <?php else: ?>
          <tr>
         <?php endif ?>
           <td style="vertical-align:middle;">
           <?php if ($unAtelier['Atelier']['periode_id'] > 0): ?>
            <i class="fa fa-calendar-o"></i>
           <?php elseif ($unAtelier['Atelier']['hebdomadaire'] > 1): ?>
            <i class="fa fa-refresh"></i>
           <?php else: ?>
            <i class="fa fa-exclamation"></i>
           <?php endif ?>
            <?php if (strlen($unAtelier["Atelier"]["nom"])>$tablenght): ?>
             <?= '<span class="tooltipspan" data-toggle="tooltip" title="'.str_replace(array("'", "\""), array("&apos;", "&quot;"),$unAtelier['Atelier']['nom']).'">'.substr($unAtelier['Atelier']['nom'],0,$tablenght).'...'.'</span>'; ?>
            <?php else: ?>
              <?= $unAtelier["Atelier"]["nom"]; ?>
            <?php endif ?>

           </td>
           <td style="vertical-align:middle;">
            <?php if (strlen($unAtelier["Matiere"]["nom"])>$tablenght): ?>
             <?= '<span class="tooltipspan" data-toggle="tooltip" title="'.str_replace(array("'", "\""), array("&apos;", "&quot;"),$unAtelier['Matiere']['nom']).'">'.substr($unAtelier['Matiere']['nom'],0,$tablenght).'...'.'</span>'; ?>
            <?php else: ?>
              <?= $unAtelier["Matiere"]["nom"]; ?>
            <?php endif ?>
           </td>
           <td style="vertical-align:middle;">
            <?php if (strlen($unAtelier["Atelier"]["description"])>$tablenght): ?>
             <?= '<span class="tooltipspan" data-toggle="tooltip" title="'.strip_tags(str_replace(array("'", "\""), array("&apos;", "&quot;"),$unAtelier["Atelier"]["description"])).'">'.substr($unAtelier['Atelier']['description'],0,$tablenght).'...'.'</span>'; ?>
            <?php else: ?>
              <?= $unAtelier["Atelier"]["description"]; ?>
            <?php endif ?>
           </td>
           <td style="vertical-align:middle;">
            <?= $nomJour[$this->Time->format($unAtelier['Atelier']['jour'], '%w')] ?>
            <?= $this->Time->format( $unAtelier['Atelier']['jour'].' '.$unAtelier['Horaire']['heureDebut'], '%d/%m/%Y %H:%M ') ?>
           </td>

           <td style="vertical-align:middle;"><?= $unAtelier['User']['civilite'] ?>. <?= $unAtelier['User']['nom'] ?></td>
           <td style="vertical-align:middle;"><?= $unAtelier['Salle']['nom'] ?></td>
           <td style="vertical-align:middle;">
            <?php foreach ($unAtelier['Division'] as $key => $uneClasse): ?>
             <?php if ($key != count($unAtelier['Division'])-1): ?>
              <?= $uneClasse['nom'].", " ?>
             <?php else: ?>
              <?= $uneClasse['nom'] ?>
             <?php endif ?>
            <?php endforeach ?>
           </td>
           <td style="vertical-align:middle;"><?= count($unAtelier['Inscription'])."/".$unAtelier['Atelier']['nombre_place'] ?></td>
           <td style="vertical-align:middle;" class="text-center">
            <div class="btn-group">
             <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
              Actions <span class="caret"></span>
             </button>
             <ul class="dropdown-menu" role="menu">
              <li class="list-group-item-info">
               <?= $this->Html->link('Validation', array('controller' => 'periscolaires', 'action' => 'validation', $unAtelier['Atelier']['id']), array('escape' => false, 'title' => "Valider")); ?>
              </li>
              <li class="list-group-item-success">
               <?= $this->Html->link('Modifier', array('controller' => 'periscolaires', 'action' => 'update', $unAtelier['Atelier']['id']), array('escape' => false, 'title' => "Modifier")); ?>
              </li>
              <li class="list-group-item-danger">
               <?= $this->Html->link('Supprimer', array('controller' => 'periscolaires', 'action' => 'remove', $unAtelier['Atelier']['id']), array('escape' => false, 'title' => "Supprimer" ), "Voulez-vous vraiment supprimer ce periscolaire ?"); ?>
              </li>
              <?php if ($unAtelier['Atelier']['suspendu'] == 0): ?>
               <li class="list-group-item-warning">
                <?= $this->Html->link('Suspendre', array('controller' => 'periscolaires', 'action' => 'suspendre', $unAtelier['Atelier']['id']), array('escape' => false, 'title' => "Suspendre")); ?>        
               <li>
              <?php else: ?>
               <li class="list-group-item-success">
                <?= $this->Html->link('Reconduire', array('controller' => 'periscolaires', 'action' => 'suspendre', $unAtelier['Atelier']['id']), array('escape' => false, 'title' => "Reconduire")); ?>
               <li> 
              <?php endif ?>
             </ul>
            </div>
           </td>

         <?php endforeach ?>
         </tbody>
        </table>
       </div>
       <div role="tabpanel" class="tab-pane <?php if($type == 'calendrier') echo 'active'; ?>" id="calendrier">
        <table class="table table-striped table-bordered table-condensed" style="table-layout:fixed;margin-bottom:0px">
         <tr>
          <th><a href="<?= $this->Html->url(array('controller' => 'periscolaires', 'action' => 'gestion', strftime('%Y' ,strtotime('-1 day', strtotime($semaine[0]))), strftime('%m' ,strtotime('-1 day', strtotime($semaine[0]))), strftime('%d' ,strtotime('-1 day', strtotime($semaine[0]))), 'calendrier')) ?>" class="btn btn-primary btn-xs" style="width:100%;font-weight:800;">&laquo;</a></th>
          <th colspan="8" class="text-center">
           <?= ucfirst(strftime('%B %Y', strtotime($semaine[0]))); ?>
          </th>
          <th><a href="<?= $this->Html->url(array('controller' => 'periscolaires', 'action' => 'gestion', strftime('%Y' ,strtotime('+1 day', strtotime($semaine[6]))), strftime('%m' ,strtotime('+1 day', strtotime($semaine[6]))), strftime('%d' ,strtotime('+1 day', strtotime($semaine[6]))), 'calendrier')) ?>" class="btn btn-primary btn-xs" style="width:100%;font-weight:800;">&raquo;</a></th>
         </tr>
        </table>
        <table class="table table-striped table-bordered table-hover table-condensed" style="table-layout:fixed">
         <tr>
          <th></th>
          <?php foreach ($semaine as $key => $value): ?>
          <th class="text-center">
           <?= ucfirst(strftime('%A %e', strtotime($value))); ?>
          </th> 
          <?php endforeach; ?>
         </tr>
         <?php foreach ($horaires as $key => $value): ?>
         <tr>
          <th class="text-center">
           <?= $value["Horaire"]["heureDebut"]; ?><br/>
           <?= $value["Horaire"]["heureFin"]; ?>
          </th>
          <?php foreach ($semaine as $key2 => $value2): ?>
          <td>
           <ul class="list-unstyled">
           <?php 
           foreach ($tousLesAteliers as $key3 => $value3) {
            if ($value3["Atelier"]["jour"] == $value2 && $value3["Atelier"]["horaire_id"] == $value["Horaire"]["id"]) {
            ?>
             <li><a href="<?= $this->Html->url(array('controller' => 'periscolaires', 'action' => 'gestion', 0, 0, 0, 0, $value3['Atelier']['id'])) ?>"><?= $value3["Atelier"]["nom"] ?></a></li>
            <?php
            }
           }
           ?>
           </ul>
          </td> 
          <?php endforeach; ?>
         </tr>
         <?php endforeach; ?>
        </table>
       </div>
      </div>
     </div>
     <?php else: ?>
      <table class="table table-bordered table-hover table-condensed">
       <thead>
        <tr>
         <th>Nom</th>
         <th>Matière</th>
         <th>Description</th>
         <th>Date et Heure</th>
         <th>Animateur</th>
         <th>Lieux</th>
         <th>Classe(s)</th>
         <th>Places</th>
         <th class="text-center">Actions</th>
        </tr>
       </thead>
       <tbody>
       <?php if (count($unAtelier['Inscription']) > count($unAtelier['Creditation'])): ?>
        <tr class="warning">
       <?php elseif (count($unAtelier['Inscription']) == count($unAtelier['Creditation']) && count($unAtelier['Inscription']) != 0): ?>
        <tr class="info">
       <?php else: ?>
        <tr>
       <?php endif ?>
         <td style="vertical-align:middle;">
         <?php if ($unAtelier['Atelier']['periode_id'] > 0): ?>
          <i class="fa fa-calendar-o"></i>
         <?php elseif ($unAtelier['Atelier']['hebdomadaire'] > 1): ?>
          <i class="fa fa-refresh"></i>
         <?php else: ?>
          <i class="fa fa-exclamation"></i>
         <?php endif ?>
          <?php if (strlen($unAtelier["Atelier"]["nom"])>$tablenght): ?>
           <?= '<span class="tooltipspan" data-toggle="tooltip" title="'.str_replace(array("'", "\""), array("&apos;", "&quot;"),$unAtelier['Atelier']['nom']).'">'.substr($unAtelier['Atelier']['nom'],0,$tablenght).'...'.'</span>'; ?>
          <?php else: ?>
            <?= $unAtelier["Atelier"]["nom"]; ?>
          <?php endif ?>

         </td>
         <td style="vertical-align:middle;">
          <?php if (strlen($unAtelier["Matiere"]["nom"])>$tablenght): ?>
           <?= '<span class="tooltipspan" data-toggle="tooltip" title="'.str_replace(array("'", "\""), array("&apos;", "&quot;"),$unAtelier['Matiere']['nom']).'">'.substr($unAtelier['Matiere']['nom'],0,$tablenght).'...'.'</span>'; ?>
          <?php else: ?>
            <?= $unAtelier["Matiere"]["nom"]; ?>
          <?php endif ?>
         </td>
         <td style="vertical-align:middle;">
          <?php if (strlen($unAtelier["Atelier"]["description"])>$tablenght): ?>
           <?= '<span class="tooltipspan" data-toggle="tooltip" title="'.strip_tags(str_replace(array("'", "\""), array("&apos;", "&quot;"),$unAtelier["Atelier"]["description"])).'">'.substr($unAtelier['Atelier']['description'],0,$tablenght).'...'.'</span>'; ?>
          <?php else: ?>
            <?= $unAtelier["Atelier"]["description"]; ?>
          <?php endif ?>
         </td>
         <td style="vertical-align:middle;">
          <?= $nomJour[$this->Time->format($unAtelier['Atelier']['jour'], '%w')] ?>
          <?= $this->Time->format( $unAtelier['Atelier']['jour'].' '.$unAtelier['Horaire']['heureDebut'], '%d/%m/%Y %H:%M ') ?>
         </td>

         <td style="vertical-align:middle;"><?= $unAtelier['User']['civilite'] ?>. <?= $unAtelier['User']['nom'] ?></td>
         <td style="vertical-align:middle;"><?= $unAtelier['Salle']['nom'] ?></td>
         <td style="vertical-align:middle;">
          <?php foreach ($unAtelier['Division'] as $key => $uneClasse): ?>
           <?php if ($key != count($unAtelier['Division'])-1): ?>
            <?= $uneClasse['nom'].", " ?>
           <?php else: ?>
            <?= $uneClasse['nom'] ?>
           <?php endif ?>
          <?php endforeach ?>
         </td>
         <td style="vertical-align:middle;"><?= count($unAtelier['Inscription'])."/".$unAtelier['Atelier']['nombre_place'] ?></td>
         <td style="vertical-align:middle;" class="text-center">
          <div class="btn-group">
           <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            Actions <span class="caret"></span>
           </button>
           <ul class="dropdown-menu" role="menu">
            <li class="list-group-item-info">
             <?= $this->Html->link('Validation', array('controller' => 'periscolaires', 'action' => 'validation', $unAtelier['Atelier']['id']), array('escape' => false, 'title' => "Valider")); ?>
            </li>
            <li class="list-group-item-success">
             <?= $this->Html->link('Modifier', array('controller' => 'periscolaires', 'action' => 'update', $unAtelier['Atelier']['id']), array('escape' => false, 'title' => "Modifier")); ?>
            </li>
            <li class="list-group-item-danger">
             <?= $this->Html->link('Supprimer', array('controller' => 'periscolaires', 'action' => 'remove', $unAtelier['Atelier']['id']), array('escape' => false, 'title' => "Supprimer" ), "Voulez-vous vraiment supprimer cet atelier ?"); ?>
            </li>
            <?php if ($unAtelier['Atelier']['suspendu'] == 0): ?>
             <li class="list-group-item-warning">
              <?= $this->Html->link('Suspendre', array('controller' => 'periscolaires', 'action' => 'suspendre', $unAtelier['Atelier']['id']), array('escape' => false, 'title' => "Suspendre")); ?>        
             <li>
            <?php else: ?>
             <li class="list-group-item-success">
              <?= $this->Html->link('Reconduire', array('controller' => 'periscolaires', 'action' => 'suspendre', $unAtelier['Atelier']['id']), array('escape' => false, 'title' => "Reconduire")); ?>
             <li> 
            <?php endif ?>
           </ul>
          </div>
         </td>
       </tbody>
      </table>
     <?php endif; ?>
    </fieldset>
   </div>
  </div>
 </div>
</div>











<?php /*
<div class="span9">
 <div class="row-fluid">
  <div class="span12">
   <div class="thumbnail form-modal">
    <div class="modal-header">
     <h3> Gestion des Accueil de loisirs périscolaire :
      <?= $this->Html->link('Ajouter un accueil', array('controller' => 'periscolaires', 'action' => 'add'), array('class' => 'btn btn-success pull-right')); ?>

     </h3>
    </div>


  <div class="modal-body modal-display-all-body" style="height:100%;">
   <table style="table-layout:fixed;margin-bottom:60px;" class="table table-bordered table-striped table-hover table-condensed no-more-tables">
    <thead>
     <tr>
      <th >Nom</th>
      <th >Matière</th>
      <th >Description</th>
      <th >Date et Heure</th>
      <th >Animateur</th>
      <th >Lieux</th>
      <th >Classe(s)</th>
      <th >Places</th>
      <th style="text-align:center;width:15%;">Actions</th>
     </tr>
    </thead>
    <tbody>

      <?php $nomJour = array('Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'); ?>
      <?php foreach ($tousLesAteliers as $key => $unAtelier): ?>

      <?php
      $status = "Ponctuel";
      if ($unAtelier['Atelier']['periode_id'] > 0) {
        $status = "Périodique";
      }
      elseif ($unAtelier['Atelier']['hebdomadaire'] > 1) {
        $status = "Hebdomadaire";
      }
      ?>

       <tr>
        <td><?= $unAtelier['Atelier']['nom'] ?></td>
        <td><?= $unAtelier['Matiere']['nom'] ?></td>
        <td><?= $unAtelier['Atelier']['description'] ?></td>
        <td>
          <?= $nomJour[$this->Time->format( $unAtelier['Atelier']['jour'], '%w')] ?>
          <?= $this->Time->format( $unAtelier['Atelier']['jour'].' '.$unAtelier['Horaire']['heureDebut'], '%d/%m/%Y %H:%M ') ?>
          <span class="label label-info" style="float:left;">
            <?= $status ?> 
          </span>

        </td>
        <td><?= $unAtelier['User']['civilite'] ?>. <?= $unAtelier['User']['nom'] ?></td>
        <td><?= $unAtelier['Salle']['nom'] ?></td>
        <td>




        <?php foreach ($unAtelier['Division'] as $key => $uneClasse): ?>
           <?= $uneClasse['nom'].", " ?>
        <?php endforeach ?>

        </td>
        <td><?= count($unAtelier['Inscription'])."/".$unAtelier['Atelier']['nombre_place'] ?></td>

        <td style="text-align:right;width:15%">
          <?= $this->Html->link('<i class="icon icon-zoom-in"></i>', array('controller' => 'periscolaires', 'action' => 'validation', $unAtelier['Atelier']['id']), array('class' => 'btn btn-info btn-small', 'escape' => false, 'title' => "Valider")); ?>
          <?= $this->Html->link('<i class="icon-pencil"></i>', array('controller' => 'periscolaires', 'action' => 'update', $unAtelier['Atelier']['id']), array('class' => 'btn btn-small btn-success', 'escape' => false, 'title' => "Modifier")); ?>
          <?= $this->Html->link('<i class="icon icon-trash"></i>', array('controller' => 'periscolaires', 'action' => 'remove', $unAtelier['Atelier']['id']), array('class' => 'btn btn-danger btn-small', 'escape' => false, 'title' => "Supprimer" )); ?>

          <?php if ($unAtelier['Atelier']['suspendu'] == 0): ?>
            <?= $this->Html->link('Suspendre', array('controller' => 'periscolaires', 'action' => 'suspendre', $unAtelier['Atelier']['id']), array('class' => 'btn btn-danger btn-small', 'escape' => false, 'title' => "Suspendre" )); ?>        
          <?php else: ?>
            <?= $this->Html->link('Reconduire', array('controller' => 'periscolaires', 'action' => 'suspendre', $unAtelier['Atelier']['id']), array('class' => 'btn btn-success btn-small', 'escape' => false, 'title' => "Suspendre" )); ?>
          <?php endif ?>

        </td>
       </tr>
      <?php endforeach ?>

    </tbody>
   </table>
  </div>

   </div>
  </div>
 </div>
</div>
*/?>