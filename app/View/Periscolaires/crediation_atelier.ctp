<div class="row">
 <div class="col-lg-12">
  <div class="panel panel-default">
   <div class="panel-body">
    <fieldset>
     <legend>
      Validation du loisir périscolaire
      <?= $this->Html->link('Modifier',array('controller' => 'periscolaires', 'action' => 'crediationAtelierUpdate', $ateliersCourant[0]['Atelier']['id']), array('class' => 'btn btn-success pull-right', 'style' => 'position:relative;top:-8px;')); ?>
     </legend>
     <table class="table table-bordered table-striped table-condensed">
      <thead>
       <tr>
        <th>Nom</th>
        <th>Prénom</th>
        <th>Classe</th>
        <th style="text-align:center;">Présence</th>
        <th style="text-align:center;">Valider</th>
        <th style="text-align:center;">Commentaire</th>
       </tr>
      </thead>
      <tbody>
      <?php foreach ($ateliersCourant as $key => $crediter): ?>
       <tr>
        <td><?= $crediter['User']['nom'] ?></td>
        <td><?= $crediter['User']['prenom'] ?></td>
        <td><?= $crediter['User']['Division']['nom'] ?></td>
       <?php if ($crediter['Creditation']['presence']): ?>
        <td style="text-align:center;">Oui</td>
       <?php else: ?>
        <td style="text-align:center;">Non</td>
       <?php endif ?>
        <td style="text-align:center;"><?= $crediter['Creditation']['valid'] ?></td>
        <td style="text-align:center;"> <?= $crediter['Creditation']['commentaire'] ?> <span style="opacity: 0.0;">.</span></td>
       </tr>
      <?php endforeach ?>
      </tbody>
     </table>
    </fieldset>
   </div>
  </div>
 </div>
</div>