
<div class="row">
 <div class="col-lg-12">
  <div class="panel panel-default">
   <div class="panel-body">
  <?php
   $LabelOptions = array("class" => "col-lg-3 control-label");
   $options = array
   (
    "class" => "form-horizontal",
    "inputDefaults" => array
    (
     "class" => "form-control",
     "div" => array("class" => "form-group"),
     "label" => $LabelOptions,
     "between" => "<div class='col-lg-9'>",
     "after" => "</div>",
     'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block')),
     "format" => array('before', 'label', 'between', 'input', 'error', 'after')
    ),
   );
   echo $this->Form->create("Atelier", $options);
  ?>
   <fieldset>
    <legend>
      Ajouter un loisir périscolaire :
    </legend>
    <?php
     $options = array
     (
      "placeholder" => "Nom de l'atelier",
      "label" => array_merge($LabelOptions, array("text" => "Nom de l'atelier"))
     );
     if ($this->Form->error("nom")) { $options["div"] = "form-group has-error"; }
     echo $this->Form->input("nom", $options);
     $options = array
     (
      "label" => array_merge($LabelOptions, array("text" => "Animateur"))
     );
     if ($this->Form->error("user_id")) { $options["div"] = "form-group has-error"; }
     echo $this->Form->input("user_id", $options);
     $options = array
     (
      "label" => array_merge($LabelOptions, array("text" => "Activité"))
     );
     if ($this->Form->error("matiere_id")) { $options["div"] = "form-group has-error"; }
     echo $this->Form->input("matiere_id", $options); 
     $options = array
     (
      "label" => array_merge($LabelOptions, array("text" => "Horaire"))
     );
     if ($this->Form->error("horaire_id")) { $options["div"] = "form-group has-error"; }
     echo $this->Form->input("horaire_id", $options); 


     /*
     AFFICHAGE EN SELECT DE CAKEPHP
     $options = array
     (
      "between" => "<div class='col-lg-9 toEmpty'>",
      "label" => array_merge($LabelOptions, array("text" => "Jour (Date du premier atelier pour une période)")),
      "dateFormat" => "DMY"
     );
     if ($this->Form->error("jour")) { $options["div"] = "form-group has-error"; }
     echo $this->Form->input("jour", $options);
     */
     $options = array
     (
      "class" => "form-control inputDatePicker",
      "type" => "text",
      "between" => "<div class='col-lg-9 toEmpty'>",
      "label" => array_merge($LabelOptions, array("text" => "Jour (Date du premier atelier pour une période)")),
      "dateFormat" => "YMD"
     );
     if ($this->Form->error("jour")) { $options["div"] = "form-group has-error"; }
     echo $this->Form->input("jour", $options);     
    ?>
    <div class="form-group">
     <div class="col-xs-3">
      <ul class="nav nav-tabs tabs-left">
       <li class="active"><a href="#date" data-toggle="tab"><b>Date</b></a></li>
       <li><a href="#periode" data-toggle="tab"><b>Période</b></a></li>
      </ul>
     </div>
     <div class="col-xs-9">
      <div class="tab-content">
       <div class="tab-pane active" id="date">
       <?php
        $options = array
        (
         "value" => 1,
         "between" => "<div class='col-lg-12'>",
         "after" => "<p class='help-block'>hebdomadaire (nombre de répétition)</p></div>",
         "format" => array('before', 'between', 'input', 'error', 'after')
        );
        if ($this->Form->error("hebdomadaire")) { $options["div"] = "form-group has-error"; }
        echo $this->Form->input('hebdomadaire', $options);
       ?>
       </div>
       <div class="tab-pane" id="periode">
       <?php
        $options = array
        (
         "empty" => "(choisissez)",
         "between" => "<div class='col-lg-12'>",
         "format" => array('before', 'between', 'input', 'error', 'after')
        );
        if ($this->Form->error("periode_id")) { $options["div"] = "form-group has-error"; }
        echo $this->Form->input('periode_id', $options);
       ?>
       </div>
      </div>
     </div>  
    </div>
    <?php
     $options = array
     (
      "label" => array_merge($LabelOptions, array("text" => "Lieux de pratique"))
     );
     if ($this->Form->error("salle_id")) { $options["div"] = "form-group has-error"; }
     echo $this->Form->input("salle_id", $options);
     $options = array
     (
      "label" => array_merge($LabelOptions, array("text" => "Nombre de place")),
      "value" => 0
     );
     if ($this->Form->error("nombre_place")) { $options["div"] = "form-group has-error"; }
     echo $this->Form->input("nombre_place", $options);
     $options = array
     (
      'required' => false,
      "label" => array_merge($LabelOptions, array("text" => "Description"))
     );
     if ($this->Form->error("description")) { $options["div"] = "form-group has-error"; }
     echo $this->Form->input("description", $options); 
     $options = array
     (
      "label" => array_merge($LabelOptions, array("text" => "Classes")),
      "multiple" => true,
      "after" => "<p class='help-block'>CTRL enfoncé pour selectionner plusieurs classes</p></div>"
     );
     if ($this->Form->error("Atelier.Division")) { $options["div"] = "form-group has-error"; }
     echo $this->Form->input("Atelier.Division", $options);
    ?>
    <div class="form-group">
     <div class="col-lg-3 col-lg-offset-3">
      <?= $this->Html->link("Retour", array("action" => "gestion"), array("class" => "btn btn-danger col-xs-12 col-sm-12", "style" => "margin-bottom:4px;")); ?>
     </div>
     <?php
      $options = array
      (
       "class" => "btn btn-success col-xs-12 col-sm-12 ",
       "before" => "<div class='col-lg-3 col-lg-offset-3'>",
       "after" => "</div>"
      );
      echo $this->Form->submit("Ajouter", $options);
     ?>
    </div>
   </fieldset>
   <?= $this->Form->end(); ?>
   <script>
    $(document).ready(function() {
     // A COMMENTER POUR ENLEVER DATEPICKER
     $.datepicker.setDefaults( $.datepicker.regional[ "fr" ] );
     $(".inputDatePicker").datepicker({dateFormat: "yy-mm-dd"});
     // FIN A COMMENTER POUR ENLEVER DATEPICKER
     tinymce.init({
       selector: "textarea",
       plugins: [
       "advlist autolink lists link image charmap print preview anchor",
       "searchreplace visualblocks code fullscreen",
       "insertdatetime media table contextmenu paste"
       ],
       toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
     });
     $("#AtelierJourDay").wrap("<div class='col-lg-4' style='padding:0px;padding-right:4px;'></div>");
     $("#AtelierJourMonth").wrap("<div class='col-lg-4' style='padding:0px;padding-right:4px;'></div>");
     $("#AtelierJourYear").wrap("<div class='col-lg-4' style='padding:0px;'></div>");
     $(".toEmpty").contents().filter(function(){ return this.nodeType === 3; }).remove();
    });
   </script>
   </div>
  </div>
 </div>
</div>




























<?php /*

<script type="text/javascript">


(function( $ ) {
    $.widget( "ui.dp", {
            _create: function() {
                var el = this.element.hide();
                this.options.altField = el;
                var input = this.input = $('<input>').insertBefore( el )
                input.focusout(function(){
                        if(input.val() == ''){
                            el.val('');
                        }
                    });
                input.datepicker(this.options)
                if(convertDate(el.val()) != null){
                    this.input.datepicker('setDate', convertDate(el.val()));
                }
            },
            destroy: function() {
                this.input.remove();
                this.element.show();
                $.Widget.prototype.destroy.call( this );
            }
    });
    
    var convertDate = function(date){
      if(typeof(date) != 'undefined' && date != null && date != ''){
        return new Date(date);
      } else {
        return null;
      }
    }
})( jQuery );

$(document).ready(function() {
   $(".inputDatePicker").dp({
      dateFormat: 'yy-mm-dd'
   }); 
});

$(function() {
  tinymce.init({
    selector: "textarea",
    plugins: [
    "advlist autolink lists link image charmap print preview anchor",
    "searchreplace visualblocks code fullscreen",
    "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
  });
});
function toggle (source)
{
 checkboxes = document.getElementsByTagName("input");
 console.log(checkboxes);
 for(var i=0, n=checkboxes.length;i<n;i++)
 {
  if (checkboxes[i].placeholder == source.placeholder)
  {
   checkboxes[i].checked = source.checked;
  }
 }
}

$(function()
{
 $('#form_ajout_new').submit(function()
 {
  tinyMCE.triggerSave("{{form.description.vars.id}}");
  $("#controlgroupNom").attr("class", "control-group");
  $("#controlgroupNomHelpBlock").attr("style", "display:none;");
  $("#controlgroupContenu").attr("class", "control-group");
  $("#controlgroupContenuHelpBlock").attr("style", "display:none;");
  if (document.getElementById('{{form.nom.vars.id}}').value == "")
  {
   $("#controlgroupNom").attr("class", "control-group error");
   $("#controlgroupNomHelpBlock").attr("style", "display:block;");
   $('html, body').animate({ scrollTop: $("#controlgroupNom").offset()}, 1000);
   return false;
  }
  if (document.getElementById('{{form.description.vars.id}}').value == "")
  {
   $("#controlgroupContenu").attr("class", "control-group error");
   $("#controlgroupContenuHelpBlock").attr("style", "display:block;");
   return false;
  }
  else
  {
   return true; 
  }
 });
});

function prevent_chrome_error ()
{
 document.getElementById('{{form.description.vars.id}}').style.display = 'block';
 document.getElementById('{{form.description.vars.id}}').style.visibility = 'hidden';
 document.getElementById('{{form.description.vars.id}}').style.position = 'relative';
 document.getElementById('{{form.description.vars.id}}').style.left = '-1000';
}
</script>
<div class="span9">
 <div class="row-fluid">
  <div class="span12">
   <div class="thumbnail form-modal">
    <div class="modal-header">
     <h3>Nouvel atelier :</h3>

      <?= $this->Session->flash('flash', array('element' => 'failure')) ?>

    </div>







<?= $this->Form->create('Periscolaires', array( 'class' => 'form-horizontal' )); ?>
  <div class="modal-body">

    <?= $this->Form->input('nom', array('label' => 'Nom de l\'Atelier :')) ?>

    <?= $this->Form->input('user_id', array('label' => 'Animateur :')) ?>

    <?= $this->Form->input('matiere_id', array('label' => 'Activité :')) ?>

    <?= $this->Form->input('horaire_id', array('label' => 'Horaire :')) ?>

    <?php
      echo $this->Form->input('jour', array("class" => "inputDatePicker", 'type' => 'text', 'id' => 'ent_atelierbundle_atelier_timeAtelier_date', 'placeholder' => 'aaaa-mm-jj', 'dateFormat' => 'YMD', 'label' => 'Jour (Date du premier atelier pour une période) :'));
    ?>

<br />
<div id="controlgroupDate" class="control-group">
       <div class="tabbable tabs-left">
        <ul class="nav nav-tabs control-label" style="margin:0px;">
         <li class="active" style="text-align:center;"><a href="#date" data-toggle="tab">Date</a></li>
         <li style="text-align:center;"><a href="#periode" data-toggle="tab">Période</a></li>
        </ul>
        <div class="controls">
         <div class="tab-content">
          <div class="tab-pane active" id="date">
            <?= $this->Form->input('hebdomadaire', array('label' => 'Hebdomadaire (nombre de répétition)','value' => '1')) ?>

          </div>
          <div class="tab-pane" id="periode">
             <?= $this->Form->input('periode_id', array('empty' => '(Choisissez)')) ?>
          </div>
         </div>
        </div>
       </div>
      </div>




    <?= $this->Form->input('salle_id', array('label' => 'Lieux de pratique :')) ?>

    <?= $this->Form->input('nombre_place', array('label' => 'Nombre de place :')) ?>

    <?= $this->Form->input('description', array('label' => 'Description :')) ?>

    <?php
      echo $this->Form->input('Atelier.Division',array('label'=>'<font color="black">Classes :</font> <font color="grey"><i>(CTRL enfoncé pour selectionner plusieurs classes)</i></font>', 'type'=>'select', 'multiple'=>true));
    ?>

   </div>

<?php

echo $this->Session->flash('flash', array('element' => 'failure'));

$options = array(
    'label' => 'Sauvegarder',
    'class' => 'btn btn-success',
    'formnovalidate' => 'true',
    'div' => array(
        'class' => 'modal-footer',
    )
);
echo $this->Form->end($options);

?>

   </div>
  </div>
 </div>
</div>
*/ ?>