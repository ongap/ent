<?php
$this->layout = 'print';
?>

<h2>Voici les élèves qui sont inscrits dans votre atelier <?= $ateliersCourant[0]['Atelier']['nom']; ?> pour le <?= $this->Time->format( $ateliersCourant[0]['Atelier']['jour'].' '.$ateliersCourant[0]['Horaire']['heureDebut'], '%d/%m/%Y %H:%M ') ?> :</h2>
<p style="float:right;font-size:30px;">Lieu : <?= $ateliersCourant[0]['Salle']['nom'] ?> </p>
       <table id="tableau_liste_eleve" class="table table-bordered table-striped table-hover table-condensed" style="table-layout:fixed;">
         <thead>
          <tr>
           <th>Nom</th>
           <th>Prénom</th>
           <th>Classe</th>
           <th>Présence</th>
           <th>Valider</th>
           <th>Commentaire</th>
          </tr>
         </thead>
        <tbody>
          <?php foreach ($ateliersCourant[0]['Inscription'] as $key => $inscript): ?>
            <tr>
              <td><?= $inscript['User']['nom'] ?></td>
              <td><?= $inscript['User']['prenom'] ?></td>
              <td><?= $inscript['User']['Division']['nom'] ?></td>
              <td style="text-align:center;"><input type="checkbox" name=" " /></td>
              <td style="text-align:center;"><input type="text" name=" " style="width:50px;"/></td>
              <td style="text-align:center;"><input type="text" name=" " style="width:150px;"/></td>
            </tr>
          <?php endforeach ?>
        </tbody>
       </table>
        Animateur : <?= $ateliersCourant[0]['User']['civilite']." ".$ateliersCourant[0]['User']['nom']." ".$ateliersCourant[0]['User']['prenom']; ?>
   </body>
</html>

