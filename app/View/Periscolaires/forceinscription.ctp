<?php
$x = 0;
$tablenght = 16;
?>
<div class="row">
 <div class="col-lg-12">
  <div class="panel panel-default">
   <div class="panel-body">
    <fieldset>
     <legend>
      Inscriptions 
     </legend>
     <table class="table table-striped table-bordered table-hover table-condensed" style="table-layout:fixed;margin-bottom:0px;">
      <tr>
       <th><a href="<?= $this->Html->url(array('controller' => 'periscolaires', 'action' => 'forceinscription', $idCourante, 'null', strftime('%Y' ,strtotime('-1 day', strtotime($semaine[0]))), strftime('%m' ,strtotime('-1 day', strtotime($semaine[0]))), strftime('%d' ,strtotime('-1 day', strtotime($semaine[0]))))) ?>" class="btn btn-primary btn-xs" style="width:100%;font-weight:800;">&laquo;</a></th>
       <th colspan="7" class="text-center">Périscolaires du <?= strftime('%A %e %B %Y', strtotime($semaine[0])); ?> au <?= strftime('%A %e %B %Y', strtotime($semaine[6])); ?></th>
       <th><a href="<?= $this->Html->url(array('controller' => 'periscolaires', 'action' => 'forceinscription', $idCourante, 'null', strftime('%Y' ,strtotime('+1 day', strtotime($semaine[6]))), strftime('%m' ,strtotime('+1 day', strtotime($semaine[6]))), strftime('%d' ,strtotime('+1 day', strtotime($semaine[6]))))) ?>" class="btn btn-primary btn-xs" style="width:100%;font-weight:800;">&raquo;</a></th>
      </tr>
     </table>
     <table class="table table-bordered table-striped table-hover table-condensed">
      <thead>
       <tr>
        <th>Nom</th>
        <th>Matière</th>
        <th>Description</th>
        <th>Date et Heure</th>
        <th>Animateur</th>
        <th>Lieux</th>
        <th>Places</th>
        <th class="text-center">Actions</th>
       </tr>
      </thead>
      <tbody>
      <?php $nomJour = array('Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'); ?>
      <?php foreach ($tousLesAteliers as $key => $unAtelier): ?>
       <?php 
        $i = 0;
        foreach ($unAtelier['Inscription'] as $key => $value){
          if ($value['user_id'] == $idCourante) {
            $i = 1;
            $cleInscription = $key;
          }
        }
       ?>
       <?php if($i == 1): ?>
       <tr class="success">
       <?php elseif(count($unAtelier['Inscription']) == $unAtelier['Atelier']['nombre_place']): ?>
       <tr class="warning">
       <?php else: ?>
       <tr>
       <?php endif ?>  
        <td style="vertical-align:middle;">
         <?php if (strlen($unAtelier["Atelier"]["nom"])>$tablenght): ?>
          <?= '<span class="tooltipspan" data-toggle="tooltip" title="'.str_replace(array("'", "\""), array("&apos;", "&quot;"),$unAtelier['Atelier']['nom']).'">'.substr($unAtelier['Atelier']['nom'],0,$tablenght).'...'.'</span>'; ?>
         <?php else: ?>
           <?= $unAtelier["Atelier"]["nom"]; ?>
         <?php endif ?>
        </td>
        <td style="vertical-align:middle;">
         <?php if (strlen($unAtelier["Matiere"]["nom"])>$tablenght): ?>
          <?= '<span class="tooltipspan" data-toggle="tooltip" title="'.str_replace(array("'", "\""), array("&apos;", "&quot;"),$unAtelier['Matiere']['nom']).'">'.substr($unAtelier['Matiere']['nom'],0,$tablenght).'...'.'</span>'; ?>
         <?php else: ?>
           <?= $unAtelier["Matiere"]["nom"]; ?>
         <?php endif ?>
        </td>
        <td style="vertical-align:middle;">
         <?php if (strlen($unAtelier["Atelier"]["description"])>$tablenght): ?>
          <?= '<span class="tooltipspan" data-toggle="tooltip" title="'.strip_tags(str_replace(array("'", "\""), array("&apos;", "&quot;"),$unAtelier["Atelier"]["description"])).'">'.substr($unAtelier['Atelier']['description'],0,$tablenght).'...'.'</span>'; ?>
         <?php else: ?>
           <?= $unAtelier["Atelier"]["description"]; ?>
         <?php endif ?>
        </td>
        <td style="vertical-align:middle;">
         <?= $nomJour[$this->Time->format($unAtelier['Atelier']['jour'], '%w')] ?>
         <?= $this->Time->format( $unAtelier['Atelier']['jour'].' '.$unAtelier['Horaire']['heureDebut'], '%d/%m/%Y %H:%M ') ?>
        </td>
        <td style="vertical-align:middle;"><?= $unAtelier['User']['civilite'] ?>. <?= $unAtelier['User']['nom'] ?></td>
        <td style="vertical-align:middle;"><?= $unAtelier['Salle']['nom'] ?></td>
        <td style="vertical-align:middle;"><?= count($unAtelier['Inscription'])."/".$unAtelier['Atelier']['nombre_place'] ?></td>
        <td style="vertical-align:middle;" class="text-center">
         <div class="btn-group">
          <?php if (count($unAtelier['Inscription']) == $unAtelier['Atelier']['nombre_place']): ?> 
            plus de place
          <?php elseif ($i == 0): ?> 
           <?= $this->Html->link('Inscrire', array('controller' => 'periscolaires', 'action' => 'forceinscription', $idCourante, $unAtelier['Atelier']['id']), array('class' => 'btn btn-success btn-sm', 'escape' => false)); ?>
          <?php else: ?>
           <?= $this->Html->link('Désinscrire', array('controller' => 'periscolaires', 'action' => 'forcedesinscription', $unAtelier['Inscription'][$cleInscription]['id']), array('class' => 'btn btn-danger btn-sm', 'escape' => false)); ?>
          <?php endif ?>
         </div>
        </td>
       </tr>
      <?php endforeach ?>
      </tbody>
     </table>
    </fieldset>
   </div>
  </div>
 </div>
</div>