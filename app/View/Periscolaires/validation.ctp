<div class="row">
 <div class="col-lg-12">
  <div class="panel panel-default">
   <div class="panel-body">
    <fieldset>
     <legend>
      Gestion du loisir périscolaire
      <p class="pull-right" style="position:relative;top:-8px;">
       <span class="label label-info">
       <?php if (!empty($listeAteliers)): ?>
        Périodique
       <?php elseif ($ateliersCourant['Atelier']['hebdomadaire'] > 1): ?>
        Hebdomadaire
       <?php endif ?>
       </span>
      </p>
     </legend>
     <div class="row">
      <div class="col-lg-6">
       <table class="table table-bordered table-striped table-condensed">
        <tbody>
         <tr>
          <th>Nom</th>
          <td><?= $ateliersCourant['Atelier']['nom']; ?></td>
         </tr>
         <tr>
          <th>Matière</th>
          <td><?= $ateliersCourant['Matiere']['nom']; ?></td>
         </tr>
         <tr>
          <th>Date</th>
          <td><?= $this->Time->format( $ateliersCourant['Atelier']['jour'].' '.$ateliersCourant['Horaire']['heureDebut'], '%d/%m/%Y %H:%M ') ?></td>
         </tr>
         <tr>
          <th>Lieux</th>
          <td><?= $ateliersCourant['Salle']['nom']; ?></td>
         </tr>
         <tr>
          <th>Nombre de places</th>
          <td><?= count($ateliersCourant['Inscription'])."/".$ateliersCourant['Atelier']['nombre_place']; ?></td>
         </tr>
         <tr>
          <th>Description</th>
          <td><?= $ateliersCourant['Atelier']['description']; ?></td>
         </tr>
        </tbody>
       </table>
      </div>
      <div class="col-lg-6">
       <table class="table table-bordered table-condensed table-striped">
        <thead>
         <tr>
          <th>Classes</th>
         </tr>
        </thead>
        <tbody>
         <?php foreach ($ateliersCourant['Division'] as $key => $uneClasse): ?>
         <tr>
          <td><?= $uneClasse['nom'] ?></td>
         </tr>
         <?php endforeach ?>
        </tbody>
       </table>       
      </div>
     </div>
     <hr/>
     <table class="table table-bordered table-condensed">
      <tbody>
       <tr>
        <td style="text-align:center;">Pas encore validé</td>
        <td class="warning" style="text-align:center;">Partiellement validé</td>
        <td class="info" style="text-align:center;">Complétement validé</td>
       </tr>
      </tbody>
     </table>
     <table class="table table-bordered table-hover table-condensed">
      <thead>
       <tr>
        <th>Date</th>
        <th style="text-align:center;">Action</th>
       </tr>
      </thead>
      <tbody>
      <?php $nomJour = array('Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'); ?>

      <?php if (count($ateliersCourant['Inscription']) > count($ateliersCourant['Creditation'])): ?>
       <tr class="warning">
      <?php elseif(count($ateliersCourant['Inscription']) == count($ateliersCourant['Creditation']) && count($ateliersCourant['Inscription']) != 0): ?>
       <tr class="info">
      <?php else: ?>
       <tr>
      <?php endif ?>

        <?php
        if (count($ateliersCourant['Inscription']) < count($ateliersCourant['Creditation'])) {
          echo $this->Html->link('Corriger', array('controller' => 'periscolaires', 'action' => 'crediationCorriger', $ateliersCourant['Atelier']['id']));
        }
        ?>

        <td>
         <?= $nomJour[$this->Time->format( $ateliersCourant['Atelier']['jour'], '%w')] ?>
         <?= $this->Time->format( $ateliersCourant['Atelier']['jour'].' '.$ateliersCourant['Horaire']['heureDebut'], '%d/%m/%Y %H:%M ') ?>
        </td>
        <td style="text-align:center;">
         <?= $this->Html->link('Valider', array('controller' => 'periscolaires', 'action' => 'crediationAtelier', $ateliersCourant['Atelier']['id']), array('class' => 'btn btn-success btn-sm', 'escape' => false)); ?>
        </td>
       </tr>
      <?php if (!empty($listeAteliers)): ?>
      <?php foreach ($listeAteliers as $key => $unAtelier): ?>
      <?php if (count($unAtelier['Inscription']) > count($unAtelier['Creditation'])): ?>
       <tr class="warning">
      <?php elseif(count($unAtelier['Inscription']) == count($unAtelier['Creditation']) && count($unAtelier['Inscription']) != 0): ?>
       <tr class="info">
      <?php else: ?>
       <tr>
      <?php endif ?>
        <td>
         <?= $nomJour[$this->Time->format($unAtelier['Atelier']['jour'], '%w')] ?>
         <?= $this->Time->format( $unAtelier['Atelier']['jour'].' '.$unAtelier['Horaire']['heureDebut'], '%d/%m/%Y %H:%M ') ?>
        </td>
        <td style="text-align:center;">
         <?= $this->Html->link('Valider', array('controller' => 'periscolaires', 'action' => 'crediationAtelier', $unAtelier['Atelier']['id']), array('class' => 'btn btn-success btn-sm', 'escape' => false)); ?>
        </td>
       </tr>
      <?php endforeach ?>
      <?php endif ?>
      </tbody>
     </table>
     <hr/>
     <?= $this->Html->link('Imprimer',array('controller' => 'periscolaires', 'action' => 'impression', $ateliersCourant['Atelier']['id']), array('class' => 'btn btn-primary pull-right')); ?>
    </fieldset>
   </div>
  </div>
 </div>
</div>