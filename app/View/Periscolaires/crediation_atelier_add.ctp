<?php $x = 0; ?>
<div class="row">
 <div class="col-lg-12">
  <div class="panel panel-default">
   <div class="panel-body">
    <fieldset>
     <legend>
      Validation du loisir périscolaire
     </legend>
     <?= $this->Form->create('Creditation'); ?>
     <table class="table table-bordered table-striped table-condensed">
      <thead>
       <tr>
        <th>Nom</th>
        <th>Prénom</th>
        <th>Classe</th>
        <th style="text-align:center;">Présence</th>
        <th style="text-align:center;">Valider</th>
        <th style="text-align:center;">Commentaire</th>
       </tr>
      </thead>
      <tbody>
      <?php foreach ($ateliersCourant as $key => $inscript): ?>
       <tr>
       <?= $this->Form->input($x.'.Creditation.user_id', array('type' => 'hidden', 'value' => $inscript['User']['id'])) ?>
       <?= $this->Form->input($x.'.Creditation.atelier_id', array('type' => 'hidden', 'value' => $inscript['Atelier']['id'])) ?>
        <td style="vertical-align:middle;"><?= $inscript['User']['nom'] ?></td>
        <td style="vertical-align:middle;"><?= $inscript['User']['prenom'] ?></td>
        <td style="vertical-align:middle;"><?= $inscript['User']['Division']['nom'] ?></td>
        <td style="text-align:center;vertical-align:middle;"><?= $this->Form->input($x.'.Creditation.presence', array("div"=>false,"format"=>array("input"), "checked"=>"checked")) ?></td>
        <td style="text-align:center;vertical-align:middle;"><?= $this->Form->input($x.'.Creditation.valid', array("div"=>false,"class" => "form-control", "format"=>array("input"), "value"=>1)) ?></td>
        <td style="text-align:center;vertical-align:middle;"><?= $this->Form->input($x.'.Creditation.commentaire', array("div"=>false,"class" => "form-control", "format"=>array("input"), "value"=>"RAS")) ?>
       </tr>
       <?php $x++; ?>
      <?php endforeach ?>
      </tbody>
     </table>
     <?php
      echo $this->Session->flash('flash', array('element' => 'failure'));
      $options = array
      (
       'label' => 'Sauvegarder',
       'class' => 'btn btn-success',
       'formnovalidate' => 'true',
       'div' => array
       (
        'class' => 'pull-right',
       )
      );
      echo $this->Form->end($options);
     ?>
    </fieldset>
   </div>
  </div>
 </div>
</div>