
<div class="span9">
 <div class="row-fluid">
  <div class="span12">
   <div class="thumbnail form-modal">
    <div class="modal-header">
     <h3> Patch Atelier :
     </h3>
    </div>

    <?= $this->Session->flash('flash', array('element' => 'failure')) ?>

  <div class="modal-body modal-display-all-body" style="height:100%;">
   <table style="table-layout:fixed;margin-bottom:60px;" class="table table-bordered table-striped table-hover table-condensed no-more-tables">
    <thead>
     <tr>
      <th >Nom</th>
      <th >Matière</th>
      <th >Période</th>
      <th >Date et Heure</th>
      <th >Animateur</th>
      <th >Classe(s)</th>
      <th >Places</th>
      <th style="text-align:center;width:15%;">Actions</th>
     </tr>
    </thead>
    <tbody>

      <?php
        $nomJour = array('Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi');
        $x = 0;
      ?>
      <?php foreach ($tousLesAteliers as $key => $unAtelier): ?>

       <tr>
        <td><?= $unAtelier['Atelier']['id'] ?> : <?= $unAtelier['Atelier']['nom'] ?></td>
        <td><?= $unAtelier['Matiere']['nom'] ?></td>
        <td> P : <?= $unAtelier['Atelier']['periode_id'] ?></td>
        <td>
          <?= $nomJour[$this->Time->format( $unAtelier['Atelier']['jour'], '%w')] ?>
          <?= $this->Time->format( $unAtelier['Atelier']['jour'].' '.$unAtelier['Horaire']['heureDebut'], '%d/%m/%Y %H:%M ') ?>
        </td>
        <td><?= $unAtelier['User']['civilite'] ?>. <?= $unAtelier['User']['nom'] ?></td>
        <td>




        <?php foreach ($unAtelier['Division'] as $key => $uneClasse): ?>
           <?= $uneClasse['nom'].", " ?>
        <?php endforeach ?>

        </td>
        <td><?= count($unAtelier['Inscription'])."/".$unAtelier['Atelier']['nombre_place'] ?></td>

        <td style="text-align:right;width:15%">

          <?php if ($unAtelier['Atelier']['status'] != 'periscolaire' && $unAtelier['Atelier']['periscolaire'] == '1'): ?>
            <?= $this->Html->link('Patcher !', array('controller' => 'periscolaires', 'action' => 'patch', $unAtelier['Atelier']['id']), array('class' => 'btn btn-success btn-small', 'title' => "Patch Partiel", 'label' => 'Patcher' )); ?>
          <?php else: ?>
            <?= $this->Html->link('Patcher', array('controller' => 'periscolaires', 'action' => 'patch', $unAtelier['Atelier']['id']), array('class' => 'btn btn-danger btn-small', 'escape' => false, 'title' => "Aucun Patch", 'label' => 'Patcher' )); ?>
          <?php endif ?>

        </td>
       </tr>
       <?php $x++; ?>
      <?php endforeach ?>

    </tbody>
   </table>
  </div>

   </div>
  </div>
 </div>
</div>