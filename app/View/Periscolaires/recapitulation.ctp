<?php
$tablenght = 12;
?>
<div class="row">
 <div class="col-lg-12">
  <div class="panel panel-default">
   <div class="panel-body">
    <fieldset>
     <legend>
      Mes loisirs périscolaires :
     </legend>
     <table class="table table-bordered table-striped table-hover table-condensed">
      <thead>
       <tr>
        <th>Nom</th>
        <th>Matière</th>
        <th>Description</th>
        <th>Date et Heure</th>
        <th>Animateur</th>
        <th>Lieux</th>
        <th>Places</th>
       </tr>
      </thead>
      <tbody>
      <?php $nomJour = array('Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'); ?>
      <?php foreach ($InscriptionCourante as $key => $unAtelier): ?>
       <tr>
        <td style="vertical-align:middle;">
         <?php if (strlen($unAtelier["Atelier"]["nom"])>$tablenght): ?>
          <?= '<span class="tooltipspan" data-toggle="tooltip" title="'.str_replace(array("'", "\""), array("&apos;", "&quot;"),$unAtelier['Atelier']['nom']).'">'.substr($unAtelier['Atelier']['nom'],0,$tablenght).'...'.'</span>'; ?>
         <?php else: ?>
           <?= $unAtelier["Atelier"]["nom"]; ?>
         <?php endif ?>
        </td>
        <td style="vertical-align:middle;">
         <?php if (strlen($unAtelier["Matiere"]["nom"])>$tablenght): ?>
          <?= '<span class="tooltipspan" data-toggle="tooltip" title="'.str_replace(array("'", "\""), array("&apos;", "&quot;"),$unAtelier['Matiere']['nom']).'">'.substr($unAtelier['Matiere']['nom'],0,$tablenght).'...'.'</span>'; ?>
         <?php else: ?>
           <?= $unAtelier["Matiere"]["nom"]; ?>
         <?php endif ?>
        </td>
        <td style="vertical-align:middle;">
         <?php if (strlen($unAtelier["Atelier"]["description"])>$tablenght): ?>
          <?= '<span class="tooltipspan" data-toggle="tooltip" title="'.strip_tags(str_replace(array("'", "\""), array("&apos;", "&quot;"),$unAtelier["Atelier"]["description"])).'">'.substr($unAtelier['Atelier']['description'],0,$tablenght).'...'.'</span>'; ?>
         <?php else: ?>
           <?= $unAtelier["Atelier"]["description"]; ?>
         <?php endif ?>
        </td>
        <td style="vertical-align:middle;">
         <?= $nomJour[$this->Time->format($unAtelier['Atelier']['jour'], '%w')] ?>
         <?= $this->Time->format( $unAtelier['Atelier']['jour'].' '.$unAtelier['Horaire']['heureDebut'], '%d/%m/%Y %H:%M ') ?>
        </td>
        <td style="vertical-align:middle;"><?= $unAtelier['User']['civilite'] ?>. <?= $unAtelier['User']['nom'] ?></td>
        <td style="vertical-align:middle;"><?= $unAtelier['Salle']['nom'] ?></td>
        <td style="vertical-align:middle;"><?= count($unAtelier['Inscription'])."/".$unAtelier['Atelier']['nombre_place'] ?></td>
       </tr>
      <?php endforeach ?>      
      </tbody>
     </table>
    </fieldset>
   </div>
  </div>
 </div>
</div>