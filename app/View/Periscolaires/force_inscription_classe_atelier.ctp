<?php
function search($array, $key, $value)
{
 $results = array();
 if (is_array($array))
 {
  if (isset($array[$key]) && $array[$key] == $value)
  {
   $results[] = $array;
  }
  foreach ($array as $subarray)
  {
   $results = array_merge($results, search($subarray, $key, $value));
  }
 }
 return $results;
}
?>
<div class="row">
 <div class="col-lg-12">
  <div class="panel panel-default">
   <div class="panel-body">
    <fieldset>
     <legend>
      Inscription
     </legend>
     <?php if(count($atelierCourant['Inscription']) == $atelierCourant['Atelier']['nombre_place']): ?>
      <p class="bg-warning" style="padding:15px;">
     <?php elseif(count($atelierCourant['Inscription']) > $atelierCourant['Atelier']['nombre_place']): ?>
      <p class="bg-danger" style="padding:15px;">
     <?php else: ?>
      <p class="bg-info" style="padding:15px;">
     <?php endif ?>
      Nombre de place occupée : <b><?= count($atelierCourant['Inscription'])."/".$atelierCourant['Atelier']['nombre_place'] ?></b>
     </p>
     <table class="table table-bordered table-striped table-hover table-condensed">
      <thead>
       <tr>
        <th>Nom</th>
        <th>Prénom</th>
        <th class="text-center">Action</th>
       </tr>
      </thead>
      <tbody class="liste">
      <?php foreach ($classeCourante as $key => $unEleve): ?>
       <tr>
        <td style="vertical-align:middle">
         <?= $unEleve['User']['nom'] ?>
        </td>
        <td style="vertical-align:middle">
         <?= $unEleve['User']['prenom'] ?>
        </td>
        <td style="vertical-align:middle" class="text-center">
         <?php
          $result = search($atelierCourant['Inscription'], 'user_id', $unEleve['User']['id']);
          if (count($result))
          {
           echo $this->Html->link('Desinscription', array('controller' => 'periscolaires', 'action' => 'inscrireUnEleve', $unEleve['User']['id'], $atelierCourant['Atelier']['id']), array('class' => 'btn btn-danger btn-sm', 'escape' => false));
          }
          else
          {
           echo $this->Html->link('Inscription', array('controller' => 'periscolaires', 'action' => 'inscrireUnEleve', $unEleve['User']['id'], $atelierCourant['Atelier']['id']), array('class' => 'btn btn-success btn-sm', 'escape' => false));
          }
         ?>
        </td>
       </tr>
      <?php endforeach ?>
      </tbody>
     </table>
    </fieldset>
   </div>
  </div>
 </div>
</div>


<?php /*
<?php
function search($array, $key, $value)
{
    $results = array();

    if (is_array($array)) {
        if (isset($array[$key]) && $array[$key] == $value) {
            $results[] = $array;
        }

        foreach ($array as $subarray) {
            $results = array_merge($results, search($subarray, $key, $value));
        }
    }

    return $results;
}

?>

<div class="span9">
 <div class="row-fluid">
  <div class="span12">
   <div class="thumbnail form-modal">
    <div class="modal-header">
     <h3> Inscription :
     </h3>
    </div>


  <div class="modal-body modal-display-all-body" style="height:100%;">
    
   Nombre de place occupée : <?= count($atelierCourant['Inscription'])."/".$atelierCourant['Atelier']['nombre_place'] ?>
   <table style="table-layout:fixed;margin-bottom:60px;" class="table table-bordered table-striped table-hover table-condensed no-more-tables">
    <thead>
     <tr>
      <th >Nom</th>
      <th >Prenom</th>
      <th style="text-align:center;width:15%;">Actions</th>
     </tr>
    </thead>
    <tbody>




      <?php foreach ($classeCourante as $key => $unEleve): ?>


       <tr>
        <td><?= $unEleve['User']['nom'] ?></td>
        <td><?= $unEleve['User']['prenom'] ?></td>

        <td style="text-align:right;width:15%">

<?php
$result = search($atelierCourant['Inscription'], 'user_id', $unEleve['User']['id']);

if (count($result)) {
  echo $this->Html->link('Desinscription', array('controller' => 'periscolaires', 'action' => 'inscrireUnEleve', $unEleve['User']['id'], $atelierCourant['Atelier']['id']), array('class' => 'btn btn-danger btn-small', 'escape' => false));
}
else{
  echo $this->Html->link('Inscription', array('controller' => 'periscolaires', 'action' => 'inscrireUnEleve', $unEleve['User']['id'], $atelierCourant['Atelier']['id']), array('class' => 'btn btn-success btn-small', 'escape' => false));
}

?>

        </td>
       </tr>
      <?php endforeach ?>

    </tbody>
   </table>
  </div>

   </div>
  </div>
 </div>
</div>
*/ ?>