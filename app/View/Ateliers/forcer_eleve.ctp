<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<fieldset>
					<legend>
						Inscriptions pour 
						<strong>
							<?= $infoUser['civilite'].' '.$infoUser['nom'].' '.$infoUser['prenom'].' ('.$infoUser['classe'].')' ?>
						</strong>
					</legend>

					<br>

					<?php
					//Génération du tableau en-tête
					$tableauCommande = $this->Atl->enteteTableau("forcerEleve", $infoUser['id'], $annee, $numSemaine);

					//Traitement des ateliers
					$ateliers = array();
					foreach ($tousLesAteliers as $key => $atelier) {

						$id = $atelier['Id'];
						if (isset($atelier['Parent']) && $atelier['Parent'] != '0') {
							$id = $atelier['Parent'];
						}

						//Génération des boutons
						$urlLien 	= array (
							'controller' => 'ateliers',
							'action' => 'forcerEleve',
							'rubrique' => $rubrique,
							$infoUser['id'],
							$annee,
							$numSemaine,
							'inscription',
							$id
						);

						$styleBtn	= array('escape' => false, 'class' => 'btn btn-success disabled btn-sm');
						$btn 		= $this->Html->link ('Inscrire', $urlLien, $styleBtn);
						if (!isset($atelier['Inscrit']) && $atelier['Inscriptions'] < $atelier['Places'] && $atelier['Id'] != ""){
							$styleBtn['class'] 	= 'btn btn-success btn-sm';
							$btn = $this->Html->link ('Inscrire', $urlLien, $styleBtn);
						}
						elseif ($atelier['Id'] != "" && isset($atelier['Inscrit']) && $atelier['Inscrit'] != ""){
							$urlLien[3] 		= 'desinscription';
							$urlLien[4] 		= $atelier['Inscrit'];
							$styleBtn['class'] 	= 'btn btn-danger btn-sm';
							$btn = $this->Html->link ('Désinscrire', $urlLien, $styleBtn);
						}
						$ateliers[] = $this->Atl->genererLigne($atelier, $btn);
					}

					$tableau = $this->Atl->listeTableau($ateliers);

					echo $tableauCommande."\n".$tableau;
					?>

				</fieldset>
			</div>
		</div>
	</div>
</div>