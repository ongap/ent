<div class="row">
	<div class="col-lg-12">
		<?= $this->Session->flash('flash', array('element' => 'failure')); ?>
		<div class="panel panel-default">
			<div class="panel-body">
				<fieldset>
					<legend>
						Suivi des ateliers pour la classe <?= $classeCourante[0]['Division']['nom'] ?> :
						<form class="form-inline pull-right" action="javascript:" style="position:relative;top:-8px;">
							<div class="form-group">
								<input id="inputChercher" type="text" class="form-control" placeholder="Chercher..." />
							</div>
						</form>
					</legend>
					<?php
						$LabelOptions = array("class" => "col-lg-3 control-label");
						$options = array (
							"id" => "form_ajout_new",
							"class" => "form-horizontal",
							"inputDefaults" => array (
								"class" => "form-control",
								"div" => array("class" => "form-group"),
								"label" => $LabelOptions,
								"between" => "<div class='col-lg-9'>",
								"after" => "</div>",
								'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block')),
								"format" => array('before', 'label', 'between', 'input', 'error', 'after')
								),
							);
						echo $this->Form->create("Atelier", $options);
						$options = array (
							"class" => "inputDatePicker form-control",
							"type" => "text",
							"dateFormat" => "YMD",
							"label" => array_merge($LabelOptions, array("text" => "Premier date :"))
						);
						echo $this->Form->input("dateDebut", $options);
						$options = array (
							"class" => "inputDatePicker form-control",
							"type" => "text",
							"dateFormat" => "YMD",
							"label" => array_merge($LabelOptions, array("text" => "Seconde date :"))
							);
						echo $this->Form->input("dateFin", $options);
						$options = array (
							"label" => false,
							"class" => "btn btn-success col-xs-12 col-sm-12",
							"between" => "<div class='col-lg-3 col-lg-offset-9'>",
							"type" => "button"
						);
						echo $this->Form->input("Rechercher", $options);     
						echo $this->Form->end();
					?>

					<table id="tableau_liste_eleve" class="table table-bordered table-striped table-hover table-condensed" style="padding-top:24px;">
						<thead>
						<?php

							$options = array('class' => 'text-center', 'style' => 'vertical-align:middle');
							$ateliers 						= array('<!-- Vide -->');
							$attributesAteliers 	= array();
							$attributesAteliers[] = array(
								'Élève' => $options
								);
							foreach ($tousLesAteliers as $key => $atelier) {
								$status = "Ponctuel";
								if ($atelier['Periodique']) {
									$status = "Périodique";
								}
								elseif ($atelier['Hebdomadaire']) {
									$status = "Hebdomadaire";
								}

								$nom = $atelier['Nom'].' le '.$this->Time->format($atelier['Date et Heure'], '%d/%m/%Y').' ('.$status.')';

								$ateliers[] = array($nom => array('colspan' => '3', 'class' => 'text-center', 'style' => 'vertical-align:middle'));
								$attributesAteliers[] = array ( 'Inscript' 	=> $options );
								$attributesAteliers[] = array ( 'Présent' 	=> $options );
								$attributesAteliers[] = array ( 'Crédité' 	=> $options );

							}

							echo $this->Html->tableHeaders($ateliers);
							echo $this->Html->tableHeaders($attributesAteliers);
						?>

						</thead>
						<tbody style="margin-top:200px;" class="liste">
							<?php
								$users = array();
								foreach ($classeCourante as $key => $user) {
									$options	= array('class' => 'text-center', 'style' => 'color: gray;');
									$ligne		= array($user['User']['nom'].' '.$user['User']['prenom']);

									foreach ($tousLesAteliers as $key => $atelier) {
										$options['style'] = 'color: gray;';
										$inscription 			= '-';
										$present		 			= '-';
										$creditation 			= '-';
										$commentaire 			= '-';
										foreach ($user['Inscription'] as $key => $incript) {
											if ($atelier['Id'] == $incript['atelier_id']) {
												$inscription 			= 'Oui';
												$options['style'] = 'color: green;';
											}
										}
										$ligne[]	= array($inscription, $options); 
										$options['style'] = 'color: gray;';

										foreach ($user['Creditation'] as $key => $credit) {
											if ($atelier['Id'] == $credit['atelier_id']) {
												$present		 			= ($credit['presence']) ? 'Oui' : 'Non' ;
												$creditation 			= $credit['valid'];
												$commentaire 			= $credit['commentaire'];
											}
										}
										if ($present == 'Oui') {
											$options['style'] = 'color: green;';
										}
										elseif ($present == 'Non') {
											$options['style'] = 'color: red;';
										}
										$ligne[]	= array($present		, $options); 

										if ($creditation == '-') {
											$options['style'] = 'color: gray;';
										}
										elseif (intval($creditation) > 0) {
											$options['style'] = 'color: green;';
											$creditation 			= '<span title="'.$commentaire.'">'.$creditation.'</span>';
										}
										elseif (intval($creditation) <= 0) {
											$options['style'] = 'color: red;';
											$creditation 			= '<span title="'.$commentaire.'">'.$creditation.'</span>';
										}										
										$ligne[]	= array($creditation, $options); 
									}
									$users[]	= $ligne;
								}

								echo $this->Html->tableCells($users);
							?>
						</tbody>
					</table>
				</fieldset>
			</div>
		</div>
	</div>
</div>

<!-- Pour la fonction de rechercher-->
<script>
	$(document).ready(function() {
		var $rows = $(".liste tr");
		$('#inputChercher').keyup(function()
		{
			if ($('#inputChercher').val() != "")
			{
				$($("#inputChercher").parent()[0]).addClass("has-warning");
			}
			else
			{
				$($("#inputChercher").parent()[0]).removeClass("has-warning");    
			}
			var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
			$rows.show().filter(function()
			{
				var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
				return !~text.indexOf(val);
			}).hide();
		});
		$('.listab li a').click(function()
		{
			$('#inputChercher').val("");
			$($("#inputChercher").parent()[0]).removeClass("has-warning");
			var val = $.trim($("").val()).replace(/ +/g, ' ').toLowerCase();
			$rows.show().filter(function()
			{
				var text = $("").text().replace(/\s+/g, ' ').toLowerCase();
				return !~text.indexOf(val);
			}).hide(); 
		});
	});
</script>

<!-- Pour les dates piker-->
<script type="text/javascript">
	(function( $ ) {
		$.widget( "ui.dp", {
			_create: function() {
				var el = this.element.hide();
				this.options.altField = el;
				var input = this.input = $('<input>').insertBefore( el )
				input.focusout(function(){
					if(input.val() == ''){
						el.val('');
					}
				});
				input.datepicker(this.options)
				if(convertDate(el.val()) != null){
					this.input.datepicker('setDate', convertDate(el.val()));
				}
			},
			destroy: function() {
				this.input.remove();
				this.element.show();
				$.Widget.prototype.destroy.call( this );
			}
		});
		var convertDate = function(date){
			if(typeof(date) != 'undefined' && date != null && date != ''){
				return new Date(date);
			} else {
				return null;
			}
		}
	})( jQuery );
	$(document).ready(function() {
		$(".inputDatePicker").dp({
			dateFormat: 'yy-mm-dd'
		}); 
	});
</script>