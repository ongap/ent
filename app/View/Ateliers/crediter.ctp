<?php
	$optionsHead = array('class' => 'text-center');
?>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<fieldset>
					<legend>
						Créditation de l'atelier <strong><?= $atelier['Atelier']['nom'] ?></strong> du <?= $this->Time->format( $atelier['Atelier']['jour'].' '.$atelier['Horaire']['heureDebut'], '%d/%m/%Y %H:%M ') ?> :
						<?= $this->Html->link(
								'Retour',
								array('controller' => 'ateliers', 'rubrique' => $rubrique, 'action' => 'validation', $atelier['Atelier']['id']),
								array('class' => 'btn btn-info pull-right', 'style' => 'position:relative;top:-8px;')
							);
						?>
					</legend>
					<?php
					$LabelOptions = array("class" => "col-lg-3 control-label");
					$options = array
					(
						"class" => "form-horizontal",
						"inputDefaults" => array
						(
							"class" => "form-control",
							"div" => array("class" => "form-group"),
							"label" => $LabelOptions,
							"between" => "<div class='col-lg-9'>",
							"after" => "</div>",
							'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block')),
							"format" => array('before', 'label', 'between', 'input', 'error', 'after')
						),
					);
					echo $this->Form->create('Creditation', $options);
					?>
					<table class="table table-bordered table-striped table-condensed">
						<thead>
							<?= $this->Html->tableHeaders ( array ( 
									'Nom',
									'Prénom',
									'Classe',
									array('Présence' 		=> $optionsHead),
									array('Valider' 		=> $optionsHead),
									array('Commentaire' => $optionsHead)
								))
							?>
						</thead>
						<tbody>

							<?php
								$index = 0;
								$users = array();
								foreach ($usersCrediter as $key => $user) {

									$options = array('class' => 'text-center');

									$prefixChamp = $index.'.Creditation.';
									$optionChamp = array('type' => 'hidden', 'div' => false, 'format' => array('input', 'error'));

									$champs['hidden'] 			= $this->Form->input($prefixChamp.'id', 					$optionChamp);
									$champs['hidden'] 		 .= $this->Form->input($prefixChamp.'user_id', 			$optionChamp);
									$champs['hidden'] 		 .= $this->Form->input($prefixChamp.'atelier_id', 	$optionChamp);

									unset($optionChamp['type']);
									if ($this->Form->error("presence")) { $optionChamp["div"] = "form-group has-error"; }
									$champs['presence'] 		= $this->Form->input($prefixChamp.'presence', 		$optionChamp);
									$optionChamp["div"] 		= false;

									$optionChamp['class'] 	= 'form-control';
									if ($this->Form->error("valid")) { $optionChamp["div"] = "form-group has-error"; }
									$champs['valid'] 				= $this->Form->input($prefixChamp.'valid', 				$optionChamp);
									$optionChamp["div"] 		= false;

									if ($this->Form->error("commentaire")) { $optionChamp["div"] = "form-group has-error"; }
									$champs['commentaire'] 	= $this->Form->input($prefixChamp.'commentaire', 	$optionChamp);
									$optionChamp["div"] 		= false;

									$users[] = array(
										$user['User']['nom'],
										$user['User']['prenom'],
										$user['Division']['nom'],
										array($champs['hidden'].$champs['presence'], $options),
										array($champs['valid'], $options),
										array($champs['commentaire'], $options),
									);

									$index++;
								}

								echo $this->Html->TableCells($users);

							?>

						</tbody>
					</table>
					<?php
					$options = array
					(
						'label' => 'Sauvegarder',
						'class' => 'btn btn-success',
						'formnovalidate' => 'true',
						'div' => array
						(
							'class' => 'pull-right',
						)
					);
					echo $this->Form->end($options);
					?>
				</fieldset>
			</div>
		</div>
	</div>
</div>