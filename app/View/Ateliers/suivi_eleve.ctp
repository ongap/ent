<?php
$role = $this->Session->read('Auth.User.role');
$tablenght = 16;
?>
<div class="row">
 <div class="col-lg-12">
  <div class="panel panel-default">
   <div class="panel-body">
    <fieldset>
     <legend>
      Liste des ateliers pour l'élève <?= $eleve['User']['prenom'].' '.$eleve['User']['nom'] ?>
      <form class="form-inline pull-right" action="javascript:" style="position:relative;top:-8px;">
       <div class="form-group">
        <input id="inputChercher" type="text" class="form-control" placeholder="Chercher..." />
       </div>
      </form>
     </legend>
     <table class="table table-bordered table-striped table-hover table-condensed">
      <thead>
       <tr>
        <th>Nom</th>
        <th>Animateur</th>
        <th>Date et Heure</th>
        <th>Place</th>
        <th class="text-center">Action</th>
       </tr>
      </thead>
      <tbody class="liste">
      <?php $nomJour = array('Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'); ?>
      <?php foreach ($InscriptionCourante as $key => $unAtelier): ?>
      <?php if (date("Y-m-d") < $unAtelier['Atelier']['jour']): ?>
       <tr class="success">
      <?php elseif (date("Y-m-d") > $unAtelier['Atelier']['jour']): ?>
       <tr class="danger">
      <?php else: ?>
       <tr class="info">
      <?php endif ?>
        <td style="vertical-align:middle;">
         <?php 
          if (strlen($unAtelier['Atelier']['nom'])>$tablenght)
          {
           echo "<span class='tooltipspan' data-toggle='tooltip' title='".$unAtelier['Atelier']['nom']."'>".substr($unAtelier['Atelier']['nom'],0,$tablenght)."..."."</span>";
          }
          else
          {
           echo $unAtelier['Atelier']['nom'];
          }
         ?>
        </td>
        <td style="vertical-align:middle;">
         <?php 
          if (strlen($unAtelier['User']['nom'])>$tablenght)
          {
           echo "<span class='tooltipspan' data-toggle='tooltip' title='".$unAtelier['User']['nom']."'>".substr($unAtelier['User']['nom'],0,$tablenght)."..."."</span>";
          }
          else
          {
           echo $unAtelier['User']['nom'];
          }
         ?>
        </td>
        <td style="vertical-align:middle;">
         <?= $nomJour[$this->Time->format( $unAtelier['Atelier']['jour'], '%w')] ?>
         <?= $this->Time->format($unAtelier['Atelier']['jour'].' '.$unAtelier['Horaire']['heureDebut'], '%d/%m/%Y %H:%M ') ?>
        </td>
        <td style="vertical-align:middle;">
         <?= count($unAtelier['Inscription'])."/".$unAtelier['Atelier']['nombre_place'] ?>
        </td>
        <td class="text-center">
         <?= $this->Html->link('Imprimer', array('controller' => 'ateliers', 'action' => 'impression', $unAtelier['Atelier']['id']), array('class' => 'btn btn-info btn-sm', 'escape' => false)); ?>
        </td>
       </tr>
      <?php endforeach ?>
      </tbody>
     </table>
    </fieldset>
    <div class="modal-footer">
      <?= $this->Html->link('Gestion des classes', array('controller' => 'divisions', 'action' => 'gestion'), array('class' => 'btn btn-primary')); ?>
    </div>
   </div>
  </div>
 </div>
</div>
<script>
 $(document).ready(function() {
  var $rows = $(".liste tr");
  $('#inputChercher').keyup(function()
  {
   if ($('#inputChercher').val() != "")
   {
    $($("#inputChercher").parent()[0]).addClass("has-warning");
   }
   else
   {
    $($("#inputChercher").parent()[0]).removeClass("has-warning");    
   }
   var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
   $rows.show().filter(function()
   {
    var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
    return !~text.indexOf(val);
   }).hide();
  });
  $('.listab li a').click(function()
  {
   $('#inputChercher').val("");
   $($("#inputChercher").parent()[0]).removeClass("has-warning");
   var val = $.trim($("").val()).replace(/ +/g, ' ').toLowerCase();
   $rows.show().filter(function()
   {
    var text = $("").text().replace(/\s+/g, ' ').toLowerCase();
    return !~text.indexOf(val);
   }).hide(); 
  });
 });
</script>