<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<fieldset>
					<legend>
						Validation de l'atelier <strong><?= $atelier['Atelier']['nom'] ?></strong> du <?= $this->Time->format( $atelier['Atelier']['jour'].' '.$atelier['Horaire']['heureDebut'], '%d/%m/%Y %H:%M ') ?> :

						<?= $this->Html->link ( 
							'Modifier',
							array('controller' => 'ateliers', 'action' => 'crediter', 'rubrique' => $rubrique, $atelier['Atelier']['id']),
							array('class' => 'btn btn-success pull-right', 'style' => 'position:relative;top:-8px;'));
						?>
					</legend>
					<table class="table table-bordered table-striped table-condensed">
						<thead>
							<?php
								$optionsHead = array('class' => 'text-center');

								echo $this->Html->tableHeaders ( array ( 
									'Nom',
									'Prénom',
									'Classe',
									array('Présence' 		=> $optionsHead),
									array('Valider' 		=> $optionsHead),
									array('Commentaire' => $optionsHead)
								))
							?>
						</thead>
						<tbody>
							<?php
							$users = array();
								foreach ($usersCrediter as $key => $user) {
									$options = array('class' => 'text-center');
									$present = ($user['Creditation']['presence']) ? 'Oui' : 'Non';

									$users[] = array(
										$user['User']['nom'],
										$user['User']['prenom'],
										$user['Division']['nom'],
										array($present, $options),
										array($user['Creditation']['valid'], $options),
										array($user['Creditation']['commentaire'], $options)
									);
								}

								echo $this->Html->tableCells($users);

							 ?>
						</tbody>
					</table>
				</fieldset>
			</div>
		</div>
	</div>
</div>