<?php
	$nomJour        = array('Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi');
	$atelierCourant = $ateliersCourant[0];
?>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<fieldset>
					<legend>
						Gestion de l'atelier <strong><?= $atelierCourant['Atelier']['nom'] ?></strong>
						<?php if (!empty($listeAteliers)): ?>
							<p class="pull-right" style="position:relative;top:-8px;">
								<span class="label label-info">
									Périodique
								</span>
							</p>
						<?php elseif ($atelierCourant['Atelier']['hebdomadaire'] > 1): ?>
							<p class="pull-right" style="position:relative;top:-8px;">
								<span class="label label-info">
									Hebdomadaire
								</span>
							</p>
						<?php endif ?>
					</legend>

					<div class="row">
						<div class="col-lg-6">

							<table class="table table-bordered table-striped table-condensed">
								<tbody>
									<tr>
										<th>Nom</th>
										<td><?= $atelierCourant['Atelier']['nom']; ?></td>
									</tr>
									<tr>
										<th>Matière</th>
										<td><?= $atelierCourant['Matiere']['nom']; ?></td>
									</tr>
									<tr>
										<th>Date</th>
										<td><?= $this->Time->format( $atelierCourant['Atelier']['jour'].' '.$atelierCourant['Horaire']['heureDebut'], '%d/%m/%Y %H:%M ') ?></td>
									</tr>
									<tr>
										<th>Lieux</th>
										<td><?= $atelierCourant['Salle']['nom']; ?></td>
									</tr>
									<tr>
										<th>Nombre de places</th>
										<td><?= count($atelierCourant['Inscription'])."/".$atelierCourant['Atelier']['nombre_place']; ?></td>
									</tr>
									<tr>
										<th>Description</th>
										<td><?= $atelierCourant['Atelier']['description']; ?></td>
									</tr>
								</tbody>
							</table>

						</div>

						<div class="col-lg-6">
							<table class="table table-bordered table-condensed table-striped">
								<thead>
									<tr>
										<th>Classes</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($atelierCourant['Division'] as $key => $uneClasse): ?>
										<tr>
											<td><?= $uneClasse['nom'] ?></td>
										</tr>
									<?php endforeach ?>
								</tbody>
							</table>       
						</div>
					</div>

					<hr/>

					<!-- @TODO:Mettre en modal -->
					<table class="table table-bordered table-condensed">
						<tbody>
						<?= $this->Html->tableCells ( array(
									array (
										array('Pas encore validé', 		array('class' => "text-center")),
										array('Partiellement validé', array('class' => "warning text-center")),
										array('Complétement validé', 	array('class' => "info text-center"))
										),
									)
								)	
						?>
						</tbody>
					</table>

					<table class="table table-bordered table-hover table-condensed">
						<thead>
							<tr>
								<?= $this->Html->tableHeaders ( array ( 
										'Date',
										array('Action' => array('class' => "text-center"))
										)
									)
								?>
							</tr>
						</thead>
						<tbody>

							<?php
							
								$ateliers = array();
								foreach ($ateliersCourant as $key => $atelier) {
									$options  = array();
									$inscriptions = count($atelier['Inscription']);
									$creditations = count($atelier['Creditation']);
									//Ajouter la class pour le type de validation
									$options['class'] = "";
									if ($inscriptions > $creditations) {
										$options['class'] .= "warning";
									}
									elseif ($inscriptions == $creditations && $inscriptions != 0) {
										$options['class'] .= "info";
									}

									$dateEtHeure  = $nomJour[$this->Time->format( $atelier['Atelier']['jour'], '%w')];
									$dateEtHeure .= " ";
									$dateEtHeure .= $this->Time->format( $atelier['Atelier']['jour'].' '.$atelier['Horaire']['heureDebut'], '%d/%m/%Y %H:%M ');

									$optionsBtn = array('class' => 'btn btn-success btn-sm', 'escape' => false);
									$urlBtn 		= array('controller' => 'ateliers', 'action' => 'crediter', 'rubrique' => $rubrique, $atelier['Atelier']['id']);

									$btnCrediter = $this->Html->link('Créditer', $urlBtn, $optionsBtn);
									$urlBtn['action'] 		= 'creditation';
									$optionsBtn['class'] 	= 'btn btn-info btn-sm';
 									$btnVoir		 = $this->Html->link('Voir', $urlBtn, $optionsBtn);

 									$optionLigneBtn = $options;
 									$optionLigneBtn['class'] .= " text-center";

									$ateliers[] = array(
										array($dateEtHeure, $options),
										array($btnCrediter." ".$btnVoir, $optionLigneBtn),
									);
								}

								echo $this->Html->tableCells($ateliers);

							?>

						</tbody>
					</table>
					<hr/>
					<?= $this->Html->link('Imprimer',array('controller' => 'ateliers', 'action' => 'impression', $atelierCourant['Atelier']['id']), array('class' => 'btn btn-primary pull-right')); ?>
				</fieldset>
			</div>
		</div>
	</div>
</div>