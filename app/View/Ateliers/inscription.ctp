<?php
	$x=0;
	$nomJour = array('Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi');
?>

<div class="row">
	<?php foreach ($tousLesAteliers as $key => $atelier): ?>
		<?php if ($atelier['Periodique'] == false || ($atelier['Periodique'] && $atelier['Dates'])): ?>
			<div class="col-lg-6">
				<div class="panel panel-default">
					<div class="panel-heading"><b><?= $atelier['Nom'] ?></b></div>
					<ul class="list-group">
						<li class="list-group-item">
							<span class="label label-info"><?= $atelier['Matière'] ?></span>
							<span class="label label-warning"><?= $atelier['Lieu'] ?></span>
							<span class="label label-default">
								Nombre de places occupées : <?= $atelier['Inscriptions']."/".$atelier['Places']; ?>
							</span>
						</li>
					</ul>
					<div class="panel-body">
						<p><?= $atelier['Description'] ?></p>
					</div>
					<ul class="list-group">
						<li class="list-group-item" style="overflow: hidden;">
							<?= $this->Html->image('blue-user-icon.png', array('alt' => 'CakePHP', 'width' => "35px", 'class' => "img-polaroid img-rounded")) ?>
							<?= $atelier['Animateur'] ?>
							<span class="pull-right" style="font-size:14px;position:relative;top:8px;font-style:italic;margin-right:5px;">
								<?php if ($atelier['Periodique']): ?>
									<?php
										$heure = explode(' ', $atelier['Date et Heure']);
									?>
									<?php foreach ($atelier['Dates'] as $key => $date): ?>
										<?= $nomJour[$this->Time->format( $date, '%w')] ?>
										<?= $this->Time->format( $date.' '.$heure[1], '%d/%m/%Y %H:%M') ?>
										<br>
									<?php endforeach ?>
								<?php else: ?>
									<?= $nomJour[$this->Time->format( $atelier['Date et Heure'], '%w')] ?>
									<?= $this->Time->format( $atelier['Date et Heure'], '%d/%m/%Y %H:%M') ?>
								<?php endif ?>
							</span>
						</li>

						<?php if (!isset($atelier['Inscrit']) && $atelier['Inscriptions'] < $atelier['Places'] && $atelier['Date et Heure'] > date('Y-m-d') && $atelier['Id'] != ""): ?>
							<li class="list-group-item">
								<?= $this->Html->link('Inscription', array('controller' => 'ateliers', 'action' => 'inscription', 'rubrique' => $rubrique, $atelier['Id']), array('class' => 'btn btn-success', "style" => "width:100%")); ?>
							</li>
						<?php elseif ($atelier['Id'] != "" && isset($atelier['Inscrit']) && $atelier['Inscrit'] != ""): ?>
							<li class="list-group-item">
							<?= $this->Html->link ( 
								'Désinscription',
								array('controller' => 'ateliers', 'action' => 'desinscription', 'rubrique' => $rubrique, $atelier['Inscrit']),
								array('class' => 'btn btn-danger', "style" => "width:100%")
							); ?>
							</li>
						<?php endif ?>

					</ul>
				</div>
			</div>
		<?php endif ?>
	<?php endforeach ?>
</div>