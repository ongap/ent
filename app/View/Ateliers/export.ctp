<div class="span9">
	<div class="row-fluid">
		<div class="span12">
			<div class="thumbnail form-modal">
				<div class="modal-header">
					<h3> Exporter les ateliers :
					</h3>
				</div>


				<div class="modal-body modal-display-all-body" style="height:100%;">

					Choisir une date de début et de fin : <br>

					<?php
					$LabelOptions = array("class" => "col-lg-3 control-label");
					$options = array(
						"class" => "form-horizontal",
						"action" => "export",
						"inputDefaults" => array(
							"class" => "form-control",
							"div" => array("class" => "form-group"),
							"label" => $LabelOptions,
							"between" => "<div class='col-lg-9'>",
							"after" => "</div>",
							'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block')),
							"format" => array('before', 'label', 'between', 'input', 'error', 'after')
						),
					);
					echo $this->Form->create("Atelier", $options);

					$options = array(
						"class" => "form-control inputDatePicker",
						"type" => "text",
						"between" => "<div class='col-lg-9 toEmpty'>",
						"label" => array_merge($LabelOptions, array("text" => "Permier jour")),
						"dateFormat" => "YMD"
					);
					if ($this->Form->error("jour_debut")) { $options["div"] = "form-group has-error"; }
					echo $this->Form->input("jour_debut", $options);

					$options = array(
						"class" => "form-control inputDatePicker",
						"type" => "text",
						"between" => "<div class='col-lg-9 toEmpty'>",
						"label" => array_merge($LabelOptions, array("text" => "Dernier jour")),
						"dateFormat" => "YMD"
					);
					if ($this->Form->error("jour_fin")) { $options["div"] = "form-group has-error"; }
					echo $this->Form->input("jour_fin", $options);

					echo "Supprimer les ateliers exportés ?<br>";
					$options = array(
						"oui" => "Oui",
						"non" => "Non"
					);
					$attributes = array(
						'value' => "non",
						'separator' => "<br>",
						'legend' => false
					);
					echo $this->Form->radio("suppression", $options, $attributes);

					$options = array(
						"class" => "btn btn-success col-xs-12 col-sm-12 ",
						"between" => "<div class='col-lg-3 col-lg-offset-9'>",
						"after" => "</div>",
						"label" => false,
						"type" => "button"
					);
					echo $this->Form->input("Exporter", $options);

					echo $this->Form->end();
					?>

					<script>
						$(document).ready(function() {
							$.datepicker.setDefaults( $.datepicker.regional[ "fr" ] );
							$(".inputDatePicker").datepicker({dateFormat: "yy-mm-dd"});
						});
					</script>

				</div>

			</div>
		</div>
	</div>
</div>
