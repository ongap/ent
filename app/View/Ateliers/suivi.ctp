<?php
	$role = $this->Session->read('Auth.User.Role.nom_code');
	$optStyle = array('style' => "vertical-align:middle");
?>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<fieldset>
					<legend>
						<?= $titre ?> :
					</legend>
					<ul class="nav nav-pills nav-justified listab" style="margin-bottom:8px;">

						<li class="active">
							<a href="#atelier" data-toggle="tab">Suivi par atelier</a>
						</li>
						<li>
							<a href="#classe" data-toggle="tab">Suivi par classe</a>
						</li>
						<?php if ($role == 'WEBMASTER' || $role == 'ADMIN'): ?>
							<li>
								<a href="#eleve" data-toggle="tab">Suivi par élève</a>
							</li>
							<li>
								<a href="#prof" data-toggle="tab">Suivi par animateurs</a>
							</li>
						<?php endif ?>
						<li>
							<!-- -->
						</li>
						<li>
							<form class="form-inline" action="javascript:">
								<div class="form-group">
									<input id="inputChercher" type="text" class="form-control" placeholder="Chercher..." />
								</div>
							</form>
						</li>
					</ul>

					<div class="tab-content">
						<div class="tab-pane fade in active" id="atelier">

							<hr>
							<div class="col-lg-10">

								<?php
									$LabelOptions = array("class" => "col-lg-3 control-label");
									$options = array (
										"id" => "form_ajout_new",
										"class" => "form-horizontal",
										"inputDefaults" => array (
											"class" => "form-control",
											"div" => array("class" => "form-group"),
											"label" => $LabelOptions,
											"between" => "<div class='col-lg-9'>",
											"after" => "</div>",
											'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block')),
											"format" => array('before', 'label', 'between', 'input', 'error', 'after')
											),
										);
									echo $this->Form->create("Suivi", $options);
									$options = array (
										"class" => "inputDatePicker form-control",
										"type" => "text",
										"dateFormat" => "YMD",
										"label" => array_merge($LabelOptions, array("text" => "Première date :"))
									);
									echo $this->Form->input("dateDebut", $options);
									$options = array (
										"class" => "inputDatePicker form-control",
										"type" => "text",
										"dateFormat" => "YMD",
										"label" => array_merge($LabelOptions, array("text" => "Dernière date :"))
										);
									echo $this->Form->input("dateFin", $options);
									$options = array (
										"label" => false,
										"class" => "btn btn-success col-xs-12 col-sm-12",
										"between" => "<div class='col-lg-3 col-lg-offset-9'>",
										"type" => "button"
									);
									echo $this->Form->input("Rechercher", $options);     
									echo $this->Form->end();
								?>

							</div>
							<table class="table table-bordered table-striped table-hover table-condensed">
								<thead>
									<?= $this->Html->tableHeaders(array('Nom', 'Animateur', 'Date et Heure', 'Places', array('Action' => array('class' => 'text-center')))) ?>
								</thead>
								<tbody class="liste">
									<?php
										$ateliers = array();
										foreach ($tousLesAteliers as $key => $atelier) {
											$btn = $this->Html->link(
												'Imprimer',
												array (
													'controller' 	=> 'ateliers',
													'action'  		=> 'impression',
													'rubrique' 		=> $rubrique,
													$atelier['Id']
												),
												array ( 'class' => 'btn btn-info btn-sm', 'escape' => false )
											);

											if ( preg_match('/'.date('Y-m-d').'/', $atelier['Date et Heure']) ) {
												$etat = "info";
											}
											elseif ( $atelier['Date et Heure'] > date('Y-m-d') ) {
												$etat = "success";
											}
											elseif ( $atelier['Date et Heure'] < date('Y-m-d') ) {
												$etat = "danger";
											}

											$opt = array_merge($optStyle, array('class' => $etat));
											$optBtn = array_merge($optStyle, array('class' => $etat." text-center"));

											$ateliers[] = array (
												array($this->Atl->infoBulle($atelier['Nom'], 16), $opt),
												array($atelier['Animateur'], $opt),
												array($atelier['Date et Heure'], $opt),
												array($atelier['Inscriptions'].'/'.$atelier['Places'], $opt),
												array($btn, $optBtn)
											);
										}
									?>
									<?= $this->Html->tableCells($ateliers); ?>
								</tbody>
							</table>
						</div>

						<div class="tab-pane fade" id="classe">
							<table class="table table-bordered table-striped table-hover table-condensed">
								<thead>
									<?= $this->Html->tableHeaders(array('Classe', array('Action' => array('class' => 'text-center')))) ?>
								</thead>
								<tbody class="liste">
									<?php
										$divisions = array();
										foreach ($toutesLesClasses as $key => $division) {
											$btn = $this->Html->link(
												'Selectionner',
												array (
													'controller' 	=> 'ateliers',
													'action'  		=> 'suiviClasse',
													'rubrique' 		=> $rubrique,
													$key
												),
												array ( 'class' => 'btn btn-info btn-sm', 'escape' => false )
											);

											$divisions[] = array (
												array($this->Atl->infoBulle($division, 32), $optStyle),
												array($btn, array('style' => "vertical-align:middle", 'class' => "text-center"))
											);
										}
									?>
									<?= $this->Html->tableCells($divisions); ?>
								</tbody>
							</table>
						</div>

						<?php if ($role == 'WEBMASTER' || $role == 'ADMIN'): ?>

							<div class="tab-pane fade" id="eleve">
								<table class="table table-bordered table-striped table-hover table-condensed">
									<thead>
										<?= $this->Html->tableHeaders(array('Nom', 'Prénom', 'Classe', 'Nombre d\'inscription', 'Nombre de créditation', array('Action' => array('class' => 'text-center')))) ?>
									</thead>
									<tbody class="liste">
										<?php
											$eleves = array();
											foreach ($tousLesEleves as $key => $eleve) {

												$optionsBtn = array('class' => 'btn btn-success btn-sm', 'escape' => false);
												$urlBtn			= array('controller' => 'ateliers', 'action' => 'forcerEleve', 'rubrique' => $rubrique, $eleve['User']['id']);

												$boutons  = $this->Html->link('Inscrire', $urlBtn, $optionsBtn);
												$urlBtn['action'] 		= 'recapitulation';
												$optionsBtn['class'] 	= 'btn btn-info btn-sm';
												$boutons .= ' ';
												$boutons .= $this->Html->link('Récapitulation', $urlBtn, $optionsBtn);

												$eleves[] = array(
													array($this->Atl->infoBulle($eleve['User']['nom']), $optStyle),
													array($this->Atl->infoBulle($eleve['User']['prenom']), $optStyle),
													array($this->Atl->infoBulle($eleve['Division']['nom']), $optStyle),
													array(count($eleve['Inscription']), $optStyle),
													array(count($eleve['Creditation']), $optStyle),
													array($boutons, array('class' => 'text-center'))
												);
											}

											echo $this->Html->tableCells($eleves);
										?>
										
									</tbody>
								</table>

								<?= $this->Html->link('Imprimer', array('controller' => 'ateliers', 'action' => 'impressionSuivi', 'rubrique' => $rubrique, ), array('class' => 'btn btn-info btn-sm', 'escape' => false)) ?>

							</div>

							<div class="tab-pane fade" id="prof">
								<table class="table table-bordered table-striped table-hover table-condensed">
									<thead>
										<?= $this->Html->tableHeaders(array('Nom', 'Prénom', 'Nombre d\'atelier', array('Action' => array('class' => 'text-center')))) ?>
									</thead>
									<tbody class="liste">
										<?php 

											$animateurs = array();
											foreach ($tousLesAnimateurs as $key => $animateur) {

												$btn = $this->Html->link('Voir', array('controller' => 'ateliers', 'action' => 'suiviAnimateur', 'rubrique' => $rubrique, $animateur['User']['id']), array('class' => 'btn btn-info btn-sm', 'escape' => false));

												$animateurs[] = array(
													array($this->Atl->infoBulle($animateur['User']['nom']), 		$optStyle),
													array($this->Atl->infoBulle($animateur['User']['prenom']), 	$optStyle),
													array(count($animateur['Atelier']), 												$optStyle),
													array($btn, array('class' => 'text-center')),
												);
											}

											echo $this->Html->tableCells($animateurs);

										?>      
									</tbody>
								</table>
							</div>
						<?php endif ?>

					</div>
				</fieldset>
			</div>
		</div>
	</div>
</div>

<!-- Pour la fonction de rechercher-->
<script>
	$(document).ready(function() {
		var $rows = $(".liste tr");
		$('#inputChercher').keyup(function()
		{
			if ($('#inputChercher').val() != "")
			{
				$($("#inputChercher").parent()[0]).addClass("has-warning");
			}
			else
			{
				$($("#inputChercher").parent()[0]).removeClass("has-warning");    
			}
			var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
			$rows.show().filter(function()
			{
				var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
				return !~text.indexOf(val);
			}).hide();
		});
		$('.listab li a').click(function()
		{
			$('#inputChercher').val("");
			$($("#inputChercher").parent()[0]).removeClass("has-warning");
			var val = $.trim($("").val()).replace(/ +/g, ' ').toLowerCase();
			$rows.show().filter(function()
			{
				var text = $("").text().replace(/\s+/g, ' ').toLowerCase();
				return !~text.indexOf(val);
			}).hide(); 
		});
	});
</script>

<!-- Pour les dates piker-->
<script type="text/javascript">
	(function( $ ) {
		$.widget( "ui.dp", {
			_create: function() {
				var el = this.element.hide();
				this.options.altField = el;
				var input = this.input = $('<input>').insertBefore( el )
				input.focusout(function(){
					if(input.val() == ''){
						el.val('');
					}
				});
				input.datepicker(this.options)
				if(convertDate(el.val()) != null){
					this.input.datepicker('setDate', convertDate(el.val()));
				}
			},
			destroy: function() {
				this.input.remove();
				this.element.show();
				$.Widget.prototype.destroy.call( this );
			}
		});
		var convertDate = function(date){
			if(typeof(date) != 'undefined' && date != null && date != ''){
				return new Date(date);
			} else {
				return null;
			}
		}
	})( jQuery );
	$(document).ready(function() {
		$(".inputDatePicker").dp({
			dateFormat: 'yy-mm-dd'
		}); 
	});
</script>