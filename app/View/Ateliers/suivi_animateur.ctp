<?php
	$tablenght = 16;
?>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<fieldset>
					<legend>
						Liste des ateliers pour <?= $animateur['User']['civilite'].'. '.$animateur['User']['nom'].' '.$animateur['User']['prenom'] ?>
						<form class="form-inline pull-right" action="javascript:" style="position:relative;top:-8px;">
							<div class="form-group">
								<input id="inputChercher" type="text" class="form-control" placeholder="Chercher..." />
							</div>
						</form>
					</legend>
					<table class="table table-bordered table-striped table-hover table-condensed">
						<thead>
							<?= $this->Html->tableHeaders(array('Nom', 'Animateur', 'Date et heure', 'Places', array('Action' => array('class' => "text-center")))) ?>
						</thead>
						<tbody class="liste">
							<?php 
								$nomJour = array('Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi');


								$ateliers = array();
								foreach ($tousLesAteliers as $key => $atelier) {
									$btn  = $this->Html->link('Imprimer', array('controller' => 'ateliers', 'action' => 'impression', 'rubrique' => $rubrique, $atelier['Id']), array('class' => 'btn btn-info btn-sm', 'escape' => false));
									$btn .= ' ';
									$btn .= $this->Html->link('Créditation', array('controller' => 'ateliers', 'action' => 'creditation', 'rubrique' => $rubrique, $atelier['Id']), array('class' => 'btn btn-info btn-sm', 'escape' => false));

									$options = array('class' => 'info');
									if (date("Y-m-d H:i") < $atelier['Date et Heure']) {
										$options['class'] = 'success';
									}
									elseif (date("Y-m-d H:i") > $atelier['Date et Heure']) {
										$options['class'] = 'danger';
									}

									$optionsBtn = array('class' => $options['class'].' text-center');

									$ateliers[] = array(
										array($this->Atl->infoBulle($atelier['Nom']), $options),
										array($this->Atl->infoBulle($atelier['Animateur']), $options),
										array($nomJour[$this->Time->format( $atelier['Date et Heure'], '%w')].' '.$this->Time->format($atelier['Date et Heure'], '%d/%m/%Y %H:%M'), $options),
										array($atelier['Inscriptions'].'/'.$atelier['Places'], $options),
										array($btn, $optionsBtn)
									);
								}

								echo $this->Html->TableCells($ateliers);

							?>


						</tbody>
					</table>
				</fieldset>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		var $rows = $(".liste tr");
		$('#inputChercher').keyup(function()
		{
			if ($('#inputChercher').val() != "")
			{
				$($("#inputChercher").parent()[0]).addClass("has-warning");
			}
			else
			{
				$($("#inputChercher").parent()[0]).removeClass("has-warning");    
			}
			var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
			$rows.show().filter(function()
			{
				var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
				return !~text.indexOf(val);
			}).hide();
		});
		$('.listab li a').click(function()
		{
			$('#inputChercher').val("");
			$($("#inputChercher").parent()[0]).removeClass("has-warning");
			var val = $.trim($("").val()).replace(/ +/g, ' ').toLowerCase();
			$rows.show().filter(function()
			{
				var text = $("").text().replace(/\s+/g, ' ').toLowerCase();
				return !~text.indexOf(val);
			}).hide(); 
		});
	});
</script>