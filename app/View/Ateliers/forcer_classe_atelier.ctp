<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<fieldset>
					<legend>
						Inscriptions pour la classe <strong><?= $classeCourante[0]['Division']['nom'] ?></strong>:
					</legend>
					<?php
						$inscrits = count($atelier['Inscription']);
						$places  = $atelier['Atelier']['nombre_place'];

						//Traitement des ateliers
						$users = array();
						foreach ($classeCourante as $key => $user) {
							//Génération des boutons
							$urlLien 	= array (
								'controller' => 'ateliers',
								'action' => 'forcerClasseAtelier',
								'rubrique' => $rubrique,
								$user['Division']['id'],
								$atelier['Atelier']['id'],
								'inscription',
								$user['User']['id']
							);

							//Si l'id de l'élève apparait dans la liste d'inscription
							$atelier['Inscrit'] = "";
							foreach ($atelier['Inscription'] as $key => $inscrit) {
								if ($inscrit['user_id'] == $user['User']['id']) {
									$atelier['Inscrit'] = $inscrit['id'];
								}
							}

							$styleBtn	= array('escape' => false, 'class' => 'btn btn-success disabled btn-sm');
							$btn 			= $this->Html->link ('Inscrire', $urlLien, $styleBtn);
							if ($atelier['Inscrit'] == "" && $inscrits < $places){
								$styleBtn['class'] 	= 'btn btn-success btn-sm';
								$btn = $this->Html->link ('Inscrire', $urlLien, $styleBtn);
							}
							elseif ( $atelier['Inscrit'] != ""){
								$urlLien[2] 		= 'desinscription';
								$urlLien[3] 		= $atelier['Inscrit'];
								$styleBtn['class'] 	= 'btn btn-danger btn-sm';
								$btn = $this->Html->link ('Désinscrire', $urlLien, $styleBtn);
							}

							$users[] = array(
								array($user['User']['nom'], array('style' => "vertical-align:middle")),
								array($user['User']['prenom'], array('style' => "vertical-align:middle")),
								array($btn, array('style' => "vertical-align:middle", 'class' => "text-center"))
							);
						}

						$styleClass = "bg-info";
						if($inscrits == $places) {
							$styleClass = "bg-warning";
						}
					?>
					<p class="<?= $styleClass ?>" style="padding:15px;">
						Nombre de place occupée : 
						<strong>
							<?= $inscrits ?>
							/
							<?= $places ?>
						</strong>
					</p>

					<table class="table table-bordered table-striped table-hover table-condensed">

						<thead>
							<?= $this->Html->tableHeaders(array('Nom', 'Prénom', array('Action' => array('class' => 'text-center')))) ?>
						</thead>

						<tbody class="liste">
							<?= $this->Html->tableCells($users) ?>
						</tbody>

					</table>
				</fieldset>
			</div>
		</div>
	</div>
</div>