<?php
	$tablenght = 12;
?>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<fieldset>
					<legend>
						Ateliers de <?= $eleve['User']['nom'].' '.$eleve['User']['prenom'].' ('.$eleve['Division']['nom'].')' ?>:
					</legend>
					<table class="table table-bordered table-striped table-hover table-condensed">
						<thead>
							<?php if ($commentaire): ?>
								<?= $this->Html->tableHeaders(array('Atelier', 'Date et Heure', 'Présent', 'Crédit', 'Commentaire')) ?>
							<?php else: ?>
								<?= $this->Html->tableHeaders(array('Atelier', 'Date et Heure', 'Présent', 'Crédit')) ?>
							<?php endif ?>
						</thead>
						<tbody>
							<?php
								$nomJour 	= array('Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi');
								$ateliers = array();

								foreach ($ateliersBDD as $key => $atelier){
									foreach ($eleve['Inscription'] as $key => $inscript) {
										if ($inscript['atelier_id'] == $atelier['Id']) {
											foreach ($eleve['Creditation'] as $key => $credit) {
												if ($credit['atelier_id'] == $atelier['Id']) {
													$options = array('style' => 'vertical-align:middle;');

													$nomAtelier = $atelier['Nom'].' de <strong>'.$atelier['Animateur'].'</strong>';

													$optionsPresent = $options;
													$optionsCredit	= $options;

													$optionsPresent['style'] 	= $options['style'].'color:red;';
													$optionsCredit['style'] 	= $options['style'].'color:red;';

													if ($credit['presence']) {
														$optionsPresent['style'] 	= $options['style'].'color:green;';
													}

													if ($credit['valid'] > 0) {
														$optionsCredit['style'] 	= $options['style'].'color:green;';
													}

													$ligne = array(
														array($this->Atl->infoBulle($nomAtelier), $options),
														array($nomJour[$this->Time->format( $atelier['Date et Heure'], '%w')].' '.$this->Time->format($atelier['Date et Heure'], '%d/%m/%Y %H:%M'), $options),
														array(($credit['presence']) ? 'Oui' : 'Non', $optionsPresent),
														array($credit['valid'], $optionsCredit),
														);

													if ($commentaire) {
														$ligne[] = array($credit['commentaire'], $options);
													}

													$ateliers[] = $ligne;
												}
											}
										}
									}
								}

								echo $this->Html->tableCells($ateliers);
							?>
						</tbody>
					</table>
				</fieldset>
			</div>
		</div>
	</div>
</div>