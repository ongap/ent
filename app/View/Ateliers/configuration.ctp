<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <fieldset>
          <legend>
            Paramètres :
          </legend>

          <?php echo $this->element('menu.parametres'); ?>

          <hr/>
          <h4>Exporter les ateliers : </h4>
          <?= $this->Html->link('Exporter', array (
            'controller' => 'ateliers',
            'action' => 'export'
          ),
          array (
            'class' => 'btn btn-info',
          )); ?>
          <hr/>
          <h4>Configuration des ateliers : </h4>

          <div class="col-lg-8 col-lg-offset-2">
            <?php
              $options = array (
                "url"    => "/ateliers/configuration",
                "class"  => "form-horizontal",
                "action" => "update",
                "inputDefaults" => array (
                  "class"  => "",
                  "div"    => array("class" => "input-group"),
                  "after"  => "</div>",
                  "before" => '<span class="input-group-addon">',
                  "type"    => "checkbox",
                  "between" => '</span><span class="form-control" aria-label="...">',
                  "after"   => '</span>',
                  "format" => array('before', 'input', 'between', 'label', 'after')
                ),
              );
              echo $this->Form->create(false, $options);

              $options = array (
                "label"   => "Autoriser les élèves à se désinscrire",
                "checked" => ($this->request->data['Parametre']['desinEleve']) ? "checked" : null
              );
              echo $this->Form->input("desinEleve", $options);

              $options = array (
                "label"  => "Afficher l'atelier même quand il n'y a plus de place",
                "checked" => ($this->request->data['Parametre']['placeEleve']) ? "checked" : null
              );
              echo $this->Form->input("placeEleve", $options);

              $options = array (
                "label"  => "Afficher l'atelier même quand la date est passée",
                "checked" => ($this->request->data['Parametre']['dateAtlEleve']) ? "checked" : null
              );
              echo $this->Form->input("dateAtlEleve", $options);

              $options = array (
                "label"  => "Afficher les commentaires de créditation aux élèves",
                "checked" => ($this->request->data['Parametre']['comAtlEleve']) ? "checked" : null
              );
              echo $this->Form->input("comAtlEleve", $options);

              $options = array (
                "class" => "btn btn-success col-xs-12 col-sm-12 ",
                "before" => "<div class='col-lg-3 col-lg-offset-9'>",
                "after" => "</div>"
              );
              echo $this->Form->submit("Modifier", $options);

              echo $this->Form->end();
            ?>
            
          </div>

        </fieldset>
      </div>
    </div>
  </div>
</div>