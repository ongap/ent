<?php
	$users = array();
	foreach ($tousLesUsers as $key => $user) {
		$btn = $this->Html->link(
			'Selectionner',
			array (
				'controller' 	=> 'ateliers',
				'action' 			=> 'forcerEleve',
				'rubrique' 		=> $rubrique,
				$user['User']['id']
			),
			array ( 'class' => 'btn btn-success btn-sm', 'escape' => false )
		);

		$users[] = array (
			array($this->Atl->infoBulle($user['User']['nom'], 32), array('style' => "vertical-align:middle")),
			array($this->Atl->infoBulle($user['User']['prenom'],32), array('style' => "vertical-align:middle")),
			array($this->Atl->infoBulle($user['Division']['nom'], 32), array('style' => "vertical-align:middle")),
			array($btn, array('style' => "vertical-align:middle", 'class' => "text-center"))
		);
	}

	$divisions = array();
	foreach ($toutesLesClasses as $key => $division) {
		$btn = $this->Html->link(
			'Selectionner',
			array (
				'controller' 	=> 'ateliers',
				'action'  		=> 'forcerClasse',
				'rubrique' 		=> $rubrique,
				$key
			),
			array ( 'class' => 'btn btn-success btn-sm', 'escape' => false )
		);

		$divisions[] = array (
			array($this->Atl->infoBulle($division), array('style' => "vertical-align:middle")),
			array($btn, array('style' => "vertical-align:middle", 'class' => "text-center"))
		);
	}
?>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<fieldset>
					<legend>
						<?= $titre ?> :
					</legend>
					<ul class="nav nav-pills nav-justified listab" style="margin-bottom:8px;">
						<li class="active">
							<a href="#eleve" data-toggle="tab">Par élèves</a>
						</li>
						<li>
							<a href="#classe" data-toggle="tab">Par classes</a>
						</li>
						<li>
							<!-- vide -->
						</li>
						<li>
							<form class="form-inline" action="javascript:">
								<div class="form-group">
									<input id="inputChercher" type="text" class="form-control" placeholder="Chercher..." />
								</div>
							</form>
						</li>
					</ul>

					<div class="tab-content">

						<!-- Chargement des utilisateurs-->
						<div class="tab-pane fade in active" id="eleve">
							<table class="table table-bordered table-striped table-hover table-condensed">
								<thead>
									<?= $this->Html->tableHeaders(array('Nom','Prénom', 'Classe', array('Action' => array('class' => 'text-center')))) ?>
								</thead>
								<tbody class="liste">
									<?=	$this->Html->tableCells($users) ?>
								</tbody>
							</table>
						</div>

						<!-- Chargement des utilisateurs-->
						<div class="tab-pane fade" id="classe">
							<table class="table table-bordered table-striped table-hover table-condensed">
								<thead>
									<?= $this->Html->tableHeaders(array('Nom', array('Action' => array('class' => 'text-center')))) ?>
								</thead>
								<tbody class="liste">
									<?=	$this->Html->tableCells($divisions) ?>
								</tbody>
							</table>
						</div>

					</div>
				</fieldset>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		var $rows = $(".liste tr");
		$('#inputChercher').keyup(function() {
			if ($('#inputChercher').val() != "") {
				$($("#inputChercher").parent()[0]).addClass("has-warning");
			}
			else {
				$($("#inputChercher").parent()[0]).removeClass("has-warning");    
			}
			var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
			$rows.show().filter(function() {
				var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
				return !~text.indexOf(val);
			}).hide();
		});
		$('.listab li a').click(function() {
			$('#inputChercher').val("");
			$($("#inputChercher").parent()[0]).removeClass("has-warning");
			var val = $.trim($("").val()).replace(/ +/g, ' ').toLowerCase();
			$rows.show().filter(function() {
				var text = $("").text().replace(/\s+/g, ' ').toLowerCase();
				return !~text.indexOf(val);
			}).hide(); 
		});
	});
</script>
