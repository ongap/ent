<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<fieldset>
					<legend>
						Inscriptions pour la classe <strong><?= $classeCourante[0]['Division']['nom'] ?></strong>:
					</legend>

					<?php
					//Génération du tableau en-tête
					$tableauCommande = $this->Atl->enteteTableau("forcerClasse", $classeCourante[0]['Division']['id'], $annee, $numSemaine);

					//Traitement des ateliers
					$ateliers = array();
					foreach ($tousLesAteliers as $key => $atelier) {

						//Génération des boutons
						$urlLien 	= array (
							'controller' => 'ateliers',
							'action' => 'forcerClasseAtelier',
							'rubrique' => $rubrique,
							$classeCourante[0]['Division']['id'],
							$atelier['Id']
							);

						$styleBtn		= array('escape' => false, 'class' => 'btn btn-success btn-sm');
						$btn 				= $this->Html->link ('Selectionner', $urlLien, $styleBtn);
						$ateliers[]	= $this->Atl->genererLigne($atelier, $btn);
					}

					$tableau = $this->Atl->listeTableau($ateliers);

					echo $tableauCommande."\n".$tableau;
					?>

				</fieldset>
			</div>
		</div>
	</div>
</div>