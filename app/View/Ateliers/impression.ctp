<?php
$this->layout = 'print';
?>

<h2>Voici les élèves qui sont inscrits à l'atelier <?= $ateliersCourant['Atelier']['nom']; ?> pour le <?= $this->Time->format( $ateliersCourant['Atelier']['jour'].' '.$ateliersCourant['Horaire']['heureDebut'], '%d/%m/%Y %H:%M ') ?> :</h2>
<p style="float:right;font-size:30px;">Lieu : <?= $ateliersCourant['Salle']['nom'] ?> </p>
<table id="tableau_liste_eleve" class="table table-bordered table-striped table-hover table-condensed">
	<thead>
		<?= $this->Html->tableHeaders(array('Nom', 'Prénom', 'Classe', 'Présence', 'Valider', 'Commentaire')) ?>
	</thead>
	<tbody>
		<?php
			$listeInscript = array();
			foreach ($inscripts as $key => $inscript) {
				$listeInscript[] = array(
					$inscript['User']['nom'],
					$inscript['User']['prenom'],
					$inscript['Division']['nom'],
					array('<input type="checkbox" name=" " />', array('style' => "text-align:center;")),
					array('<input type="text" 		name=" " size="5" />', array('style' => "text-align:center;")),
					array('<input type="text" 		name=" " size="10"/>', array('style' => "text-align:center;")),
				);
			}
		?>
		<?= $this->Html->tableCells($listeInscript) ?>
	</tbody>
</table>
Animateur : <?= $ateliersCourant['User']['civilite'].". ".$ateliersCourant['User']['nom']." ".$ateliersCourant['User']['prenom']; ?>
</body>
</html>