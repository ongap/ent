<?php
	$tablenght = 12;
	$nomJour = array('Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi');
	$role = $this->Session->read('Auth.User.Role.nom_code');
?>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<fieldset>
					<legend>
						<?= $titre ?> :
						<?= $this->Html->link(
								'Ajouter',
								array('controller' => 'ateliers', 'rubrique' => $rubrique, 'action' => 'add'),
								array('class' => 'btn btn-success pull-right', 'style' => 'position:relative;top:-8px;')
							);
						?>
					</legend>

					<!-- @TODO:Mettre en modal -->
					<table class="table table-bordered table-condensed">
						<tbody>
						<?= $this->Html->tableCells ( array(
									array (
										'Pas encore validé',
										array('Partiellement validé', array('class' => "warning")),
										array('Complétement validé', 	array('class' => "info"))
									),
									array(array('Périodique : <i class="fa fa-calendar-o"></i>', array( 'colspan' => "3" ))),
									array(array('Hebdomadaire : <i class="fa fa-refresh"></i>', array( 'colspan' => "3" ))),
									array(array('Ponctuel : <i class="fa fa-exclamation"></i>', array( 'colspan' => "3" ))),
									)
								)	
						?>
						</tbody>
					</table>

					<div>
						<!-- onglet -->
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active"><a href="#tableau" aria-controls="tableau" role="tab" data-toggle="tab">Tableau</a></li>
							<li role="presentation"><a href="#calendrier" aria-controls="calendrier" role="tab" data-toggle="tab">Calendrier</a></li>
						</ul>

						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="tableau">
								<?php
									//Génération du tableau en-tête de navigation
									echo $this->Atl->enteteTableau("gestion", null, $annee, $numSemaine);

									//Traitement des ateliers
									$ateliers = array();
									foreach ($tousLesAteliers as $key => $atelier) {

										//Génération du dropdown menu
										$id = ($atelier['Parent'] == 0) ? $atelier['Id'] : $atelier['Parent'] ;
										$dropdown  = '<div class="btn-group">';

										$dropdown .= $this->Atl->simpleBouton('Actions <span class="caret"></span>', array('class' => "btn btn-default dropdown-toggle", 'data-toggle' => "dropdown", 'aria-expanded' => "false"));

										$dropdown .= '<ul class="dropdown-menu" role="menu" >';
										$dropdown .= $this->Atl->itemMenu('validation', 'Validation', $id, 'info');
										$dropdown .= $this->Atl->itemMenu('impression', 'Imprimer', $id, 'info');
										if (!($role == 'PROF' && $atelier['Verrouille'])) {
											$dropdown .= $this->Atl->itemMenu('update', 'Modifier', $id, 'success');
											$dropdown .= $this->Atl->itemMenu('remove', 'Supprimer', $id, 'warning');
											if ($atelier['Suspendu']) {
												$dropdown .= $this->Atl->itemMenu('suspendre', 'Reconduire', $id, 'success');
											}
											else{
												$dropdown .= $this->Atl->itemMenu('suspendre', 'Suspendre', $id, 'warning');
											}
										}

										$dropdown .= '</ul>';
										$dropdown .= '</div>';


										$options  = array();
										//Ajouter la class pour le type de validation
										if ($atelier['Inscriptions'] > $atelier['Creditations']) {
											$options['class'] = "warning";
										}
										elseif ($atelier['Inscriptions'] == $atelier['Creditations'] && $atelier['Inscriptions'] != 0) {
											$options['class'] = "info";
										}

										//Ajouter le style pour les suspendus
										if ($atelier['Suspendu'] == 1) {
											$options['style'] = "color:#777;";
										}

										//Ajouter l'idéograme pour le type d'aletier
										$ideogramme = '<i class="fa fa-exclamation"></i>';
										if ($atelier['Periodique'] != false) {
											$ideogramme = '<i class="fa fa-calendar-o"></i>';
										}
										elseif ($atelier['Hebdomadaire']) {
											$ideogramme = '<i class="fa fa-refresh"></i>';
										}

										$atelier['Nom'] = $ideogramme.$atelier['Nom'];
										$ateliers[] = $this->Atl->genererLigne($atelier, $dropdown, $options);
									}

									echo $this->Atl->listeTableau($ateliers);
								?>
							</div>

							<div role="tabpanel" class="tab-pane" id="calendrier">

								<?php
									//Génération du tableau en-tête de navigation
									//@TODO on perd le focus quand on est sur le calendrier et que l'on change de page
									//@TODO Les liens ne s'affiche pas correctement
									echo $this->Atl->enteteTableau("gestion", null, $annee, $numSemaine);
								?>

								<table class="table table-striped table-bordered table-hover table-condensed" style="table-layout:fixed">
									<?php
										$option['class'] = "text-center";
										$donneTete 		= array();
										$donneTete[] 	= "<!-- Vide -->"; //La première case est vide 

										$dto = new DateTime();
										$dto->setISODate($annee, $numSemaine);
										for ($i=0; $i < 7; $i++) { 
											$dateCourante = $dto->format('Y-m-d');
											$donneTete[] = ucfirst(strftime('%A %e', strtotime($dateCourante)));
											$dto->modify('+1 day');
										}

										echo $this->Html->tableHeaders($donneTete, null, $option);

										//Un tableau avec les ateliers sur les horaires et dates
										$calendrier = array();
										foreach ($horaires as $key => $value){
											$ligne 				= array();
											$ligne[] 			= 'De <strong>'.substr($value['Horaire']['heureDebut'], 0, -3).'</strong> à <strong>'.substr($value['Horaire']['heureFin'], 0, -3).'</strong>';

											$dto = new DateTime();
											$dto->setISODate($annee, $numSemaine);
											for ($i=0; $i < 7; $i++) {
												$cell = "";
												$dateCourante = $dto->format('Y-m-d');
												foreach ($tousLesAteliers as $key => $atelier) {
													$dateEtHeure = $dateCourante.' '.$value['Horaire']['heureDebut'];
													if ($atelier['Date et Heure'] == $dateEtHeure) {
														$title 		= $atelier['Nom']." de ".$atelier['Animateur'];
														$content 	= $atelier['Description']."\n";
														$cell .= '<button type="button" class="btn btn-default atl-popover" data-container="body" data-toggle="popover" data-placement="bottom" title="'.$title.'" data-content="'.$content.'" data-original-title="" title="">';
														$cell .= $this->Atl->truncate($atelier['Nom'], 10);
														$cell .= '</button>';
													}
												}
												$dto->modify('+1 day');
												$ligne[] = $cell;
											}

											$calendrier[] = $ligne;
										}
										echo $this->Html->tableCells($calendrier);
									?>

								</table>

							</div>
						</div>
					</div>

				</fieldset>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$('.atl-popover').popover()
</script>
