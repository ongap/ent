<?php
$tablenght = 16;
?>
<div class="row">
 <div class="col-lg-12">
  <div class="panel panel-default">
   <div class="panel-body">
    <fieldset>
     <legend>
       Gestion des actualités :
       <?= $this->Html->link('Nouvelle', array('controller' => 'actualites', 'action' => 'add'), array('class' => 'btn btn-success pull-right', 'style' => 'position:relative;top:-8px;')); ?>
     </legend>
     <table class="table table-bordered table-striped table-hover table-condensed">
      <thead>
       <tr>
        <th class="text-center">Titre</th>
        <th class="text-center">Création</th>
        <th class="text-center">Suspendu</th>
        <th class="text-center">Action</th>
       </tr>
      </thead>
      <tbody>
      <?php foreach ($tousLesNews as $key => $uneNew): ?>
       <?php if($uneNew['Actualite']['suspendre'] == 1): ?>
       <tr class="warning">
       <?php else: ?>
       <tr class="success">
       <?php endif ?>    
        <td style="vertical-align:middle" class="text-center">
         <?php 
          if (strlen($uneNew['Actualite']['titre'])>$tablenght)
          {
           echo "<span class='tooltipspan' data-toggle='tooltip' title='".$uneNew['Actualite']['titre']."'>".substr($uneNew['Actualite']['titre'],0,$tablenght)."..."."</span>";
          }
          else
          {
           echo $uneNew['Actualite']['titre'];
          }
         ?>   
        </td>
        <td style="vertical-align:middle" class="text-center"> <?= $this->Time->format('d/m/Y ', $uneNew['Actualite']['created']) ?></td>
        <td class="text-center">
        <?php 
         if ($uneNew['Actualite']['suspendre'] == 1)
         {
          echo "Oui";
         }
         else
         {
          echo "Non";
         }
        ?>
        </td>
        <td style="vertical-align:middle" class="text-center">
         <?php
          if ($uneNew['Actualite']['suspendre'] == 1) { $rs = "Reconduire"; }
          else { $rs = "Suspendre"; }
          echo $this->Html->link($rs, array('controller' => 'actualites', 'action' => 'suspendre', $uneNew['Actualite']['id']), array('class' => 'btn btn-warning btn-sm', 'escape' => false));
         ?>
         <?= $this->Html->link("Modifier", array('controller' => 'actualites', 'action' => 'update', $uneNew['Actualite']['id']), array('class' => 'btn btn-success btn-sm', 'escape' => false)); ?>
         <?= $this->Html->link("Supprimer", array('controller' => 'actualites', 'action' => 'remove', $uneNew['Actualite']['id']), array('class' => 'btn btn-danger btn-sm', 'escape' => false), "Voulez-vous vraiment supprimer cette actualité ?"); ?>
        </td>
       </tr>
      <?php endforeach ?>
      </tbody>
     </table>
    </fieldset>
   </div>
  </div>
 </div>
</div>