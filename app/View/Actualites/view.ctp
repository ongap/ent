<?php foreach ($tousLesNews as $key => $uneNew): ?>
<?php if($uneNew['Actualite']['suspendre'] == 0): ?>
<div class="row row-bot">
 <div class="col-lg-12">
  <div class="panel panel-default"> 
   <div class="panel-body">
    <fieldset>
     <legend>
      <?= $uneNew['Actualite']['titre'] ?>
     </legend>
     <?= $uneNew['Actualite']['contenu'] ?>
    </fieldset>
   </div>
   <div class="panel-footer">
   De <b><?= $uneNew['User']['civilite'].'. '.$uneNew['User']['nom'] ?></b>,
   le <em><?= $this->Time->format('d/m/Y ', $uneNew['Actualite']['created']);?></em>
   </div>
  </div>
 </div>
</div>
<?php endif ?>
<?php endforeach ?>