<div class="row">
 <div class="col-lg-12">
  <div class="panel panel-default">
   <div class="panel-body">
  <?php
   $LabelOptions = array("class" => "col-lg-3 control-label");
   $options = array
   (
    "class" => "form-horizontal",
    "action" => "add",
    "inputDefaults" => array
    (
     "class" => "form-control",
     "div" => array("class" => "form-group"),
     "label" => $LabelOptions,
     "between" => "<div class='col-lg-9'>",
     "after" => "</div>",
     'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block')),
     "format" => array('before', 'label', 'between', 'input', 'error', 'after')
    ),
   );
   echo $this->Form->create("Actualite", $options);
  ?>
   <fieldset>
    <legend>
      Ajouter un actualité :
    </legend>
    <?php
     $options = array
     (
      "placeholder" => "Titre",
      "label" => array_merge($LabelOptions, array("text" => "Titre"))
     );
     if ($this->Form->error("titre")) { $options["div"] = "form-group has-error"; }
     echo $this->Form->input("titre", $options);

     $options = array
     (
      'required' => false,
      "placeholder" => "Contenu",
      "label" => array_merge($LabelOptions, array("text" => "Contenu"))
     );
     if ($this->Form->error("contenu")) { $options["div"] = "form-group has-error"; }
     echo $this->Form->input("contenu", $options);
    ?>
    <div class="form-group">
     <div class="col-lg-3 col-lg-offset-3">
      <?= $this->Html->link("Retour", array("action" => "gestion"), array("class" => "btn btn-danger col-xs-12 col-sm-12", "style" => "margin-bottom:4px;")); ?>
     </div>
     <?php
      $options = array
      (
       "class" => "btn btn-success col-xs-12 col-sm-12 ",
       "before" => "<div class='col-lg-3 col-lg-offset-3'>",
       "after" => "</div>"
      );
      echo $this->Form->submit("Ajouter", $options);
     ?>
    </div>
   </fieldset>
   <?= $this->Form->end(); ?>
   </div>
  </div>
 </div>
</div>
<script>
$(document).ready(function() {
  tinymce.init({
    selector: "textarea",
    plugins: [
    "advlist autolink lists link image charmap print preview anchor",
    "searchreplace visualblocks code fullscreen",
    "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
  });
});
</script>
