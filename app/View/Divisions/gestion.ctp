<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<fieldset>
					<legend>
						Gestion des classes :
					</legend>
					<?php
					$LabelOptions = array("class" => "col-lg-3 control-label");
					$options = array (
						"class" => "form-horizontal",
						"action" => "add",
						"inputDefaults" => array (
							"class" => "form-control",
							"div" => array("class" => "form-group"),
							"label" => $LabelOptions,
							"between" => "<div class='col-lg-9'>",
							"after" => "</div>",
							'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block')),
							"format" => array('before', 'label', 'between', 'input', 'error', 'after')
							),
						);
					echo $this->Form->create("Division", $options);

					$options = array (
						"placeholder" => "Nom",
						"label" => array_merge($LabelOptions, array("text" => "Nom"))
						);
					if ($this->Form->error("nom")) { $options["div"] = "form-group has-error"; }
					echo $this->Form->input("nom", $options);

					$options = array (
						"class" => "btn btn-success col-xs-12 col-sm-12 ",
						"between" => "<div class='col-lg-3 col-lg-offset-9'>",
						"after" => "</div>",
						"label" => false,
						"type" => "button"
						);
					echo $this->Form->input("Ajouter", $options);

					echo $this->Form->end();
					?>
					<hr/>

					<table class="table table-bordered table-striped table-hover table-condensed" style="table-layout:fixed;">
						<thead>
							<tr>
								<th class="text-center">Classe</th>
								<th class="text-center">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($tousLesClasses as $key => $uneClasse): ?>
								<tr>
									<td style="vertical-align:middle" class="text-center"><?= $uneClasse['Division']['nom']; ?></td>
									<td style="vertical-align:middle" class="text-center">
										<?= $this->Html->link("Modifier", array('controller' => 'divisions', 'action' => 'update', $uneClasse['Division']['id']), array('class' => 'btn btn-success btn-sm', 'escape' => false)); ?>
										<?= $this->Html->link("Supprimer", array('controller' => 'divisions', 'action' => 'remove', $uneClasse['Division']['id']), array('class' => 'btn btn-danger btn-sm', 'escape' => false), "Voulez-vous vraiment supprimer cette classe ?"); ?>
									</td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</fieldset>
			</div>
		</div>
	</div>
</div>
