<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<fieldset>
					<legend>
						Modifier la classe :
					</legend>
					<?php
					$LabelOptions = array("class" => "col-lg-3 control-label");
					$options = array (
						"class" => "form-horizontal",
						"action" => "update",
						"inputDefaults" => array (
							"class" => "form-control",
							"div" => array("class" => "form-group"),
							"label" => $LabelOptions,
							"between" => "<div class='col-lg-9'>",
							"after" => "</div>",
							'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block')),
							"format" => array('before', 'label', 'between', 'input', 'error', 'after')
							),
						);
					echo $this->Form->create("Division", $options);

					$options = array (
						"placeholder" => "Nom",
						"label" => array_merge($LabelOptions, array("text" => "Nom"))
						);
					if ($this->Form->error("nom")) { $options["div"] = "form-group has-error"; }
					echo $this->Form->input("nom", $options);
					?>
					<div class="form-group">
						<div class="col-lg-3 col-lg-offset-3">
							<?= $this->Html->link("Retour", array("action" => "gestion"), array("class" => "btn btn-danger col-xs-12 col-sm-12", "style" => "margin-bottom:4px;")); ?>
						</div>
						<?php
						$options = array (
							"div" => false,
							"between" => false,
							"class" => "btn btn-success col-xs-12 col-sm-12 ",
							"before" => "<div class='col-lg-3 col-lg-offset-3'>",
							"after" => "</div>",
							"label" => false,
							"type" => "button"
							);
						echo $this->Form->input("Modifier", $options);     
						?>
					</div>
					<?= $this->Form->input('id', array('type' => 'hidden')) ?>
					<?= $this->Form->end(); ?>
				</fieldset>
			</div>
		</div>
	</div>
</div>
