<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-body">

				Cette rubrique contient un ou plusieurs ateliers. Ces ateliers seront égalements supprimés, et les élèves seront alors désinscrits<br>
				<?php foreach ($ateliers as $key => $atelier): ?>
				<?php
					$dto = new DateTime($atelier['Atelier']['jour']);
					$lien = array ( 
						'controller' 	=> 'ateliers',
						'rubrique' 		=> $atelier['Rubrique']['nom_canonique'],
						'action' 			=> 'gestion',
						$dto->format('Y'),
						$dto->format('W'),
					);
				?>
					<ul>
						<li>
							<?= $atelier["Atelier"]["nom"] ?> (<?= count($atelier["Inscription"]) ?>/<?= $atelier["Atelier"]["nombre_place"] ?> places)
							<?= $this->Html->link('voir', $lien, array('class' => 'btn btn-info btn-xs')); ?>
						</li>
					</ul>
				<?php endforeach ?>

				Etes-vous sure de vouloir supprimer cette rubrique ?
				<?= $this->Html->link('Oui, tout supprimer maintenant', array('controller' => 'rubriques', 'action' => 'supprimer', $rubriqueCourante['Rubrique']['id'] , true), array('class' => 'btn btn-danger pull-right', 'style' => 'position:relative;top:-8px;')); ?>

			</div>
		</div>
	</div>
</div>