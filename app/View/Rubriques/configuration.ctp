<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<fieldset>
					<legend>
						Paramètres :
					</legend>

					<?php echo $this->element('menu.parametres'); ?>

					<hr/>

					<?php if ($btnAjouter): ?>
						<?= $this->Html->link(
								'Ajouter',
								array('controller' => 'rubriques', 'action' => 'ajouter'),
								array('class' => 'btn btn-success pull-right', 'style' => 'position:relative;top:-8px;')
							);
						?>
					<?php else: ?>
						<?= $this->Html->link(
								'Ajouter',
								array(),
								array('class' => 'btn btn-success pull-right disabled', 'style' => 'position:relative;top:-8px;')
							);
						?>
					<?php endif ?>


					<h4>Configuration des rubriques : </h4>

					<table class="table table-bordered table-striped table-hover table-condensed">
						<?php
							$headers = array_merge ( 
								array('<!-- -->'),
								$rubriques['nom']
							);
							echo $this->Html->tableHeaders($headers);

							$cells = array();
							foreach ($rubriques as $key => $rubrique) {
								if ($key != 'id') {
									$ligne = array(ucfirst(str_replace('_', ' ', $key)));
									foreach ($rubrique as $key => $value) {
										$ligne[] = $value;
									}
									$cells[] = $ligne;
								}
								else{
									$ligneBtn = array(array('Action', array('style' => 'vertical-align:middle')));
									foreach ($rubrique as $key => $value) {

										$btnModifier = $this->Html->link(
											'Modifier',
											array (
												'controller' 	=> 'rubriques',
												'action'  		=> 'modifier',
												$value
											),
											array ( 'class' => 'btn btn-info btn-sm', 'escape' => false )
										);

										$btnSupprimer = "";
										if (count($rubrique) > 1) {
											$btnSupprimer = $this->Html->link(
												'Supprimer',
												array (
													'controller' 	=> 'rubriques',
													'action'  		=> 'supprimer',
													$value
												),
												array ( 'class' => 'btn btn-danger btn-sm', 'escape' => false )
											);
										}

										$ligneBtn[] = $btnModifier.' '.$btnSupprimer;
									}
								}
							}

							$cells[] = $ligneBtn;

							echo $this->Html->tableCells($cells);

						?>
					</table>

				</fieldset>
			</div>
		</div>
	</div>
</div>