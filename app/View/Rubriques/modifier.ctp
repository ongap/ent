<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<?php
				$LabelOptions = array("class" => "col-lg-3 control-label");
				$options = array
				(
					"class" => "form-horizontal",
					"inputDefaults" => array
					(
						"class" => "form-control",
						"div" => array("class" => "form-group"),
						"label" => $LabelOptions,
						"between" => "<div class='col-lg-9'>",
						"after" => "</div>",
						'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block')),
						"format" => array('before', 'label', 'between', 'input', 'error', 'after')
						),
					);
				echo $this->Form->create("Rubrique", $options);
				?>
				<fieldset>
					<legend>
						Modifier une Rubrique :
					</legend>
					<?php
					$options = array (
						"placeholder" => "Nom de la rubrique",
						"label" => array_merge($LabelOptions, array("text" => "Nom de la rubrique"))
					);
					if ($this->Form->error("nom")) { $options["div"] = "form-group has-error"; }
					echo $this->Form->input("nom", $options);

					$options = array (
						"label" => array_merge($LabelOptions, array("text" => "Position dans le menu")),
					);
					if ($this->Form->error("position")) { $options["div"] = "form-group has-error"; }
					echo $this->Form->input("position", $options);

					$options = array (
						"placeholder" => "Gestion des ateliers",
						"label" => array_merge($LabelOptions, array("text" => "Label de la gestion des ateliers"))
					);
					if ($this->Form->error("label_gestion")) { $options["div"] = "form-group has-error"; }
					echo $this->Form->input("label_gestion", $options);

					$options = array (
						"placeholder" => "Gestion des inscriptions",
						"label" => array_merge($LabelOptions, array("text" => "Label de la gestion des inscriptions"))
					);
					if ($this->Form->error("label_gestion_inscription")) { $options["div"] = "form-group has-error"; }
					echo $this->Form->input("label_gestion_inscription", $options);

					$options = array (
						"placeholder" => "Suivi des ateliers",
						"label" => array_merge($LabelOptions, array("text" => "Label des suivis"))
					);
					if ($this->Form->error("label_suivi")) { $options["div"] = "form-group has-error"; }
					echo $this->Form->input("label_suivi", $options);

					$options = array (
						"placeholder" => "Inscription",
						"label" => array_merge($LabelOptions, array("text" => "Label inscription"))
					);
					if ($this->Form->error("label_inscription")) { $options["div"] = "form-group has-error"; }
					echo $this->Form->input("label_inscription", $options);

					$options = array (
						"placeholder" => "Récapitulatif des inscriptions",
						"label" => array_merge($LabelOptions, array("text" => "Label de la récapitulation"))
					);
					if ($this->Form->error("label_recapitulation")) { $options["div"] = "form-group has-error"; }
					echo $this->Form->input("label_recapitulation", $options);

					?>
					<div class="form-group">
						<div class="col-lg-3 col-lg-offset-3">
							<?= $this->Html->link("Retour", array("action" => "configuration"), array("class" => "btn btn-danger col-xs-12 col-sm-12", "style" => "margin-bottom:4px;")); ?>
						</div>
						<?php
						$options = array
						(
							"class" => "btn btn-success col-xs-12 col-sm-12 ",
							"before" => "<div class='col-lg-3 col-lg-offset-3'>",
							"after" => "</div>"
							);
						echo $this->Form->submit("Modifier", $options);
						?>
					</div>
				</fieldset>
				<?= $this->Form->end(); ?>
				<script>
					$(document).ready(function() {
						$.datepicker.setDefaults( $.datepicker.regional[ "fr" ] );
						$(".inputDatePicker").datepicker({dateFormat: "yy-mm-dd"});
						tinymce.init({
							selector: "textarea",
							plugins: [
							"advlist autolink lists link image charmap print preview anchor",
							"searchreplace visualblocks code fullscreen",
							"insertdatetime media table contextmenu paste"
							],
							toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
						});
						$(".toEmpty").contents().filter(function(){ return this.nodeType === 3; }).remove();
					});
				</script>
			</div>
		</div>
	</div>
</div>