<?php

/**
 * La classe Actualité permet la gestion des actualités.
 */
class ActualitesController extends AppController {

	/**
	 * Ajouter une actualité
	 *
	 * Cette fonction permet d'ajouter une actualité via
	 * un champ "What you see is what you get"
	 *
	 * @return callback redirige vers la page de gestion de l'actualité en cas de réussite.
	 */
	public function add(){
		$this->Session->write('active', 'gestion_actualites');

		if ($this->request->is('post')) {
			$this->Actualite->create();
			$this->request->data['Actualite']['user_id'] = $this->Session->read('Auth.User.id');

			if ($this->Actualite->save($this->request->data)) {
				$this->Session->setFlash(__('L\'actualité est sauvegardée'), "success");
				return $this->redirect(array('action' => 'view'));
			}
			$this->Session->setFlash(__('L\'actualité n\'a pas été sauvegardée. Merci de réessayer.'), "failure");
		}
	}

	/**
	 * Modifier une actualité
	 *
	 * Cette fonction permet de modifier une actualité, sans en changer la parternité
	 *
	 * @param int $id id de l'actualité à modifier.
	 * @return callback redirige vers la page de gestion de l'actualité en cas de réussite.
	 */
	public function update($id = null) {
		$this->Session->write('active', 'gestion_actualites');

		$this->Actualite->id = $id;
		if (!$this->Actualite->exists()) {
			$this->Session->setFlash(__('Aucune actualité correspondante n\'a été trouvée'), "failure");
			return $this->redirect(array('action' => 'gestion'));
		}

		if ($this->request->is(array('post', 'put'))) {
			if ($this->Actualite->save($this->request->data)) {
				$this->Session->setFlash(__('L\'actualité a été mise à jour'), "success");
				return $this->redirect(array('action' => 'gestion'));
			}
			$this->Session->setFlash(__('La modification de l\'actualité n\'a pas été sauvegardée. Merci de réessayer.'), "failure");
		}
		else{
			$this->request->data = $this->Actualite->read(null, $id);
		}
	}

	/**
	 * Supprimer une actualité
	 *
	 * Cette fonction permet de supprimer une actualité
	 *
	 * @param int $id id de l'actualité à supprimer.
	 * @return callback redirige vers la page de gestion de l'actualité en cas de réussite.
	 */
	public function remove($id = null) {
		$this->Session->write('active', 'gestion_actualites');

		if ($this->Actualite->delete($id)) {
			$this->Session->setFlash(__('L\'actualité est supprimée'), "success");
			return $this->redirect(array('action' => 'gestion'));
		}
		$this->Session->setFlash(__('L\'actualité n\'a pas été supprimée. Merci de réessayer.'), "failure");
	}

	/**
	 * Gestion des actualités
	 *
	 * Cette fonction permet de gérer les actualités.
	 */
	public function gestion(){
		$this->Session->write('active', 'gestion_actualites');

		$tousLesNews = $this->Actualite->find('all', array(
        	'order' => array('Actualite.created' => 'desc')
    		));
		$this->set(compact('tousLesNews'));
	}

	/**
	 * Suspendre une actualité
	 *
	 * Cette fonction permet de suspendre ou non une actualité.
	 *
	 * @param int $id id de l'actualité à suspendre.
	 * @return callback redirige vers la page de gestion de l'actualité en cas de réussite.
	 */
	public function suspendre($id = null) {
		$this->Session->write('active', 'gestion_actualites');

		$actualite = $this->Actualite->findById($id);

		if (count($actualite) == 0) {
			$this->Session->setFlash(__('Aucune actualité correspondante n\'a été trouvée'), "failure");
			return $this->redirect(array('action' => 'gestion'));
		}

		if($actualite['Actualite']['suspendre']){
			$actualite['Actualite']['suspendre'] = false;
		}
		else{
			$actualite['Actualite']['suspendre'] = true;
		}

		if ($this->Actualite->save($actualite['Actualite'])) {
			$this->Session->setFlash(__('L\'actualité a été mise à jour'), "success");
			return $this->redirect(array('action' => 'gestion'));
		}
		$this->Session->setFlash(__(' La modification de l\'actualité n\'a pas été sauvegardée. Merci de réessayer.'), "failure");

	}

	/**
	 * Afficher les actualités
	 *
	 * Cette fonction permet d'afficher les actualités écrite par un administrateur.
	 * Ces actualités sont visibles de tous.
	 */
	public function view(){
		$this->Session->write('active', 'actualites');

		$tousLesNews = $this->Actualite->find('all', array(
        	'order' => array('Actualite.created' => 'desc')
    		));
		$this->set(compact('tousLesNews'));
	}

}
