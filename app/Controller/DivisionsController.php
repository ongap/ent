<?php

/**
* La classe Division permet la gestion des classes.
*/
class DivisionsController extends AppController {

	/**
	* Ajouter une classe
	*
	* Cette fonction permet d'ajouter une classe
	*
	* @return callback redirige vers la page de gestion des classes en cas de réussite.
	*/
	public function add(){
		$this->Session->write('active', 'gestion_des_classes');

		if ($this->request->is('post')) {
			$this->Division->create();

			if($this->Division->save($this->request->data)){
				$this->Session->setFlash(__('La classe est ajoutée.'), "success");
				return $this->redirect(array('action' => 'gestion'));
			}
			$this->Session->setFlash(__('La classe n\'a pas été enrégistrée.'), "failure");
		}
	}

	/**
	* Modifier une classe
	*
	* Cette fonction permet de modifier une classe
	*
	* @param int $id id de la classe à modifier.
	* @return callback redirige vers la page de gestion des classes en cas de réussite.
	*/
	public function update($id = null) {
		$this->Session->write('active', 'gestion_des_classes');

		$this->Division->id = $id;
		if (!$this->Division->exists()) {
			$this->Session->setFlash(__('Aucune classe correspondante n\'a été trouvée'), "failure");
			return $this->redirect(array('action' => 'gestion'));
		}

		if ($this->request->is(array('post', 'put'))) {
			if($this->Division->save($this->request->data)){
				$this->Session->setFlash(__('La classe a été mise à jour'), "success");
				return $this->redirect(array('action' => 'gestion'));
			}
			$this->Session->setFlash(__('La classe n\'a pas été enrégistrée.'), "failure");
		}

		if (!$this->request->data) {
			$this->request->data = $this->Division->read(null, $id);
		}
	}

	/**
	* Supprimer une classe
	*
	* Cette fonction permet de supprimer une classe
	*
	* @param int $id id de la classe à supprimer.
	* @return callback redirige vers la page de gestion des classes en cas de réussite.
	*/
	public function remove($id = null) {
		$this->Session->write('active', 'gestion_des_classes');

		if ($this->Division->delete($id)) {
			$this->Session->setFlash(__('La classe est supprimée'), "success");
			return $this->redirect(array('action' => 'gestion'));
		}
		$this->Session->setFlash(__('La classe n\'a pas été supprimée. Merci de réessayer.'), "failure");
	}

	/**
	* Afficher les classes
	*
	* Cette fonction permet d'afficher les classes définie par un administrateur.
	*/
	public function gestion(){
		$this->Session->write('active', 'gestion_des_classes');

		$tousLesClasses = $this->Division->find('all');
		$this->set(compact('tousLesClasses'));
	}
}
