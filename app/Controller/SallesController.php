<?php

/**
* La classe Salle permet la gestion des salles.
*/
class SallesController extends AppController {

	/**
	* Ajouter une salle
	*
	* Cette fonction permet d'ajouter une salle
	*
	* @return callback redirige vers la page de gestion des salles en cas de réussite.
	*/
	public function add(){
		$this->Session->write('active', 'gestion_des_salles');

		if ($this->request->is('post')) {
			$this->Salle->create();

			if ($this->Salle->save($this->request->data)) {
				$this->Session->setFlash(__('Salle ajoutée'), "success");
				return $this->redirect(array('action' => 'gestion'));
			}
			$this->Session->setFlash(__('La salle n\'a pas été enrégistrée.'), "failure");
		}
	}

	/**
	* Modifier une salle
	*
	* Cette fonction permet de modifier une salle
	*
	* @param int $id id de la salle à modifier.
	* @return callback redirige vers la page de gestion des salles en cas de réussite.
	*/
	public function update($id = null) {
		$this->Session->write('active', 'gestion_des_salles');

		$this->Salle->id = $id;
		if (!$this->Salle->exists()) {
			$this->Session->setFlash(__('Aucune salle correspondante n\'a été trouvée'), "failure");
			return $this->redirect(array('action' => 'gestion'));
		}

		if ($this->request->is(array('post', 'put'))) {
			if($this->Salle->save($this->request->data)){
				$this->Session->setFlash(__('La salle a été mise à jour'), "success");
				return $this->redirect(array('action' => 'gestion'));
			}
			$this->Session->setFlash(__('La salle n\'a pas été enrégistrée.'), "failure");
		}

		if (!$this->request->data) {
			$this->request->data = $this->Salle->read(null, $id);
		}
	}

	/**
	* Supprimer une salle
	*
	* Cette fonction permet de supprimer une salle
	*
	* @param int $id id de la salle à supprimer.
	* @return callback redirige vers la page de gestion des salles en cas de réussite.
	*/
	public function remove($id = null) {
		$this->Session->write('active', 'gestion_des_salles');

		if ($this->Salle->delete($id)) {
			$this->Session->setFlash(__('La salle est supprimée'), "success");
			return $this->redirect(array('action' => 'gestion'));
		}
		$this->Session->setFlash(__('La salle n\'a pas été supprimée. Merci de réessayer.'), "failure");
	}

	/**
	* Afficher les salles
	*
	* Cette fonction permet d'afficher les salles définie par un administrateur.
	*/
	public function gestion(){
		$this->Session->write('active', 'gestion_des_salles');

		$tousLesSalles = $this->Salle->find('all');
		$this->set(compact('tousLesSalles'));
	}

}
