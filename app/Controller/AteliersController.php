<?php

/**
 * La classe atelier permet la gestion des ateliers dans les différentes rubriques.
 */
class AteliersController extends AppController {

	/**
	 * @var string|array Table utilisée(s)
	 */
	public $uses = array('Atelier', 'Creditation', 'Periode', 'Parametre', 'User', 'Division', 'Rubrique');

	/**
	 * @var string|array Helper(s) utilisé(s)
	 */
	public $helpers = array('Html', 'Atl');

	/**
	 * génération des dates
	 *
	 * Cette fonction permet de généré un tableau de date
	 * dans le cas des ateliers hebdomadaires et périodiques
	 *
	 * @param string $premiereDate date de départ.
	 * @param int $nombreRepetition nombre de date à générer.
	 * @return array string les dates dans l'ordre chronologique.
	 */
	private function generationDate($premiereDate, $nombreRepetition){
		$dates = array($premiereDate);
		if ($nombreRepetition > 1) {
			$jour = explode("-", $dates[0]);
			$week = mktime(1, 1, 1, $jour[1], $jour[2], $jour[0]);
			for ($i = 1; $i < $nombreRepetition; $i++){
				$week = $week + 60*60*24*7;
				$NouvelleDate = date('Y-m-d', $week);
				$dates[$i] = $NouvelleDate;
			}
		}
		return $dates;
	}

	/**
	 * Obtenir le nombre de répétition par rapport à deux dates
	 *
	 * Cette fonction renvoi un entié réprésentant le nombre de
	 * semaine entre deux dates passées en paramétres
	 *
	 * @param string $premiereDate date de départ.
	 * @param string $derniereDate date de d'arrivé.
	 * @return int nom de répétition.
	 */
	private function generationRepetition($premiereDate, $derniereDate){
		$datetime1 = new DateTime($premiereDate);
		$datetime2 = new DateTime($derniereDate);
		$interval  = $datetime1->diff($datetime2);
		$jours     = intval($interval->format('%a'));
		return round($jours/7, 0, PHP_ROUND_HALF_DOWN);
	}

	/**
	 * Verifier la disponibilité d'une ressource
	 *
	 * Cette fonction permet de vérifier la disponibilité d'un
	 * animateur ou d'une salle sur une date donnée.
	 *
	 * @param string $champ nom de la ressource (user_id|salle_id).
	 * @param array string $dates les dates à vérifier.
	 * @param objet $dataPost information du formulaire.
	 * @param boolean $update une condition supplémentaire est requis pour une mise à jour d'atelier.
	 * @return string en cas d'erreur, la fonction renvoit la date de l'indisponibilité.
	 */
	private function disponible($champ, $dates, $dataPost, $update = false){
		for ($i = 0; $i < count($dates); $i++){
			$conditions = array(
				'conditions' => array(
					'Atelier.'.$champ.' =' => $dataPost['Atelier'][$champ],
					'Atelier.horaire_id =' => $dataPost['Atelier']['horaire_id'],
					'Atelier.jour ='       => $dates[$i]),
				'recursive' => 1
			);
			if ($update){
				$conditions['conditions']['Atelier.id !=']         = $dataPost['Atelier']['id'];
				$conditions['conditions']['Atelier.atelier_id !='] = $dataPost['Atelier']['id'];
			}

			$stop = $this->Atelier->find('count', $conditions);
			if ($stop){
				return $dates[$i];
			}
		}
		return '00-00-0000';
	}

	/**
	 * Verifier les listes
	 *
	 * Cette fonction permet de vérifier que les listes
	 * ne sont pas vide.
	 *
	 * @param string $rubrique nom canonique de la rubrique.
	 */
	private function integriteListe($rubrique) {
		$role = $this->Session->read('Auth.User.Role.nom_code');

		//Si un professeur est connecté, on récupére seulement son entrée dans la liste d'animateur
		if ($role == "PROF") {
			$prof_id = $this->Session->read('Auth.User.id');
			$users = $this->Atelier->User->find('all',array('fields' => array('User.id', 'User.nom', 'User.civilite'), 'conditions' => array('Role.nom_code' => 'PROF', 'User.id' => $prof_id), 'order' => array('User.nom ASC')));
			$user_list = Set::combine($users, '{n}.User.id', array('{0} {1}', '{n}.User.civilite', '{n}.User.nom'));
		}
		else{
			$users = $this->Atelier->User->find('all',array('fields' => array('User.id', 'User.nom', 'User.civilite'), 'conditions' => array('Role.nom_code' => 'PROF'), 'order' => array('User.nom ASC')));
			$user_list = Set::combine($users, '{n}.User.id', array('{0} {1}', '{n}.User.civilite', '{n}.User.nom'));
		}

		//$user_list
		$division_list = $this->Atelier->Division->find('list', array('fields' => array('Division.id', 'Division.nom')));
		$matiere_list = $this->Atelier->Matiere->find('list', array('fields' => array('Matiere.id', 'Matiere.nom')));
		$horaire_list = $this->Atelier->Horaire->find('list', array('fields' => array('Horaire.id', 'Horaire.heureDebut')));
		$salle_list = $this->Atelier->Salle->find('list', array('fields' => array('Salle.id', 'Salle.nom')));

		//les listes ne doivent pas être vide
		if (count($user_list) == 0) {
			$this->Session->setFlash(__('Pas d\'animateur, vous devez d\'abord en créer au moins un/une pour pouvoir créer un atelier'), "failure");
			return $this->redirect(array('action' => 'gestion', 'rubrique' => $rubrique));
		}
		if (count($division_list) == 0) {
			$this->Session->setFlash(__('Pas de classe, vous devez d\'abord en créer au moins une pour pouvoir créer un atelier'), "failure");
			return $this->redirect(array('action' => 'gestion', 'rubrique' => $rubrique));
		}
		if (count($matiere_list) == 0) {
			$this->Session->setFlash(__('Pas de matière, vous devez d\'abord en créer au moins une pour pouvoir créer un atelier'), "failure");
			return $this->redirect(array('action' => 'gestion', 'rubrique' => $rubrique));
		}
		if (count($horaire_list) == 0) {
			$this->Session->setFlash(__('Pas de plage horaire, vous devez d\'abord en créer au moins une (dans paramètres globaux) pour pouvoir créer un atelier'), "failure");
			return $this->redirect(array('action' => 'gestion', 'rubrique' => $rubrique));
		}
		if (count($salle_list) == 0) {
			$this->Session->setFlash(__('Pas de salle, vous devez d\'abord en créer au moins une pour pouvoir créer un atelier'), "failure");
			return $this->redirect(array('action' => 'gestion', 'rubrique' => $rubrique));
		}

		$this->set('users', $user_list);
		$this->set('divisions', $this->Atelier->Division->find('list', array('fields' => array('Division.id', 'Division.nom'))));
		$this->set('matieres', $this->Atelier->Matiere->find('list', array('fields' => array('Matiere.id', 'Matiere.nom'))));
		$this->set('horaires', $this->Atelier->Horaire->find('list', array('fields' => array('Horaire.id', 'Horaire.heureDebut'))));
		$this->set('salles', $this->Atelier->Salle->find('list', array('fields' => array('Salle.id', 'Salle.nom'))));
		$this->set('periodes', $this->Atelier->Periode->find('list', array('fields' => array('Periode.id', 'Periode.nom'))));
		$this->set('role', $role);
		$this->set('rubrique', $rubrique);
	}

	/**
	 * Les ateliers disponibles pour une classe
	 *
	 * @param int $idClasse id de la classe selectionné.
	 * @param string $dateDebut début de l'ensemble d'atelier
	 * @param string $dateFin fin de l'ensemble d'atelier
	 * @param bool $afficherPlein Afficher l'atelier même quand il n'y a plus de place
	 * @param bool $afficherPerime Afficher l'atelier même quand la date est passée
	 * @return array('Id', 'Nom', 'Matière', 'Description', 'Date et Heure', 'Animateur', 'Lieux', 'Places', 'Inscriptions') les ateliers trié dans un tableau
	 */
	private function atelierPourClasse($idClasse = null, $dateDebut = null, $dateFin = null, $afficherPlein = true, $afficherPerime = true, $idAnimateur = null, $withRubrique = true) {
		$rubrique = $this->request->params['rubrique'];

		//Préparation de la requete pour récuperer les ateliers
		$options = array (
			'joins' => 	array (
							array (
								'table' => 'ateliers_divisions',
								'alias' => 'AtelierDivision',
								'type' => 'INNER',
								'conditions' => array(
									'Atelier.id = AtelierDivision.atelier_id',
								)
							)
						),
			'fields' => array (
							'Atelier.id',
							'Atelier.nom',
							'Matiere.nom',
							'Atelier.description',
							'Horaire.heureDebut',
							'Atelier.jour',
							'User.civilite',
							'User.nom',
							'Salle.nom',
							'Atelier.nombre_place',
							'Atelier.suspendu',
							'Atelier.verrouille',
							'Atelier.periode_id',
							'Periode.nom',
							'Atelier.hebdomadaire',
							'Atelier.atelier_id',
							'Rubrique.nom_canonique'
						),
			'conditions' => array (),
			'order' => array (
				'Atelier.jour',
				'Atelier.horaire_id'
			)
		);

		if ($withRubrique) {
			$options['conditions']['Rubrique.nom_canonique'] = $rubrique;
		}

		if($idClasse){
			$options['conditions']['AtelierDivision.division_id'] = $idClasse;
			$options['conditions']['OR'] =  array (
				array ('Atelier.suspendu' => 0),
				array ('Atelier.suspendu' => null)
			);
		}

		if ($idAnimateur) {
			$options['conditions']['Atelier.user_id'] = $idAnimateur;
		}

		if (!$afficherPerime) {
			$options['conditions']['Atelier.jour >'] = date('Y-m-d');
		}

		if ($dateDebut) {
			if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$dateDebut)) {
				$options['conditions']['Atelier.jour >='] = $dateDebut;
			}
		}

		if ($dateFin) {
			if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$dateFin)) {
				$options['conditions']['Atelier.jour <='] = $dateFin;
			}
		}

		$tousLesAteliersBDD = $this->Atelier->find('all', $options);

		$ateliers = array();
		foreach ($tousLesAteliersBDD as $key => $atelier) {
			if (!(!$afficherPlein && count($atelier['Inscription']) == $atelier['Atelier']['nombre_place'])) {

				//Générer une liste avec les noms de classe
				$divisionsListe = array();
				foreach ($atelier['Division'] as $key => $division) {
					array_push($divisionsListe, $division['nom']);
				}
				asort($divisionsListe);
				$divisions = "";
				foreach ($divisionsListe as $key => $division) {
					$divisions .= $division.", ";
				}
				$divisions = substr($divisions, 0, -2);

				//Calcule de la position 
				//@TODO Vérifier que ça fonctionne avec les hebdo
				$PositionRep = '(1/1)';
				$dates       = false;
				$totalAtl = count($atelier['Atelier'])-10;
				if ($totalAtl != 0) {
					$totalAtl++;
					$dates = $this->generationDate($atelier['Atelier']['jour'], $totalAtl);
					$positionAtl = 1;
					$PositionRep = '('.$positionAtl.'/'.$totalAtl.')';
				}
				else if ($atelier['Atelier']['atelier_id'] != 0) {
					$atl = $this->Atelier->findById($atelier['Atelier']['atelier_id']);
					$totalAtl = count($atl['Atelier'])-14;
					$positionAtl = $this->generationRepetition($atl['Atelier']['jour'], $atelier['Atelier']['jour'])+1;
					$PositionRep = '('.$positionAtl.'/'.$totalAtl.')';
				}

				if ($PositionRep == '(1/1)' && $atelier['Atelier']['periode_id'] > 0) {
					$dates = array($atelier['Atelier']['jour']);
				}

				$ateliers[$atelier['Atelier']['id']] = array(
					'Id'            => $atelier['Atelier']['id'],
					'Nom'           => $atelier['Atelier']['nom'],
					'Matière'       => $atelier['Matiere']['nom'],
					'Description'   => $atelier['Atelier']['description'],
					'Date et Heure' => $atelier['Atelier']['jour'].' '.$atelier['Horaire']['heureDebut'],
					'Animateur'     => $atelier['User']['civilite'].'. '.$atelier['User']['nom'],
					'Lieu'          => $atelier['Salle']['nom'],
					'Classes'       => $divisions,
					'Places'        => $atelier['Atelier']['nombre_place'],
					'Inscriptions'  => count($atelier['Inscription']),
					'Periodique'    => ($atelier['Atelier']['periode_id'] > 0) ? $atelier['Periode']['nom'] : false,
					'PositionRep'   => $PositionRep,
					'Hebdomadaire'  => ($atelier['Atelier']['hebdomadaire'] > 1) ? true : false,
					'Dates'         => $dates,
					'Parent'        => $atelier['Atelier']['atelier_id'],
					'Rubrique'      => $atelier['Rubrique']['nom_canonique'],
				);
				if (!$idClasse) {
					$ateliers[$atelier['Atelier']['id']]['Suspendu']        = $atelier['Atelier']['suspendu'];
					$ateliers[$atelier['Atelier']['id']]['Verrouille']      = $atelier['Atelier']['verrouille'];
					$ateliers[$atelier['Atelier']['id']]['Creditations']    = count($atelier['Creditation']);
				}
			}
		}

		return $ateliers;
	}

	/**
	 * Les ateliers et inscription d'un élève
	 *
	 * @param int $idClasse id de la classe selectionné.
	 * @param string $dateDebut début de l'ensemble d'atelier
	 * @param string $dateFin fin de l'ensemble d'atelier
	 * @param bool $desinscription Autoriser la désinscription
	 * @param bool $afficherPlein Afficher l'atelier même quand il n'y a plus de place
	 * @param bool $afficherPerime Afficher l'atelier même quand la date est passée
	 * @return array('Id', Nom', 'Matière', 'Description', 'Date et Heure', 'Animateur', 'Lieux', 'Places', 
	 */
	private function atelierPourEleve($idEleve='', $dateDebut = null, $dateFin = null, $desinscription = true, $afficherPlein = true, $afficherPerime = true) {
		$rubrique = $this->request->params['rubrique'];

		$eleve = $this->User->findById($idEleve);
		if (!$eleve) {
			return false;
		}
		$ateliersPourInscriptions = $this->atelierPourClasse($eleve['Division']['id'], $dateDebut, $dateFin, true, $afficherPerime, null, false);

		//Si il n'a pas d'atelier à afficher, sortir de la fonction
		if(count($ateliersPourInscriptions) == 0) {
			return array();
		}

		//Préparation de la requete pour récuperer les inscriptions
		$options = array (
			'conditions' => array('Inscription.user_id' => $idEleve),
			'fields' => array (
				'Inscription.id',
				'Atelier.id',
				'Atelier.jour',
				'Horaire.heureDebut'
			)
		);

		$options['joins'] = array (
			array (
				'table' 		=> 'horaires',
				'alias' 		=> 'Horaire',
				'type' 			=> 'LEFT',
				'conditions' 	=> array(
					'Horaire.id = Atelier.horaire_id'
				)
			),
		);

		//Liste des inscriptions de $idEleve
		$Inscriptions = $this->Atelier->Inscription->find('all', $options);

		foreach ($Inscriptions as $key => $inscription) {
			//Verifier si l'utilisateur est inscript à ces ateliers
			if (isset($ateliersPourInscriptions[$inscription['Atelier']['id']])) {
				$ateliersPourInscriptions[$inscription['Atelier']['id']]['Inscrit'] = $inscription['Inscription']['id'];
				if (!$desinscription) {
					$ateliersPourInscriptions[$inscription['Atelier']['id']]['Id'] = "";
				}
			}

			// //Vérifier si l'utilisateur peut s'inscrire
			foreach ($ateliersPourInscriptions as $key => $atelier) {
				$datetimeIncription = $inscription['Atelier']['jour'].' '.$inscription['Horaire']['heureDebut'];
				$heure = explode(' ', $atelier['Date et Heure']);

				//Pas d'autre inscription en même temps
				if ($atelier['Date et Heure'] == $datetimeIncription
					&& isset($ateliersPourInscriptions[$inscription['Atelier']['id']]['Id'])
					&& $atelier['Id'] != $ateliersPourInscriptions[$inscription['Atelier']['id']]['Id']) {
					$ateliersPourInscriptions[$key]['Id'] = "";
				}
				if ($atelier['Dates'] != false) {
					foreach ($atelier['Dates'] as $date) {
						if ($datetimeIncription == $date.' '.$heure[1] && !isset($atelier['Inscrit'])) {
							$ateliersPourInscriptions[$key]['Id'] = "";
						}
					}
				}
			}
		}

		foreach ($ateliersPourInscriptions as $key => $atelier) {
			if (!$afficherPlein && $atelier['Places'] == $atelier['Inscriptions'] && !isset($atelier['Inscrit'])) {
				unset($ateliersPourInscriptions[$key]);
			}
			if ($atelier['Rubrique'] != $rubrique) {
				unset($ateliersPourInscriptions[$key]);
			}
		}

		return $ateliersPourInscriptions;
	}

	/**
	 * Vérifier un atelier
	 *
	 * @param int $idAtelier l'id de l'atelier
 	 * @return obj l'atelier trouvé sinon false
	 */
	private function verificationAtelier($idAtelier = null) {
		if ($idAtelier == null) {
			$this->Session->setFlash(__('Aucun n\'atelier selectionné'), "failure");
			return false;
		}

		$ateliersCourant = $this->Atelier->find ( 'all',
			array (	'conditions' => array ( 
						'OR' => array (	array('Atelier.id' => $idAtelier),	array('Atelier.atelier_id' => $idAtelier))
					)
				)
		);

		if (count($ateliersCourant) == 0) {
			$this->Session->setFlash(__('Aucun n\'atelier trouvé'), "failure");
			return false;
		}

		return $ateliersCourant;
	}

	/**
	 * Vérifier un utilisateur
	 *
	 * @param int $idUser l'id de l'utilisateur
	 * @return obj l'atelier trouvé sinon false
	 */
	private function verificationUtilisateur($idUser = null) {
		if ($idUser == null) {
			$this->Session->setFlash(__('Aucun n\'utilisateur à inscrire'), "failure");
			return false;
		}

		$utilisateurCourant = $this->User->findById ($idUser);
		if (count($utilisateurCourant) == 0) {
			$this->Session->setFlash(__('Aucun n\'utilisateur trouvé'), "failure");
			return false;
		}

		return $utilisateurCourant;
	}

	/**
	 * Vérifier une classe
	 *
	 * @param int $idDivision l'id d'une classe
	 * @return obj division trouvé sinon false
	 */
	private function verificationDivision($idDivision = null, $avecProf = false) {
		if ($idDivision == null) {
			$this->Session->setFlash(__('Aucune classe selectionné'), "failure");
			return false;
		}

		$options = array(
			'conditions' => array('Division.id' => $idDivision),
		);

		if (!$avecProf) {
			$options['conditions']['Role.nom_code ='] = 'ELEVE';
		}

		$divisionCourant = $this->User->find('all', $options);

		if (count($divisionCourant) == 0) {
			$this->Session->setFlash(__('Aucune classe trouvé'), "failure");
			return false;
		}

		return $divisionCourant;
	}

	/**
	 * Inscrire un élève à un atelier
	 *
	 * @param int $idAtelier l'id de l'atelier
	 * @param int $idEleve l'id de l'utilisateur
 	 * @return bool true si l'inscription est une réussite
	 */
	private function inscrire($idAtelier = null, $idEleve = null) {

		//Vérification des paramètres
		$ateliersCourant    = $this->verificationAtelier($idAtelier);
		$utilisateurCourant = $this->verificationUtilisateur($idEleve);
		if (!$ateliersCourant || !$utilisateurCourant) {
			return false;
		}

		//Vérifier qu'il s'agisse d'un atelier parent
		if (isset($ateliersCourant[0]['Atelier']['atelier_id']) && $ateliersCourant[0]['Atelier']['atelier_id'] != 0) {
			$ateliersCourant = $this->verificationAtelier($ateliersCourant[0]['Atelier']['atelier_id']);
		}

		$dates = array(
			array('Atelier.jour' => $ateliersCourant[0]['Atelier']['jour'])
		);

		$x = 0;
		while (isset($ateliersCourant[0]['Atelier'][$x])) {
			array_push($dates, array('Atelier.jour' => $ateliersCourant[0]['Atelier'][$x]['jour']));
			$x++;
		}

		//Est-ce que l'élève est disponible a cette heure ce jour là ?
		$conditions = array(
			'conditions' => array(
				'Inscription.user_id' => $utilisateurCourant['User']['id'],
				'Atelier.horaire_id'  => $ateliersCourant[0]['Atelier']['horaire_id'],
				"OR"                  => $dates
			),
			'recursive' => 1
		);

		$stop = $this->Atelier->Inscription->find('all', $conditions);

		if ($stop) {
			$this->Session->setFlash(__('Déjà inscrit(e) à un atelier pour cette même date et aux mêmes horaires'), "failure");
			return false;
		}

		//Vérification qu'il y est encore de la place
		foreach ($ateliersCourant as $key => $atelier) {
			$place   = $atelier['Atelier']['nombre_place'];
			$inscrit = count($atelier['Inscription']);
			if ($inscrit >= $place) {
				$this->Session->setFlash(__('Plus de place dans cette atelier'), "failure");
				return false;
			}
		}

		if ($ateliersCourant[0]['Atelier']['periode_id'] != null) {
			foreach ($ateliersCourant as $key => $value) {
				$this->Atelier->Inscription->create();
				$this->Atelier->Inscription->save(
					array(
						'Inscription' => array(
											'user_id' => $idEleve,
											'atelier_id' => $value['Atelier']['id'],
											'date_inscript' => date('Y-m-d H:i:s')
	            		)
	            	)
	            );
			}
		}
		else{
			$this->Atelier->Inscription->create();
			$this->Atelier->Inscription->save(
				array(
					'Inscription' => array(
										'user_id' => $idEleve,
										'atelier_id' => $idAtelier,
										'date_inscript' => date('Y-m-d H:i:s')
									)
				)
			);
		}

		$this->Session->setFlash('Votre inscription est prise en compte', 'success');
		return true;
	}

	/**
	 * Désinscrire un élève à un atelier
	 *
	 * @param int $idAtelier l'id de l'atelier
	 * @param int $idEleve l'id de l'utilisateur
 	 * @return bool true si l'inscription est une réussite
	 */
	private function desinscrire($idInscription = null) {

		//Vérification des paramètres
		$atelierId = $this->Atelier->Inscription->findById($idInscription);

		if (count($atelierId) == 0) {
			$this->Session->setFlash(__('Désinscription echoué, l\'atelier demandé n\'existe pas.'), "failure");
			return false;
		}

		$ateliersCourant = $this->verificationAtelier($atelierId['Atelier']['id']);
		if (!$ateliersCourant) {
			return false;
		}

		//Vérifier qu'il s'agisse d'un atelier parent
		if (isset($ateliersCourant[0]['Atelier']['atelier_id']) && $ateliersCourant[0]['Atelier']['atelier_id'] != 0) {
			$ateliersCourant = $this->verificationAtelier($ateliersCourant[0]['Atelier']['atelier_id']);
		}

		if ($ateliersCourant[0]['Atelier']['periode_id'] != null) {
			$suppression = 0;
			foreach ($ateliersCourant as $key => $value) {
				if ($this->Atelier->Inscription->deleteAll(array('Inscription.user_id' => $atelierId['User']['id'], 'Inscription.atelier_id' => $value['Atelier']['id']), false )){
					$suppression++;
				}
			}

			if ($suppression == count($ateliersCourant)) {
				$this->Session->setFlash(__('Désinscription prise en compte.'), "success");
				return true;
			}
		}
		else{
			if ($this->Atelier->Inscription->delete($idInscription)) {
				$this->Session->setFlash(__('Désinscription prise en compte.'), "success");
					return true;
			}
		}
		$this->Session->setFlash(__('Désinscription echoué, veuillez réessayer.'), "failure");
		return false;
	}

	/**
	 * Ajout d'un atelier
	 *
	 * @return callback redirige vers la page de gestion en cas de réussite.
	 */
	public function add() {
		$rubrique = $this->request->params['rubrique'];
		$this->Session->write('active', 'gestion_des_ateliers_'.$rubrique);

		//Vérification des listes
		$this->integriteListe($rubrique);

		if ($this->request->is('post')){
			//Création des jours hebdo
			$dateAtelier = $this->request->data['Atelier']['jour'];
			$nbRepetition = $this->request->data['Atelier']['hebdomadaire'];
			$statusAtl = null;
			$nouvelAtelierId = null;

			// Un atelier ne peut être périodique et hebdomadaire (au sens de ce site) en même temps
			if($this->request->data['Atelier']['periode_id'] != '' && $nbRepetition > 1){
				$this->Session->setFlash(__('Laisser le champ hebdomadaire sur 1 et le logiciel se chargera de créer des ateliers sur toute cette période. Sinon, merci de ne pas sélectionner pas de période'), "failure");
				return false;
			}

			// Si il s'agit d'une période
			if ($this->request->data['Atelier']['periode_id'] != '') {
				$PeriodeDebut = new DateTime($this->Periode->findById($this->request->data['Atelier']['periode_id'])['Periode']['debut']);
				$PeriodeFin = new DateTime($this->Periode->findById($this->request->data['Atelier']['periode_id'])['Periode']['fin']);
				$datetimeAtelier = new DateTime($dateAtelier);

				//Verification de la date qui doit être dans la période
				if ($PeriodeDebut <= $datetimeAtelier && $PeriodeFin >= $datetimeAtelier) {
					//Savoir combien il y a de "nom de date" entre ces deux dates
					$nbRepetition = 0;
					$interval     = new DateInterval('P1D');
					$period       = new DatePeriod($datetimeAtelier, $interval, $PeriodeFin);
					$dayName      = $datetimeAtelier->format('l');

					foreach($period as $day){
						if($day->format('D') === ucfirst(substr($dayName, 0, 3))){
							$nbRepetition++;
						}
					}

					$statusAtl = "periode";
				}
				else{
					$this->Session->setFlash(__('La date proposée ne rentre pas dans la période choisie (du '.$PeriodeDebut->format('d/m/Y').' au '.$PeriodeFin->format('d/m/Y').')'), "failure");
					return false;
				}
			}

			if($dateAtelier != ""){
				$dateHeb = $this->generationDate($dateAtelier, $nbRepetition);
			}else{
				$this->Session->setFlash(__('La date ne semble pas renseigné'), "failure");
				return false;
			}

			//Vérification de la dispo des prof pour chaque jour sur cette horaire
			$dateProf = $this->disponible('user_id', $dateHeb, $this->request->data);
			if ($dateProf != '00-00-0000') {
				$datetimeProf = new DateTime($dateProf);
				$this->Session->setFlash(__('Cette animateur a déjà un atelier sur cette horaire le '.$datetimeProf->format('d/m/Y')), "failure");
				return false;
			}

			//Vérification de la dispo des salle pour chaque jour sur cette horaire
			$dateSalle = $this->disponible('salle_id', $dateHeb, $this->request->data);
			if ($dateSalle != '00-00-0000') {
				$datetimeSalle = new DateTime($dateSalle);
				$this->Session->setFlash(__('Cette salle a déjà un atelier sur cette horaire le '.$datetimeSalle->format('d/m/Y')), "failure");
				return false;
			}

			//Gestion de la rubrique
			$rubriqueActuel = $this->Rubrique->findByNomCanonique($rubrique);
			$this->request->data['Atelier']['rubrique_id'] = $rubriqueActuel['Rubrique']['id'];

			//Enregistrement
			$this->Atelier->create();
			if ($this->Atelier->save($this->request->data)){

				if ($statusAtl == "periode") {
					$nouvelAtelierId = $this->Atelier->id;
				}

				if (!isset($this->request->data['Atelier']['suspendu'])) {
					$this->request->data['Atelier']['suspendu'] = 0;
				}

				if (!isset($this->request->data['Atelier']['verrouille'])) {
					$this->request->data['Atelier']['verrouille'] = 0;
				}

				for ($i = 1; $i < $nbRepetition; $i++){
					$this->Atelier->create();
					$this->Atelier->save(array('Atelier' => array(
						'jour'         => $dateHeb[$i],
						'horaire_id'   => $this->request->data['Atelier']['horaire_id'],
						'salle_id'     => $this->request->data['Atelier']['salle_id'],
						'user_id'      => $this->request->data['Atelier']['user_id'],
						'matiere_id'   => $this->request->data['Atelier']['matiere_id'],
						'nom'          => $this->request->data['Atelier']['nom'],
						'description'  => $this->request->data['Atelier']['description'],
						'nombre_place' => $this->request->data['Atelier']['nombre_place'],
						'hebdomadaire' => $this->request->data['Atelier']['hebdomadaire'],
						'Division'     => $this->request->data['Atelier']['Division'],
						'suspendu'     => $this->request->data['Atelier']['suspendu'],
						'verrouille'   => $this->request->data['Atelier']['verrouille'],
						'atelier_id'   => $nouvelAtelierId,
						'periode_id'   => $this->request->data['Atelier']['periode_id'],
						'rubrique_id'  => $rubriqueActuel['Rubrique']['id'],
					)));
				}
				$this->Session->setFlash(__('Les/l\'atelier(s) crée.'), "success");
				return $this->redirect(array('action' => 'gestion', 'rubrique' => $rubrique));
			} else {
				$this->Session->setFlash(__('L\'atelier n\'a pas été sauvegardé. Merci de réessayer.'), "failure");
				return false;
			}
		}
		if (!isset($this->request->data['Atelier']['hebdomadaire'])) {
			$this->request->data['Atelier']['hebdomadaire'] = 1;
		}
	}

	/**
	 * Modifier un atelier
	 *
	 * @param int $id id de l'atelier à modifier.
	 * @return callback redirige vers la page de gestion en cas de réussite.
	 */
	public function update($id = null) {
		$rubrique = $this->request->params['rubrique'];
		$this->Session->write('active', 'gestion_des_ateliers_'.$rubrique);

		//Vérification des listes
		$this->integriteListe($rubrique);
		
		if (!$id) {
			$this->Session->setFlash(__('Demande de modification invalide. L\'atelier n\'est pas identifié'), "failure");
			return $this->redirect(array('action' => 'gestion', 'rubrique' => $rubrique));
		}


		$atelier = $this->Atelier->findById($id);
		if (!$atelier) {
			$this->Session->setFlash(__('L\'atelier demandé n\'existe pas'), "failure");
			return $this->redirect(array('action' => 'gestion', 'rubrique' => $rubrique));
		}

		
		//Redirection vers l'atelier parent dans le cas des périodiques
		if($atelier['Atelier']['atelier_id'] != 0){
			$this->Session->setFlash(__('Redirection vers le premier atelier de cette serie'), "info");
			return $this->redirect(array('action' => 'update', 'rubrique' => $rubrique, $atelier['Atelier']['atelier_id']));
		}

		//Si c'est un prof, il n'a accées qu'a ses ateliers (et non verrouillé)
		if ($this->Session->read('Auth.User.Role.nom_code') == "PROF" && !($atelier['Atelier']['user_id'] == $this->Session->read('Auth.User.id') && $atelier['Atelier']['verrouille'] == false)) {
			$this->Session->setFlash(__('Vous n\'avez pas accès à cet atelier en modification'), "failure");
			return $this->redirect(array('action' => 'gestion', 'rubrique' => $rubrique));
		}

		if ($this->request->is(array('post', 'put'))) {
			$this->request->data['Atelier']['id'] = $id;

            //Création des jours hebdo
            $dateAtelier = $this->request->data['Atelier']['jour'];
            $nbRepetition = $this->request->data['Atelier']['hebdomadaire'];
			$statusAtl = null;
			$nouvelAtelierId = null;

			// Un atelier ne peut être périodique et hebdomadaire (au sens de ce site) en même temps
			if($this->request->data['Atelier']['periode_id'] != '' && $nbRepetition > 1){
				$this->Session->setFlash(__('Laisser le champ hebdomadaire sur 1 et le logiciel se chargera de créer des ateliers sur toute cette période. Sinon, merci de ne pas sélectionner pas de période'), "failure");
				return false;
			}

			//Si il s'agit d'une période
            if ($this->request->data['Atelier']['periode_id'] != '') {
				$PeriodeDebut = new DateTime($this->Periode->findById($this->request->data['Atelier']['periode_id'])['Periode']['debut']);
				$PeriodeFin = new DateTime($this->Periode->findById($this->request->data['Atelier']['periode_id'])['Periode']['fin']);
				$datetimeAtelier = new DateTime($dateAtelier);

				//Verification de la date qui doit être dans la période
				if ($PeriodeDebut <= $datetimeAtelier && $PeriodeFin >= $datetimeAtelier) {
					//Savoir combien il y a de "nom de date" entre ces deux dates
					$nbRepetition = 0;
					$interval     = new DateInterval('P1D');
					$period       = new DatePeriod($datetimeAtelier, $interval, $PeriodeFin);
					$dayName      = $datetimeAtelier->format('l');

					foreach($period as $day){
						if($day->format('D') === ucfirst(substr($dayName, 0, 3))){
							$nbRepetition++;
						}
					}

					$statusAtl = "periode";
				}
				else{
					$this->Session->setFlash(__('La date proposée ne rentre pas dans la période choisie (du '.$PeriodeDebut->format('d/m/Y').' au '.$PeriodeFin->format('d/m/Y').')'), "failure");
					return false;
				}
            }

			$dateHeb = $this->generationDate($dateAtelier, $nbRepetition);

			//Vérification de la dispo des prof pour chaque jour sur cette horaire
			$dateProf = $this->disponible('user_id', $dateHeb, $this->request->data, true);
			if ($dateProf != '00-00-0000') {
				$datetimeProf = new DateTime($dateProf);
				$this->Session->setFlash(__('Cette animateur a déjà un atelier sur cette horaire le '.$datetimeProf->format('d/m/Y')), "failure");
				return false;
			}

			//Vérification de la dispo des salle pour chaque jour sur cette horaire
			$dateSalle = $this->disponible('salle_id', $dateHeb, $this->request->data, true);
			if ($dateSalle != '00-00-0000') {
				$datetimeSalle = new DateTime($dateSalle);
				$this->Session->setFlash(__('Cette salle a déjà un atelier sur cette horaire le '.$datetimeSalle->format('d/m/Y')), "failure");
				return false;
			}

			//Si c'est un périodique
			if ($statusAtl == "periode"){
				$listeAtelierfils = $this->Atelier->findAllByAtelierId($id);

				if (!isset($this->request->data['Atelier']['suspendu'])) {
					$this->request->data['Atelier']['suspendu'] = 0;
				}

				if (!isset($this->request->data['Atelier']['verrouille'])) {
					$this->request->data['Atelier']['verrouille'] = 0;
				}

				//Mise à jour du premier atelier
				if(!$this->Atelier->save($this->request->data)) {
					$this->Session->setFlash(__('Le premier atelier n\'a pas été sauvegardé. Merci de réessayer.'), "failure");
					return false;
				}

				//On parcour les nouvelles dates
				$x = 0;
				for ($i = 1; $i < $nbRepetition; $i++){
					if (isset($listeAtelierfils[$x])) {
						$value = $listeAtelierfils[$x];
						$this->request->data['Atelier']['id']   = $value['Atelier']['id'];
					}
					else{
						$this->Atelier->create();
						unset($this->request->data['Atelier']['id']);
						$this->request->data['Atelier']['atelier_id'] = $id;
					}
					$this->request->data['Atelier']['jour'] = $dateHeb[$i];
					$this->request->data['Atelier']['rubrique_id'] = $atelier['Atelier']['rubrique_id'];
					$x++;
					if (!$this->Atelier->save($this->request->data)){
						$this->Session->setFlash(__('Les ateliers n\'ont pas été sauvegardé. Merci de réessayer.'), "failure");
						return false;
					}
				}

				//En cas de surplus d'atelier, suppression des derniers
				$nbAtelierFils = count($listeAtelierfils);
				if ($x < $nbAtelierFils) {
					for ($i=$x; $i < $nbAtelierFils; $i++) { 
						if(!$this->Atelier->delete($listeAtelierfils[$i]['Atelier']['id'])) {
							$this->Session->setFlash(__('Suppression des derniers ateliers impossible'), "failure");
							return false;
						}
					}
				}

				$this->Session->setFlash(__('Les ateliers sont modifiés.'), "success");
				return $this->redirect(array('action' => 'gestion', 'rubrique' => $rubrique));
            }
			else{
				$this->Atelier->id = $id;
				if($this->Atelier->save($this->request->data)){
					$this->Session->setFlash(__('L\'atelier est modifié.'), "success");
					return $this->redirect(array('action' => 'gestion', 'rubrique' => $rubrique));
				}
				$this->Session->setFlash(__('Cette atelier n\'a pas été modifié.'), "failure");
			}
	    }

	    if (!$this->request->data) {
	        $this->request->data = $atelier;
	    }
	}

	/**
	 * Ecran résumant les informations d'un atelier ainsi que les créditations
	 *
	 * @param int $idAtelier l'id de l'atelier
	 * @return callback redirige vers la page de gestion.
	 */
	public function validation($idAtelier = null) {
		$rubrique = $this->request->params['rubrique'];
		$this->Session->write('active', 'gestion_des_ateliers_'.$rubrique);

		$ateliersCourant = $this->verificationAtelier($idAtelier);
		if (!$ateliersCourant) {
			return $this->redirect(array('action' => 'gestion', 'rubrique' => $rubrique));
		}

		$this->set(compact('rubrique'));
		$this->set(compact('ateliersCourant'));
	}

	/**
	 * Formulaire pour créditer un atelier
	 *
	 * @param int $idAtelier l'id de l'atelier
	 * @return callback redirige vers la page de gestion.
	 */
	public function crediter($idAtelier = null) {
		$rubrique = $this->request->params['rubrique'];
		$this->Session->write('active', 'gestion_des_ateliers_'.$rubrique);

		//Vérification du paramètre
		$ateliersCourant = $this->verificationAtelier($idAtelier);
		if (!$ateliersCourant) {
			return $this->redirect(array('action' => 'gestion', 'rubrique' => $rubrique));
		}
		$atelier = $ateliersCourant[0];

		$options['joins'] = array (
			array (
				'table' 		=> 'creditations',
				'alias' 		=> 'Creditation',
				'type' 			=> 'LEFT',
				'conditions' 	=> array(
					'User.id = Creditation.user_id'
				)
			),
			array (
				'table' 		=> 'inscriptions',
				'alias' 		=> 'Inscription',
				'type' 			=> 'LEFT',
				'conditions' 	=> array(
					'User.id = Inscription.user_id'
				)
			)
		);

		$options['conditions'] = array(
			'Inscription.atelier_id' => $idAtelier
		);

		$options['fields'] = array(
			'DISTINCT User.id',
			'User.nom',
			'User.prenom',
			'Division.nom',
		);

		$usersCrediter = $this->User->find('all', $options);

		$this->set(compact('rubrique'));
		$this->set(compact('atelier'));
		$this->set(compact('usersCrediter'));

		if ($this->request->is('post')) {
			if($this->Creditation->saveAll($this->request->data)){
				$this->Session->setFlash('utilisateurs crédité', 'success');
				return $this->redirect(array('action' => 'creditation', 'rubrique' => $rubrique, $idAtelier));
			}
			else{
				$this->Session->setFlash('utilisateurs non crédité', 'failure');
				return;
			}
		}
		else{
			//Pré-remplir les champs
			$this->request->data['Creditation'] = array();

			foreach ($usersCrediter as $key => $user) {
				$vide = true;
				if (count($user['Creditation']) > 0) {

					foreach ($atelier['Creditation'] as $key => $credit) {
						if ($credit['user_id'] == $user['User']['id']) {
							$this->request->data[] = array( 'Creditation' => array(
								'id'			=> $credit['id'],
								'user_id'		=> $user['User']['id'],
								'atelier_id'	=> $idAtelier,
								'presence'		=> $credit['presence'],
								'valid'			=> $credit['valid'],
								'commentaire'	=> $credit['commentaire']
							));
							$vide = false;
						}
					}
				}
				if ($vide) {
					$this->request->data[] = array( 'Creditation' => array(
						'user_id' 		=> $user['User']['id'],
						'atelier_id' 	=> $idAtelier,
						'presence' 		=> '1',
						'valid' 		=> '1',
						'commentaire' 	=> 'RAS'
					));
				}
			}
		}
	}

	/**
	 * Résumé des créditations pour un atelier
	 *
	 * @param int $idAtelier l'id de l'atelier
	 * @return callback redirige vers la page de gestion.
	 */
	public function creditation($idAtelier = null) {
		$rubrique = $this->request->params['rubrique'];
		$this->Session->write('active', 'gestion_des_ateliers_'.$rubrique);

		//Vérification du paramètre
		$ateliersCourant = $this->verificationAtelier($idAtelier);
		if (!$ateliersCourant) {
			return $this->redirect(array('action' => 'gestion', 'rubrique' => $rubrique));
		}
		$atelier = $ateliersCourant[0];

		$options['joins'] = array (
			array (
				'table' 		=> 'creditations',
				'alias' 		=> 'Creditation',
				'type' 			=> 'LEFT',
				'conditions' 	=> array(
					'User.id = Creditation.user_id'
				)
			),
			array (
				'table' 		=> 'inscriptions',
				'alias' 		=> 'Inscription',
				'type' 			=> 'LEFT',
				'conditions' 	=> array(
					'User.id = Inscription.user_id'
				)
			)
		);

		$options['conditions'] = array(
			'Creditation.atelier_id' => $idAtelier
		);

		$options['fields'] = array(
			'DISTINCT User.id',
			'User.nom',
			'User.prenom',
			'Division.nom',
			'Creditation.presence',
			'Creditation.valid',
			'Creditation.commentaire',
		);

		$usersCrediter = $this->User->find('all', $options);

		$this->set(compact('rubrique'));
		$this->set(compact('atelier'));
		$this->set(compact('usersCrediter'));
	}

	/**
	 * Ecran principal avec les ateliers sous formes de tableau ou de calendrie
	 *
	 * @param int $annee
	 * @param int $numSemaine numéro de semaine selectionné
	 */
	public function gestion($annee = null, $numSemaine = null) {
		$rubrique = $this->request->params['rubrique'];
		$this->Session->write('active', 'gestion_des_ateliers_'.$rubrique);

		if ($numSemaine === null) {
			$numSemaine = date('W');
		}

		if ($annee === null) {
			$annee = date('Y');
		}

		if ($numSemaine > 52) {
			$numSemaine = 1;
			$annee++;
		}
		elseif ($numSemaine < 0){
			$numSemaine = 51;
			$annee--;
		}

		$dto = new DateTime();
		$dto->setISODate($annee, $numSemaine);
		$dateDebut = $dto->format('Y-m-d');
		$dto->modify('+6 days');
		$dateFin = $dto->format('Y-m-d');

		if ($this->Session->read('Auth.User.Role.nom_code') == 'PROF') {
			$tousLesAteliers = $this->atelierPourClasse(null, $dateDebut, $dateFin, true, true, $this->Session->read('Auth.User.id'));
		}
		else{
			$tousLesAteliers = $this->atelierPourClasse(null, $dateDebut, $dateFin);
		}
		$horaires = $this->Atelier->Horaire->find('all');

		$rubriqueActuel = $this->Rubrique->findByNomCanonique($rubrique);
		$titre = $rubriqueActuel['Rubrique']['label_gestion'];

		$this->set(compact('titre'));
		$this->set(compact('annee'));
		$this->set(compact('numSemaine'));
		$this->set(compact('horaires'));
		$this->set(compact('tousLesAteliers'));
		$this->set(compact('rubrique'));
	}

	/**
	 * Imprimer la liste d'élève pour un atelier
	 *
	 * @param int $idAtelier l'id de l'atelier
	 */
	public function impression($idAtelier = null) {
		$ateliers = $this->verificationAtelier($idAtelier);
		if (!$ateliers) {
			return $this->redirect(array('action' => 'gestion', 'rubrique' => $rubrique));
		}
		$ateliersCourant = $ateliers[0];

		$options['joins'] = array(
			array (
				'table' => 'inscriptions',
				'alias' => 'Inscription',
				'type' => 'INNER',
				'conditions' => array(
					'User.id = Inscription.user_id',
				)
			)
		);

		$options['conditions'] = array(
			'Inscription.atelier_id' => $idAtelier
		);

		$inscripts = $this->User->find('all', $options);

		$this->set(compact('ateliersCourant'));
		$this->set(compact('inscripts'));
	}

	/**
	 * Permet de supprimer les ateliers et désinscrit
	 *
	 * @param int $idAtelier l'id de l'atelier
	 * @return callback redirection sur la page de gestion
	 */
	public function remove($idAtelier = null) {
		$rubrique = $this->request->params['rubrique'];
		$this->Session->write('active', 'gestion_des_ateliers_'.$rubrique);

		//Vérification du paramètre
		$ateliers = $this->verificationAtelier($idAtelier);
		if (!$ateliers) {
			return $this->redirect(array('action' => 'gestion', 'rubrique' => $rubrique));
		}
		$atelier = $ateliers[0];

		//Si l'atelier est vérouillé, un professeur ne peut pas le suspendre ou réconduire
		if ($atelier['Atelier']['verrouille'] && $this->Session->read('Auth.User.Role.nom_code') == 'PROF') {
			$this->Session->setFlash('Vous n\'êtes pas autorisé à supprimer cette atelier', 'failure');
			return $this->redirect(array('action' => 'gestion', 'rubrique' => $rubrique));
		}

		if ($atelier['Atelier']['periode_id'] == null) {
			if ($this->Atelier->delete($idAtelier)) {
				$this->Session->setFlash('Atelier supprimé', 'success');
				return $this->redirect(array('action' => 'gestion', 'rubrique' => $rubrique));
			}
		}
		else{
			$idAtelier = intval($idAtelier);
			if ($this->Atelier->delete($idAtelier)) {
				if ($this->Atelier->deleteAll(array('Atelier.atelier_id' => $idAtelier))) {
					$this->Session->setFlash('Ateliers supprimé', 'success');
					return $this->redirect(array('action' => 'gestion', 'rubrique' => $rubrique));
				}
			}
		}
		$this->Session->setFlash('Ateliers non supprimé', 'failure');
		return $this->redirect(array('action' => 'gestion', 'rubrique' => $rubrique));
	}

	/**
	 * Permet de suspendre ou reconduire un atelier
	 *
	 * @param int $idAtelier l'id de l'atelier
	 * @return callback redirection sur la page de gestion
	 */
	public function suspendre($idAtelier = null) {
		$rubrique = $this->request->params['rubrique'];
		$this->Session->write('active', 'gestion_des_ateliers_'.$rubrique);

		//Vérification du paramètre
		$ateliersCourant = $this->verificationAtelier($idAtelier);
		if (!$ateliersCourant) {
			return $this->redirect(array('action' => 'gestion', 'rubrique' => $rubrique));
		}

		//Vérifier qu'il s'agisse d'un atelier parent
		if (isset($ateliersCourant[0]['Atelier']['atelier_id']) && $ateliersCourant[0]['Atelier']['atelier_id'] != 0) {
			$ateliersCourant = $this->verificationAtelier($ateliersCourant[0]['Atelier']['atelier_id']);
		}

		$atelier = $ateliersCourant[0];

		//Si l'atelier est vérouillé, un professeur ne peut pas le suspendre ou réconduire
		if ($atelier['Atelier']['verrouille'] && $this->Session->read('Auth.User.Role.nom_code') == 'PROF') {
			$this->Session->setFlash('Vous n\'êtes pas autorisé à modifier cette atelier', 'failure');
			return $this->redirect(array('action' => 'gestion', 'rubrique' => $rubrique));
		}

		if($atelier['Atelier']['suspendu']){
			$atelier['Atelier']['suspendu'] = false;
			$etat = "reconduit";
		}
		else{
			$atelier['Atelier']['suspendu'] = true;
			$etat = "suspendu";
		}

		//Vérifier si il s'agit d'un périodique
		if (isset($atelier['Atelier']['periode_id']) && $atelier['Atelier']['periode_id'] != 0) {
			$this->Atelier->updateAll(
				array('Atelier.suspendu'       => $atelier['Atelier']['suspendu']),
				array('OR' => array(
					array('Atelier.id'         => $atelier['Atelier']['id']),
					array('Atelier.atelier_id' => $atelier['Atelier']['id'])
				))
			);
			$this->Session->setFlash('modification prise en compte, ateliers '.$etat, 'success');
			return $this->redirect(array('action' => 'gestion', 'rubrique' => $rubrique));
		} else {
			if ($this->Atelier->save($atelier['Atelier'])) {
				$this->Session->setFlash('modification prise en compte, atelier '.$etat, 'success');
				return $this->redirect(array('action' => 'gestion', 'rubrique' => $rubrique));
			}			
		}
	}

	/**
	 * Affichage des ateliers et inscription pour les élèves
	 *
	 * @param int $id id de l'atelier où l'élève souhaite s'inscrire.
 	 * @return callback redirige vers la page d'inscription.
	 */
	public function inscription($id = null) {
		$rubrique = $this->request->params['rubrique'];
		$this->Session->write('active', 'gestion_des_ateliers_'.$rubrique);

		$p = $this->Parametre->findById(1);

		$tousLesAteliers = $this->atelierPourEleve($this->Session->read('Auth.User.id'), null, null, $p['Parametre']['desinEleve'], $p['Parametre']['placeEleve'], $p['Parametre']['dateAtlEleve']);

		$this->set(compact('tousLesAteliers'));
		$this->set(compact('rubrique'));

		//Si un id est définie, l'élève souhaite s'incrire à l'atelier du même id
		if ($id != null) {
			$this->inscrire($id, $this->Session->read('Auth.User.id'));
			return $this->redirect(array('action' => 'inscription', 'rubrique' => $rubrique));
		}
	}

	/**
	 * Désinscription pour les élèves
	 *	
	 * @param int $id id de l'inscription.
 	 * @return callback redirige vers la page d'inscription.
	 */
	public function desinscription($id = null) {
		$rubrique = $this->request->params['rubrique'];

		$this->desinscrire($id);
		
		return $this->redirect(array('action' => 'inscription', 'rubrique' => $rubrique));
	}

	/**
	 * Un administrateur ou un professeur principale peut inscrire ses élèves dans un atelier_id
	 */
	public function forcer(){
		$rubrique = $this->request->params['rubrique'];
		$this->Session->write('active', 'gestion_des_inscriptions_'.$rubrique);

		$optionsEleves = array ( 
			'conditions' 	=> array('Role.nom_code' => 'ELEVE'),
			'fields' 		=> array('User.id', 'User.nom', 'User.prenom', 'Division.nom')
		);
		$optionsClasses = array (
			'conditions'	=> array('Role.nom_code' => 'ELEVE'),
			'fields' 		=> array('Division.id', 'Division.nom'),
			'group'			=> array('Division.id')
		);

		//@TODO Faire en sorte que ce soit choisissable dans la configuration des ateliers
		/*
		if ($this->Session->read('Auth.User.Role.nom_code') == 'PROF') {
			$optionsClasses['conditions']['Division.nom'] 	= $this->Session->read('Auth.User.Division.nom');
			$optionsEleves['conditions']['Division.nom'] 	= $this->Session->read('Auth.User.Division.nom');
		}
		//*/
		$tousLesUsers = $this->User->find ( 'all', $optionsEleves );
		$toutesLesClassesBDD = $this->User->find ( 'all', $optionsClasses );

		$toutesLesClasses = array();
		foreach ($toutesLesClassesBDD as $key => $classe) {
			$toutesLesClasses[$classe['Division']['id']] = $classe['Division']['nom'];
		}

		$rubriqueActuel = $this->Rubrique->findByNomCanonique($rubrique);
		$titre = $rubriqueActuel['Rubrique']['label_gestion_inscription'];

		$this->set(compact('titre'));
		$this->set(compact('tousLesUsers'));
		$this->set(compact('toutesLesClasses'));
		$this->set(compact('rubrique'));
	}

	/**
	 * Ecran avec les ateliers affiché par semaines pour l'utilisateur $idUser
	 *
	 * @param int $idUser id de l'utilisateur selectionné
	 * @param int $annee
	 * @param int $numSemaine numéro de semaine selectionné
	 * @param string $action Peut être soit inscription ou desinscription
	 * @param int $valeur si inscription, il s'agit de l'id de l'atelier, sinon de l'id de l'inscription
 	 * @return callback redirige vers la page forcer ou forcerEleve.
	 */
	public function forcerEleve($idUser = null, $annee = null, $numSemaine = null, $action = null, $valeur = null){
		$rubrique = $this->request->params['rubrique'];
		$this->Session->write('active', 'gestion_des_inscriptions_'.$rubrique);

		//Vérification des paramètres
		$unUser = $this->verificationUtilisateur($idUser);
		if (!$unUser) {
			return $this->redirect(array('action' => 'forcer', 'rubrique' => $rubrique));
		}

		//@TODO Faire en sorte que ce soit choisissable dans la configuration des ateliers
		/*
		if ($this->Session->read('Auth.User.Role.nom_code') == 'PROF' && $this->Session->read('Auth.User.Division.id') != $unUser['Division']['id']) {
			$this->Session->setFlash('Vous n\'êtes pas autorisé à voir cette classe', 'failure');
			return $this->redirect(array('action' => 'forcer', 'rubrique' => $rubrique));
		}
		//*/

		if ($numSemaine === null) {
			$numSemaine = date('W');
		}

		if ($annee === null) {
			$annee = date('Y');
		}

		if ($numSemaine > 52) {
			$numSemaine = 1;
			$annee++;
		}
		elseif ($numSemaine < 0){
			$numSemaine = 51;
			$annee--;
		}

		//Si une action est en cours
		if ($action) {
			if ($action == "inscription") {
				$this->inscrire($valeur, $idUser);
			}
			else if ($action == "desinscription") {
				$this->desinscrire($valeur);
			}
			return $this->redirect(array('action' => 'forcerEleve', 'rubrique' => $rubrique, $idUser, $annee, $numSemaine));
		}

		$infoUser = array (
			'id' 	 	=> $unUser['User']['id'],
			'civilite'	=> $unUser['User']['civilite'],
			'nom' 	 	=> $unUser['User']['nom'],
			'prenom' 	=> $unUser['User']['prenom'],
			'classe' 	=> $unUser['Division']['nom']
		);

		$dto = new DateTime();
		$dto->setISODate($annee, $numSemaine);
		$dateDebut = $dto->format('Y-m-d');
		$dto->modify('+6 days');
		$dateFin = $dto->format('Y-m-d');

		$tousLesAteliers = $this->atelierPourEleve($idUser, $dateDebut, $dateFin);

		$this->set('numSemaine', $numSemaine);
		$this->set('annee', $annee);
		$this->set(compact('rubrique'));
		$this->set(compact('infoUser'));
		$this->set(compact('tousLesAteliers'));
	}

	/**
	 * Ecran avec les ateliers affiché par semaines pour la classe $idClasse
	 *
	 * @param int $idClasse id de la classe selectionné
	 * @param int $annee
	 * @param int $numSemaine numéro de semaine selectionné
 	 * @return callback redirige vers la page forcer ou forcerClasse.
	 */
	public function forcerClasse($idClasse = null, $annee = null, $numSemaine = null){
		$rubrique = $this->request->params['rubrique'];
		$this->Session->write('active', 'gestion_des_inscriptions_'.$rubrique);

		//@TODO Faire en sorte que ce soit choisissable dans la configuration des ateliers
		/*
		if ($this->Session->read('Auth.User.Role.nom_code') == 'PROF' && $this->Session->read('Auth.User.Division.id') != $idClasse) {
			$this->Session->setFlash('Vous n\'êtes pas autorisé à voir cette classe', 'failure');
			return $this->redirect(array('action' => 'forcer', 'rubrique' => $rubrique));
		}
		//*/

		//Vérification des paramètres
		$classeCourante = $this->verificationDivision($idClasse);
		if (!$classeCourante) {
			return $this->redirect(array('action' => 'forcer', 'rubrique' => $rubrique));
		}

		if ($numSemaine === null) {
			$numSemaine = date('W');
		}

		if ($annee === null) {
			$annee = date('Y');
		}

		if ($numSemaine > 52) {
			$numSemaine = 1;
			$annee++;
		}
		elseif ($numSemaine < 0){
			$numSemaine = 51;
			$annee--;
		}

		$dto = new DateTime();
		$dto->setISODate($annee, $numSemaine);
		$dateDebut = $dto->format('Y-m-d');
		$dto->modify('+6 days');
		$dateFin = $dto->format('Y-m-d');

		$tousLesAteliers = $this->atelierPourClasse($idClasse, $dateDebut, $dateFin);

		$this->set(compact('tousLesAteliers'));
		$this->set(compact('rubrique'));
		$this->set(compact('classeCourante'));
		$this->set(compact('numSemaine'));
		$this->set(compact('annee'));
	}

	/**
	 * Ecran avec les ateliers affiché par semaines pour la classe $idClasse
	 *
	 * @param int $idClasse id de la classe selectionné
	 * @param int $idAtelier id de l'atelier selectionné
 	 * @param string $action Peut être soit inscription ou desinscription
	 * @param int $valeur si inscription, il s'agit de l'id de l'atelier, sinon de l'id de l'inscription
 	 * @return callback redirige vers la page forcer ou forcerClasse.
	 */
	public function forcerClasseAtelier($idClasse = null, $idAtelier = null, $action = null, $valeur = null){
		$rubrique = $this->request->params['rubrique'];
		$this->Session->write('active', 'gestion_des_inscriptions_'.$rubrique);

		//Vérification des paramètres
		$classeCourante 	= $this->verificationDivision($idClasse);
		$atelierCourant 	= $this->verificationAtelier($idAtelier); 

		if (!$classeCourante || !$atelierCourant) {
			return $this->redirect(array('action' => 'forcer', 'rubrique' => $rubrique));
		}

		//Si une action est en cours
		if ($action) {
			if ($action == "inscription") {
				$this->inscrire($idAtelier, $valeur);
			}
			else if ($action == "desinscription") {
				$this->desinscrire($valeur);
			}
			return $this->redirect(array('action' => 'forcerClasseAtelier', 'rubrique' => $rubrique, $idClasse, $idAtelier));
		}

		$this->set('atelier', $atelierCourant[0]);
		$this->set(compact('classeCourante'));
		$this->set(compact('rubrique'));
	}

	/**
	 * Ecran pour le suivi des élèves et des animateurs
	 */
	public function suivi(){
		$rubrique = $this->request->params['rubrique'];
		$this->Session->write('active', 'suivi_des_ateliers_'.$rubrique);


		if ($this->request->is('post')){
			$tousLesAteliers = $this->atelierPourClasse(null, $this->request->data['Suivi']['dateDebut'], $this->request->data['Suivi']['dateFin']);
		}
		else{
			$dto = new DateTime();
			$dto->modify('-6 days');
			$dateDebut = $dto->format('Y-m-d');
			$dto->modify('+12 days');
			$dateFin = $dto->format('Y-m-d');

			$tousLesAteliers = $this->atelierPourClasse(null, $dateDebut, $dateFin);

			$this->request->data = array(
				'Suivi' => array(
					'dateDebut' => $dateDebut,
					'dateFin' => $dateFin,
				)
			);
		}

		$optionsClasses = array (
				'conditions'	=> array('Role.nom_code' => 'ELEVE'),
				'fields' 		=> array('Division.id', 'Division.nom'),
				'group'			=> array('Division.id')
		);
		$optionsEleves = array ( 
				'conditions' => array('Role.nom_code' => 'ELEVE'),
				'fields' => array('User.id', 'User.nom', 'User.prenom', 'Division.nom')
		);

		if ($this->Session->read('Auth.User.Role.nom_code') == 'PROF') {
			$optionsClasses['conditions']['Division.nom'] 	= $this->Session->read('Auth.User.Division.nom');
			$optionsEleves['conditions']['Division.nom'] 	= $this->Session->read('Auth.User.Division.nom');
		}

		$toutesLesClassesBDD = $this->User->find ( 'all', $optionsClasses );
		$toutesLesClasses = array();
		foreach ($toutesLesClassesBDD as $key => $classe) {
			$toutesLesClasses[$classe['Division']['id']] = $classe['Division']['nom'];
		}


		$tousLesEleves = $this->User->find ( 'all', $optionsEleves );
		$tousLesAnimateurs = $this->User->find('all', array('conditions' => array('Role.nom_code' => 'PROF')));

		$rubriqueActuel = $this->Rubrique->findByNomCanonique($rubrique);
		$titre = $rubriqueActuel['Rubrique']['label_suivi'];

		$this->set(compact('titre'));
		$this->set(compact('rubrique'));
		$this->set(compact('tousLesEleves'));
		$this->set(compact('tousLesAteliers'));
		$this->set(compact('toutesLesClasses'));
		$this->set(compact('tousLesAnimateurs'));
	}

	/**
	 * Résumé des inscriptions et créditations d'une classe
	 * @param int $idClasse id de la classe selectionné
	 * @return callback redirige vers la page suivi.
 	 */
	public function suiviClasse($idClasse = null){
		$rubrique = $this->request->params['rubrique'];
		$this->Session->write('active', 'suivi_des_ateliers_'.$rubrique);

		if ($this->Session->read('Auth.User.Role.nom_code') == 'PROF' && $this->Session->read('Auth.User.Division.id') != $idClasse) {
			$this->Session->setFlash('Vous n\'êtes pas autorisé à voir cette classe', 'failure');
			return $this->redirect(array('action' => 'suivi', 'rubrique' => $rubrique));
		}

		$classeCourante = $this->verificationDivision($idClasse);
		if (!$classeCourante) {
			return $this->redirect(array('action' => 'suivi', 'rubrique' => $rubrique));
		}

		$tousLesAteliers = array();

		if ($this->request->is('post')){
			$tousLesAteliers = $this->atelierPourClasse($idClasse, $this->request->data['Atelier']['dateDebut'], $this->request->data['Atelier']['dateFin']);
		}

		$this->set(compact('rubrique'));
		$this->set(compact('classeCourante'));
		$this->set(compact('tousLesAteliers'));
	}

	/**
	 * Résumé des ateliers pour un animateur
	 * @param int $idAnimateur id de l'animateur selectionné
	 * @return callback redirige vers la page suivi.
	 */
	public function suiviAnimateur($idAnimateur = null){
		$rubrique = $this->request->params['rubrique'];
		$this->Session->write('active', 'suivi_des_ateliers_'.$rubrique);

		$animateur = $this->verificationUtilisateur($idAnimateur);
		if (!$animateur) {
			return $this->redirect(array('action' => 'suivi', 'rubrique' => $rubrique));
		}

		$tousLesAteliers = $this->atelierPourClasse(null, null, null, true, true, $idAnimateur);

		$this->set(compact('rubrique'));
		$this->set(compact('animateur'));
		$this->set(compact('tousLesAteliers'));
	}

	/**
	 * Récapitulation des ateliers validé ou non pour les élèves et professeur principaux
	 * @param int $idEleve id de l'élève selectionné
	 * @return callback redirige vers la page suivi.
	 */
	public function recapitulation($idEleve = null){
		$rubrique = $this->request->params['rubrique'];
		$this->Session->write('active', 'suivi_des_ateliers_'.$rubrique);

		$roleCourant = $this->Session->read('Auth.User.Role.nom_code');
		$parametre = $this->Parametre->findById(1);

		//Vérification des paramètres
		if (!$idEleve && $roleCourant == 'ELEVE') {
			$idEleve = $this->Session->read('Auth.User.id');
		}
		elseif ($roleCourant == 'ELEVE') {
			$this->Session->setFlash(__('Vous n\'êtes pas autorisé à voir ce profil'), "failure");
			return $this->redirect(array('action' => 'recapitulation', 'rubrique' => $rubrique));
		}

		$eleve = $this->verificationUtilisateur($idEleve);
		if (!$eleve) {
			return $this->redirect(array('action' => 'suivi', 'rubrique' => $rubrique));
		}

		$ateliersBDD = $this->atelierPourEleve($idEleve);

		$this->set(compact('eleve'));
		$this->set('commentaire', $parametre['Parametre']['comAtlEleve']);
		$this->set(compact('ateliersBDD'));
	}

	/**
	 * Configuration du module atelier dans la page de paramétre
	 */
	public function configuration(){
		$this->Session->write('active', 'gestion_des_parametres');
		$this->Session->write('menu.parametres.active', 'Ateliers');

		if ($this->request->is(array('post', 'put'))) {
			$this->Parametre->id = 1;

			// Formatage des données
			$parametres = array("Parametre" => $this->request->data);
			$parametres['Parametre']['id'] = 1;
			$champs = array('desinEleve', 'placeEleve', 'dateAtlEleve', 'comAtlEleve');
			foreach ($champs as $key => $champ) {
				if (!isset($parametres['Parametre'][$champ])) {
					$parametres['Parametre'][$champ] = '0';
				}
			}

			if($this->Parametre->save($parametres)){
				$this->Session->setFlash(__('Configuration sauvegardé.'), "success");
				return $this->redirect(array('action' => 'configuration'));
			}
			$this->Session->setFlash(__('Configuration non sauvegardé.'), "failure");
		}

		$parametre = $this->Parametre->find();
		$this->request->data = $parametre;
	}

	/**
	 * Export les ateliers
	 */
	public function export(){
		if ($this->request->is('post')) {
			//Vérifier l'ordre des dates
			$jourDebut = $this->request->data['Atelier']['jour_debut'];
			$jourFin = $this->request->data['Atelier']['jour_fin'];
			if ($jourDebut <= $jourFin){

				$this->autoRender = false;
				$modelClass = $this->modelClass;
				$this->response->type('Content-Type: text/csv');
				$this->response->download( 'atelier_du_'.$jourDebut.'_au_'.$jourFin.'.csv' );
				$this->response->body( $this->$modelClass->exportCSV($jourDebut, $jourFin) );

				//Supression des ateliers ?
				$message = "";
				if ($this->request->data['Atelier']['suppression'] == "oui") {
					$this->Atelier->deleteAll(array(
						'Atelier.jour BETWEEN ? AND ?' => array(
							$jourDebut, $jourFin
						)
					));
					$message = " et les ateliers sont supprimés";
				}
				$this->Session->setFlash(__('L\'export est réussi'.$message), "success");
				//return $this->redirect(array('action' => 'configuration'));
			}
			else{
				$this->Session->setFlash(__('La date de fin doit être ultérieure à la date de début.'), "failure");
				return;
			}
		}
	}
}
