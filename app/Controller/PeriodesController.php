<?php

/**
 * La classe periode permet la gestion des périodes.
 */
class PeriodesController extends AppController {

	/**
	 * Ajouter une période
	 * @param callback redirection vers la page de configuration
	 */
	public function add(){
		$this->Session->write('active', 'gestion_des_parametres');
		$this->Session->write('menu.parametres.active', 'Périodes');

		if ($this->request->is('post')) {
			$this->Periode->create();
			if($this->Periode->save($this->request->data)){
				$this->Session->setFlash(__('La période a été enrégistré.'), 'success');
				return $this->redirect(array('action' => 'configuration'));
			}
			$this->Session->setFlash(__('La période n\'a pas été enrégistré.'), 'failure');
		}
	}

	/**
	 * Modifier une période
	 *
	 * Cette fonction permet de modifier une période
	 *
	 * @param int $id id de la période à modifier.
	 * @return callback redirige vers la page de configuration en cas de réussite.
	 */
	public function update($id = null) {
		$this->Session->write('active', 'gestion_des_parametres');

		$this->Periode->id = $id;
		if (!$this->Periode->exists()) {
			$this->Session->setFlash(__('Aucune période correspondante n\'a été trouvée'), "failure");
			return $this->redirect(array('action' => 'configuration'));
		}


	    if ($this->request->is(array('post', 'put'))) {
	        $this->Periode->id = $id;

			if($this->Periode->save($this->request->data)){
				$this->Session->setFlash(__('La période a été modifier.'), 'success');
				return $this->redirect(array('action' => 'configuration'));
			}
			$this->Session->setFlash(__('La période n\'a pas été modifier.'), 'failure');
	        
	    }
	    else {
	        $this->request->data                     = $this->Periode->read(null, $id);
			$this->request->data['Periode']['debut'] = substr($this->request->data['Periode']['debut'], 0, -9);
			$this->request->data['Periode']['fin']   = substr($this->request->data['Periode']['fin'], 0, -9);
	    }
	}

	/**
	 * Supprimer une période
	 *
	 * Cette fonction permet de supprimer une période
	 *
	 * @param int $id id de la période à supprimer.
	 * @param bool $forcer la suppression de la période.
	 * @return callback redirige vers la page de configuration en cas de réussite.
	 */
	public function remove($id = null, $forcer = false) {
		$this->Session->write('active', 'gestion_des_parametres');

		//Vérification de l'existance de la période
		$periodeCourante = $this->Periode->findById($id);
		if (count($periodeCourante) == 0) {
			$this->Session->setFlash(__('Aucune période trouvé'), "failure");
			return $this->redirect(array('action' => 'configuration'));
		}

		if ($forcer == false) {
			if (count($periodeCourante['Atelier']) > 0) {
				$ateliers = $this->Periode->Atelier->find (
					'all',
					array ('conditions' => array('Atelier.periode_id' => $id))
				);
				$this->set(compact('ateliers'));
				$this->set(compact('periodeCourante'));
				return;
			}
		}

		if ($this->Periode->delete($id, true)) {
			$this->Session->setFlash(__('La periode %s a été supprimé.', h($periodeCourante['Periode']['nom'])), "success");
			return $this->redirect(array('action' => 'configuration'));
		}

		$this->Session->setFlash(__('La periode %s n\'a pas été supprimé.', h($periodeCourante['Periode']['nom'])), "failure");
		return $this->redirect(array('action' => 'configuration'));
	}

	/**
	 * Configuration des périodes
	 */
	public function configuration(){
		$this->Session->write('active', 'gestion_des_parametres');
		$this->Session->write('menu.parametres.active', 'Périodes');

		$toutesLesPeriodes = $this->Periode->find('all');
		$this->set(compact('toutesLesPeriodes'));
	}

}

