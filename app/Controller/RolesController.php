<?php

class RolesController extends AppController {

    public function add(){
		$this->Session->write('active', 'gestion_des_utilisateurs');

        if ($this->request->is('post')) {
            $this->Role->create();
            if ($this->Role->save($this->request->data)) {
                return $this->redirect(array('controller' => 'users', 'action' => 'gestion'));
            } else {
                $this->Session->setFlash(__('Le rôle n\'a pas été sauvegardé. Merci de réessayer.'));
            }
        }
    }

}
