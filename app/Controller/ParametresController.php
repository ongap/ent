<?php

class ParametresController extends AppController {

	/**
	 * @var string|array Table utilisée(s)
	 */
	public $uses = array('Parametre', 'Horaire', 'User');

	/**
	 * Permet d'afficher le nom du site sur les parties non authentifiés du site
	 */
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('getNomEtalissement');
	}

	/**
	 * Page principale des paramètres.
	 *
	 * Permet de modifier le nom du site et de passer en mode maintenance
	 */
	public function gestion(){
		$this->Session->write('active', 'gestion_des_parametres');
		$this->Session->write('menu.parametres.active', 'Paramètres');

		$parametre = $this->Parametre->find();
		$this->set('maintenance', $parametre['Parametre']['maintenance']);

		$this->request->data = $parametre;
	}

	/**
	 * Basculer le site du mode maintenance ou mode courant et inverssement
	 *
	 * @return callback redirige vers la page de gestion des paramètres en cas de réussite.
	 */
	public function toggleMaintenance() {
		$this->Session->write('active', 'gestion_des_parametres');

		$parametre = $this->Parametre->find();
		$this->Parametre->id = 1;
		if($parametre['Parametre']['maintenance'] == false){
			$this->Parametre->save(array("Parametre" => array("maintenance" => 1)));
			$this->Acl->Aro->query('TRUNCATE cake_sessions;');
			$this->Session->setFlash(__('L\'ENT est maintenant en mode maintenance, tout les utilisateurs ont été déconnecté. Seule le WebMaster peut ce connecter'), "success");
		}
		else{
			$this->Parametre->save(array("Parametre" => array("maintenance" => 0)));
			$this->Session->setFlash(__('L\'ENT est de nouveau accessible à tous'), "success");
		}
		return $this->redirect(array('action' => 'gestion'));
	}

	/**
	 * Gestion des horaires
	 *
	 * Liste les horaires déjà enregistré. Affiche également un formulaire pour 
	 * ajouter un horaire
	 */
	public function schedule() {
		$this->Session->write('active', 'gestion_des_parametres');
		$this->Session->write('menu.parametres.active', 'Horaires');

        if ($this->request->is('post')) {
            $this->Horaire->create();

            if($this->Horaire->save($this->request->data)){
            	$this->Session->setFlash(__('L\'horaire est ajouté.'), "success");
                return $this->redirect(array('action' => 'schedule'));
            }
            $this->Session->setFlash(__('L\'horaire n\'a pas été enrégistré.'), "failure");
        }

		$horaires = $this->Horaire->find('all', array('order' => array('heureDebut')));
		$this->set(compact('horaires'));
	}

	/**
	 * Modification d'un horaire
	 *
	 * @param int $id id de l'horaire à modifier.
	 * @return callback redirige vers la page de gestion des horaires en cas de réussite.
	 */
	public function scheduleUpdate($id = null) {
		$this->Session->write('active', 'gestion_des_parametres');

		$this->Horaire->id = $id;
		if (!$this->Horaire->exists()) {
			$this->Session->setFlash(__('Aucun horaire correspondant n\'a été trouvé'), "failure");
			return $this->redirect(array('action' => 'schedule'));
		}

		if ($this->request->is(array('post', 'put'))) {
			if($this->Horaire->save($this->request->data)){
				$this->Session->setFlash(__('L\'horaire a été mise à jour'), "success");
				return $this->redirect(array('action' => 'schedule'));
			}
			$this->Session->setFlash(__('L\'horaire n\'a pas été enrégistré.'), "failure");
		}

		if (!$this->request->data) {
			$this->request->data = $this->Horaire->read(null, $id);
		}
	}

	/**
	* Supprimer un horaire
	*
	* @param int $id id de l'horaire à supprimer.
	* @return callback redirige vers la page de gestion des horaires en cas de réussite.
	*/
	public function scheduleRemove($id = null) {
		$this->Session->write('active', 'gestion_des_parametres');

		if ($this->Horaire->delete($id)) {
			$this->Session->setFlash(__('L\'horaire est supprimée'), "success");
			return $this->redirect(array('action' => 'schedule'));
		}
		$this->Session->setFlash(__('L\'horaire n\'a pas été supprimée. Merci de réessayer.'), "failure");
	}
	
	/**
	 * Permet d'obtenir le nom du site
	 *
	 * @return string Nom du site
	 */
	public function getNomEtalissement(){
		$parametre = $this->Parametre->find();
		return $parametre['Parametre']['nomEtablissement'];
	}

	/**
	 * Configuration des modules utilisées au lancement du site
	 */
	public function setModulesUsed() {
		if ($this->User->find('count') != 1) {
            return $this->redirect(array('action' => 'gestion'));
        }

		$modules = array (
			'Ateliers' => 'Atelier',
			'Messages' => 'Messagerie',
			'Rencontres' => 'Rencontre parent professeur',
			'Cantines' => 'Cantine',
			'Garderies' => 'Garderie',
			'Rubriques' => 'Rubrique',
		);
		$this->set(compact('modules'));

        if ($this->request->is(array('post', 'put'))) {
			$supprimer = "";
			$exclure = '[';
			foreach ($modules as $nom => $alias) {
				if ($key = array_search($nom, $this->request->data['Parametre']['modules']) === false) {
					$nomController = $nom."Controller.php";
					$nomModel = substr($nom, 0, -1).".php";
					$nomView = $nom;

					if (file_exists('../Controller/'.$nomController)){
						$supprimer .= "app/Controller/".$nomController;
						$supprimer .= " ";
					}

					if (file_exists('../Model/'.$nomModel)){
						$supprimer .= "app/Model/".$nomModel;
						$supprimer .= " ";
					}

					if (is_dir('../View/'.$nomView)){
						$supprimer .= "app/View/".$nomView;
						$supprimer .= " ";
					}

					$exclure .= '"'."app/Controller/".$nomController.'", ';
					$exclure .= '"'."app/Model/".$nomModel.'", ';
					$exclure .= '"'."app/View/".$nomView.'/*", ';
				}
			}
			substr($exclure, 0, -2);
			$exclure .= ']';

			$this->set(compact('supprimer'));
			$this->set(compact('exclure'));

        	$modulesTmp = "";
        	foreach ($this->request->data['Parametre']['modules'] as $key => $value) {
        		$modulesTmp .= $value.",";
        	}
			$this->request->data['Parametre']['modules'] = substr($modulesTmp, 0, -1);
			$this->Parametre->id = 1;

			if($this->Parametre->save($this->request->data)){
				$this->Session->setFlash(__('La liste de module été enrégistré.'), "success");
				$hash_serveur = md5(md5("redstone".date("Y-m-d"))."ongap");
				return $this->redirect(array('controller' => 'users', 'action' => 'reloadPermission', $hash_serveur));
			}
			$this->Session->setFlash(__('La liste de module n\'a pas été enrégistré.'), "failure");
        }
	}
}
