<?php

/**
 * La classe MatieresController permet la gestion des matières.
 */
class MatieresController extends AppController {

	/**
	 * Ajouter une matière
	 *
	 * Cette fonction permet d'ajouter une matière
	 *
	 * @return callback redirige vers la page de gestion des matières en cas de réussite.
	 */
	public function add(){
		$this->Session->write('active', 'gestion_des_matieres');

		if ($this->request->is('post')) {
			$this->Matiere->create();

			if ($this->Matiere->save($this->request->data)){
				$this->Session->setFlash(__('La matière est ajoutée.'), "success");
				return $this->redirect(array('action' => 'gestion'));
			}
			$this->Session->setFlash(__('La matière n\'a pas été enrégistrée.'), "failure");
		}
	}

	/**
	 * Modifier une matière
	 *
	 * Cette fonction permet de modifier une matière
	 *
	 * @param int $id id de la matière à modifier.
	 * @return callback redirige vers la page de gestion des salles en cas de réussite.
	 */
	public function update($id = null) {
		$this->Session->write('active', 'gestion_des_matieres');

		$this->Matiere->id = $id;
		if (!$this->Matiere->exists()) {
			$this->Session->setFlash(__('Aucune matière correspondante n\'a été trouvée'), "failure");
			return $this->redirect(array('action' => 'gestion'));
		}

		if ($this->request->is(array('post', 'put'))) {
			if($this->Matiere->save($this->request->data)){
				$this->Session->setFlash(__('La matière a été mise à jour'), "success");
				return $this->redirect(array('action' => 'gestion'));
			}
			$this->Session->setFlash(__('La matière n\'a pas peut être enrégistrée.'), "failure");
		}

		if (!$this->request->data) {
			$this->request->data = $this->Matiere->read(null, $id);
		}
	}

	/**
	 * Supprimer une matière
	 *
	 * Cette fonction permet de supprimer une matière
	 *
	 * @param int $id id de la matière à supprimer.
	 * @return callback redirige vers la page de gestion des salles en cas de réussite.
	 */
	public function remove($id = null) {
		$this->Session->write('active', 'gestion_des_matieres');

		if ($this->Matiere->delete($id)) {
			$this->Session->setFlash(__('La matière est supprimée'), "success");
			return $this->redirect(array('action' => 'gestion'));
		}
		$this->Session->setFlash(__('La matière n\'a pas été supprimée. Merci de réessayer.'), "failure");
	}

	/**
	* Afficher les matières
	*
	* Cette fonction permet d'afficher les matières définie par un administrateur.
	*/
	public function gestion(){
		$this->Session->write('active', 'gestion_des_matieres');

		$tousLesMatieres = $this->Matiere->find('all');
		$this->set(compact('tousLesMatieres'));
	}
}