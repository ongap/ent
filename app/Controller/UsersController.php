<?php

/**
 * La classe user permet la gestion des utilisateurs.
 */
class UsersController extends AppController {

	/**
	 * @var string|array Table(s) utilisée(s)
	 */
	public $uses = array('User', 'Division', 'Parametre');

	/**
	 * @var string|array Composant(s) utilisée(s)
	 */
	public $components = array('RequestHandler');

	/**
	 * Génération des Objet Requête d’Accès
	 */
	private function generateAro() {
		//Création des ACO
		$this->Acl->Aro->create(array( 
			'alias' => 'Tout le monde'
			));
		$everybody = $this->Acl->Aro->save();

		$this->Acl->Aro->create(array( 
			'model' => 'Role', 
			'foreign_key' => 1, 
			'parent_id' => $everybody['Aro']['id'],
			'alias' => 'WebMaster'
			));
		$webMaster = $this->Acl->Aro->save();

		$this->Acl->Aro->create(array( 
			'model' => 'Role', 
			'foreign_key' => 2, 
			'parent_id' => $webMaster['Aro']['id'], 
			'alias' => 'Administrateur'
			));
		$this->Acl->Aro->save();

		$this->Acl->Aro->create(array( 
			'model' => 'Role', 
			'foreign_key' => 3, 
			'parent_id' => $everybody['Aro']['id'], 
			'alias' => 'Élève'
			));
		$this->Acl->Aro->save();

		$this->Acl->Aro->create(array( 
			'model' => 'Role', 
			'foreign_key' => 4, 
			'parent_id' => $everybody['Aro']['id'], 
			'alias' => 'Professeur'
			));
		$professeur = $this->Acl->Aro->save();

		$this->Acl->Aro->create(array( 
			'model' => 'Role', 
			'foreign_key' => 5, 
			'parent_id' => $professeur['Aro']['id'], 
			'alias' => 'Vie scolaire'
			));
		$this->Acl->Aro->save();

		$this->Acl->Aro->create(array( 
			'model' => 'Role', 
			'foreign_key' => 6, 
			'parent_id' => $professeur['Aro']['id'], 
			'alias' => 'Vacataire'
			));
		$this->Acl->Aro->save();
	}

	/**
	 * Génération des Objet Contrôle d’Accès
	 */
	private function generateAco() {
		//Création des ARO
		$this->Acl->Aco->create(array('parent_id' => null, 'alias' => 'controllers'));
		$controllerRoot = $this->Acl->Aco->save();

		//Actualité
		$this->Acl->Aco->create(array(
			'parent_id' => $controllerRoot['Aco']['id'],
			'alias' => 'Actualites'));
		$controllerActualite = $this->Acl->Aco->save();

		$actionsActualite = array('add', 'update', 'remove', 'gestion', 'suspendre', 'view');
		foreach ($actionsActualite as $action) {
			$this->Acl->Aco->create(array(
				'parent_id' => $controllerActualite['Aco']['id'],
				'alias' => $action));
			$this->Acl->Aco->save();
		}

		//Atelier
		$this->Acl->Aco->create(array(
			'parent_id' => $controllerRoot['Aco']['id'],
			'alias' => 'Ateliers'));
		$controllerAtelier = $this->Acl->Aco->save();

		$actionsAtelier = array('add', 'update', 'validation', 'creditation', 'crediter', 'impression', 'remove', 'gestion', 'inscription', 'desinscription', 'suspendre', 'forcer', 'forcerEleve', 'forcerClasse', 'forcerClasseAtelier', 'suivi', 'suiviClasse', 'suiviAnimateur', 'recapitulation', 'configuration', 'export', 'patch');
		foreach ($actionsAtelier as $action) {
			$this->Acl->Aco->create(array(
				'parent_id' => $controllerAtelier['Aco']['id'],
				'alias' => $action));
			$this->Acl->Aco->save();
		}

		$this->Acl->Aco->create(array(
			'parent_id' => $controllerRoot['Aco']['id'],
			'alias' => 'Divisions'));
		$controllerDivision = $this->Acl->Aco->save();

		$actionsDivision = array('add', 'update', 'remove', 'gestion');
		foreach ($actionsDivision as $action) {
			$this->Acl->Aco->create(array(
				'parent_id' => $controllerDivision['Aco']['id'],
				'alias' => $action));
			$this->Acl->Aco->save();
		}

		$this->Acl->Aco->create(array(
			'parent_id' => $controllerRoot['Aco']['id'],
			'alias' => 'Matieres'));
		$controllerMatiere = $this->Acl->Aco->save();

		$actionsMatiere = array('add', 'update', 'remove', 'gestion');
		foreach ($actionsMatiere as $action) {
			$this->Acl->Aco->create(array(
				'parent_id' => $controllerMatiere['Aco']['id'],
				'alias' => $action));
			$this->Acl->Aco->save();
		}

		$this->Acl->Aco->create(array(
			'parent_id' => $controllerRoot['Aco']['id'],
			'alias' => 'Parametres'));
		$controllerParametre = $this->Acl->Aco->save();

		$actionsParametre = array('gestion', 'toggleMaintenance','schedule', 'scheduleUpdate', 'scheduleRemove', 'getNomEtalissement', 'setModulesUsed', 'genererPermission');
		foreach ($actionsParametre as $action) {
			$this->Acl->Aco->create(array(
				'parent_id' => $controllerParametre['Aco']['id'],
				'alias' => $action));
			$this->Acl->Aco->save();
		}

		$this->Acl->Aco->create(array(
			'parent_id' => $controllerRoot['Aco']['id'],
			'alias' => 'Periodes'));
		$controllerPeriode = $this->Acl->Aco->save();

		$actionsPeriode = array('add', 'update', 'remove', 'configuration');
		foreach ($actionsPeriode as $action) {
			$this->Acl->Aco->create(array(
				'parent_id' => $controllerPeriode['Aco']['id'],
				'alias' => $action));
			$this->Acl->Aco->save();
		}

		$this->Acl->Aco->create(array(
			'parent_id' => $controllerRoot['Aco']['id'],
			'alias' => 'Roles'));
		$controllerRole = $this->Acl->Aco->save();

		$actionsRole = array('add');
		foreach ($actionsRole as $action) {
			$this->Acl->Aco->create(array(
				'parent_id' => $controllerRole['Aco']['id'],
				'alias' => $action));
			$this->Acl->Aco->save();
		}

		//Controller Rubrique
		$this->Acl->Aco->create(array(
			'parent_id' => $controllerRoot['Aco']['id'],
			'alias' => 'Rubriques'));
		$controllerRubrique = $this->Acl->Aco->save();

		$actionsRubrique = array('getAllRubriques', 'configuration', 'ajouter', 'modifier', 'supprimer');
		foreach ($actionsRubrique as $action) {
			$this->Acl->Aco->create(array(
				'parent_id' => $controllerRubrique['Aco']['id'],
				'alias' => $action));
			$this->Acl->Aco->save();
		}


		$this->Acl->Aco->create(array(
			'parent_id' => $controllerRoot['Aco']['id'],
			'alias' => 'Salles'));
		$controllerSalle = $this->Acl->Aco->save();

		$actionsSalle = array('add', 'update', 'remove', 'gestion');
		foreach ($actionsSalle as $action) {
			$this->Acl->Aco->create(array(
				'parent_id' => $controllerSalle['Aco']['id'],
				'alias' => $action));
			$this->Acl->Aco->save();
		}

	    $this->Acl->Aco->create(array(
	        'parent_id' => $controllerRoot['Aco']['id'],
	        'alias' => 'Users'));
	    $controllerUser = $this->Acl->Aco->save();

	    $actionsUser = array('login', 'logout', 'gestion', 'add', 'update', 'remove', 'firstConnexion', 'firstStart', 'import', 'export', 'reloadPermission', 'configuration', 'changeClasse', 'supprimeCompte', 'phone');
	    foreach ($actionsUser as $action) {
	        $this->Acl->Aco->create(array(
	            'parent_id' => $controllerUser['Aco']['id'],
	            'alias' => $action));
	        $this->Acl->Aco->save();
	    }

	}

	//@TODO générer en fonction des modules choisi
	/**
	 * génération des permissions
	 *
	 * Cette fonction permet de générer les permissions (ACL)
	 * de l'ENT en fonction des modules choisie
	 *
	 * @return callback redirige vers la page d'actualité en cas de réussite.
	 */
	private function genererPermission($short = false) {
	    $this->Acl->Aro->query('TRUNCATE aros;');
	    $this->Acl->Aro->query('TRUNCATE acos;');
	    $this->Acl->Aro->query('TRUNCATE aros_acos;');

		$this->generateAro();
		$this->generateAco();

		//Positionner les permissions
		// Autorise l'accès à tout le monde pour la connexion, deco et changement de mot de passe
		$this->Acl->deny('Tout le monde', 'controllers');
		$this->Acl->allow('Tout le monde', 'controllers/Actualites/view');
		$this->Acl->allow('Tout le monde', 'controllers/Users/login');
		$this->Acl->allow('Tout le monde', 'controllers/Users/logout');
		$this->Acl->allow('Tout le monde', 'controllers/Users/firstStart');
		$this->Acl->allow('Tout le monde', 'controllers/Users/firstConnexion');
		$this->Acl->allow('Tout le monde', 'controllers/Rubriques/getAllRubriques');

		// Autorise l'accès à tout pour le WebMaster et les administrateurs (par héritage)
		$this->Acl->allow('WebMaster', 'controllers');

		// On ne génére pas tout les permissions
		if ($short) {
			return 0;
		}

		// Autorise les élèves
		$this->Acl->allow('Élève', 'controllers/Users/phone');
		$this->Acl->allow('Élève', 'controllers/Ateliers/inscription');
		$this->Acl->allow('Élève', 'controllers/Ateliers/desinscription');
		$this->Acl->allow('Élève', 'controllers/Ateliers/recapitulation');

		// Autorise les professeurs
		$this->Acl->allow('Professeur', 'controllers/Ateliers');
		$this->Acl->deny('Professeur', 'controllers/Ateliers/desinscription');
		$this->Acl->deny('Professeur', 'controllers/Ateliers/inscription');
		$this->Acl->deny('Professeur', 'controllers/Ateliers/recapitulation');

		// Autorise la vie scolaire
		// Aucune autorisation particuliére

		// Autorise les vacataires
		$this->Acl->deny('Vacataire', 'controllers/Ateliers/add');
		$this->Acl->deny('Vacataire', 'controllers/Ateliers/forcer');
		$this->Acl->deny('Vacataire', 'controllers/Ateliers/forcerEleve');
		$this->Acl->deny('Vacataire', 'controllers/Ateliers/forcerClasse');
		$this->Acl->deny('Vacataire', 'controllers/Ateliers/forcerClasseAtelier');
		$this->Acl->deny('Vacataire', 'controllers/Ateliers/update');
		$this->Acl->deny('Vacataire', 'controllers/Ateliers/remove');

		//Récuperer la liste des modules
		$parametre = $this->Parametre->find('first');
		$modules   = explode(',', $parametre['Parametre']['modules']);

		//Ateliers
		//@TODO Comment ce déroule l'application sans le module atelier

		//Rubriques dépend du module Atelier
		if (!in_array("Rubriques", $modules)) {
			$this->Acl->deny('Tout le monde', 'controllers/Rubriques/ajouter');
			$this->Acl->deny('Tout le monde', 'controllers/Rubriques/supprimer');
			$this->Acl->deny('WebMaster', 'controllers/Rubriques/ajouter');
			$this->Acl->deny('WebMaster', 'controllers/Rubriques/supprimer');
		}

		//Messages, pas encore réalisé...
		//@TODO aprés l'intégration de la messagerie
	}

	/**
	 * Autoriser des pages
	 *
	 * Cette fonction permet aux pages login, logout et firstStart de 
	 * s'afficher avant les mécaniques ACL
	*/
	public function beforeFilter() {
		parent::beforeFilter();
		$parametre = $this->Parametre->find('first');
		if($parametre['Parametre']['maintenance']){
			$this->Auth->allow('login', 'logout', 'firstStart', 'reloadPermission');
		}
		else{
			$this->Auth->allow('login', 'logout', 'firstStart');
		}
	}

	/**
	 * Authentification
	 *
	 * Cette fonction permet aux utilisateurs de 
	 * s'authentifier sur l'ENT
	 *
	 * @return callback redirige vers la page d'actualité en cas de réussite.
	 */
	public function login(){

		if ($this->User->find('count') == 0) {
			return $this->redirect(array('action' => 'firstStart'));
		}


		if (!!($this->request->data)){
			if($this->Auth->login()){

		        $parametre = $this->Parametre->find('first');

				if($parametre['Parametre']['maintenance'] == 1 && $this->Session->read('Auth.User.Role.nom') != 'WebMaster'){
					$this->Auth->logout();
					$this->Session->setFlash(__('Une maintenance est en cours, merci de patienter quelques minutes.'), "failure");
					return $this->redirect(array('action' => 'login'));
				}

				if ($this->Session->read('Auth.User.nbConnexion') == '0') {
					return $this->redirect(array('action' => 'firstConnexion'));
				}

				if ($this->Session->read('Auth.User.Role.nom') == 'WebMaster') {
					if ($parametre['Parametre']['modules'] == '') {
						return $this->redirect(array('controller' => 'parametres', 'action' => 'setModulesUsed'));
					}
				}

				$nbConnexion = $this->Session->read('Auth.User.nbConnexion');
				$nbConnexion++;

				$parametres = $this->Parametre->find('first');
				$this->Session->write('Parametre.needPhone', $parametres['Parametre']['needPhone']);

				$this->User->save(array('User' => array(
					'id' => $this->Session->read('Auth.User.id'),
					'nbConnexion' => $nbConnexion
				)));



				return $this->redirect('/');
			}
			$this->Session->setFlash(__('Identifiant ou mot de passe incorrect.'), "failure");
		}
	}

	/**
	 * Déconnexion
	 *
	 * Cette fonction permet aux utilisateurs de 
	 * se déconnecter de l'ENT
	 *
	 * @return callback redirige vers la page de connexion.
	 */
	public function logout(){
		$this->Auth->logout();
	    return $this->redirect(array('action' => 'login'));
	}

	/**
	 * Gestion des utilisateurs
	 */
	public function gestion(){
	    $this->Session->write('active', 'gestion_des_utilisateurs');

		$tousLesUsers = $this->User->find('all', array('order' => array('User.nom ASC')));
		$this->set(compact('tousLesUsers'));

	    $utilisateurs_final = $this->Session->check('liste.user');
	    $this->set(compact('utilisateurs_final'));
	}

	/**
	 * Ajout d'un utilisateur
	 *
	 * @return callback redirige vers la page de gestion en cas de réussite.
	 */
	public function add(){
		$this->Session->write('active', 'gestion_des_utilisateurs');
	    $this->set('divisions', $this->User->Division->find('list', array('fields' => array('Division.id', 'Division.nom'))));
	    $roles = $this->User->Role->find('list', array('fields' => array('Role.id', 'Role.nom'), 'conditions' => array('Role.nom_code !=' => 'WEBMASTER')));
	    if ($this->Session->read("Auth.User.Role.nom_code") != "WEBMASTER") {
	     foreach ($roles as $key => $role) {
	      if ($role == "Administrateur") { unset($roles[$key]); }
	     }
	    }
	    $this->set(compact('roles'));

	    if ($this->request->is('post')) {
	        $this->User->create();
	        if ($this->User->save($this->request->data)) {
	            $this->Session->setFlash(__('L\'utilisateur est créé'), "success");
	            return $this->redirect(array('action' => 'gestion'));
	        } else {
	            $this->Session->setFlash(__('L\'utilisateur n\'a pas été sauvegardé. Merci de réessayer.'), "failure");
	        }
	    }
	}

	/**
	 * Modication d'un utilisateur
	 *
	 * Cette fonction permet de modifier les informations d'un 
	 * utilisateur. Quand le mot de passe est changé, 
	 *
	 * @param int $id id de l'utilisateur à modifier.
	 * @return callback redirige vers la page de gestion en cas de réussite.
	 */
	public function update($id = null){
		$this->Session->write('active', 'gestion_des_utilisateurs');
		$this->set('divisions', $this->User->Division->find('list', array('fields' => array('Division.id', 'Division.nom'))));
	    $this->set('roles', $this->User->Role->find('list', array('fields' => array('Role.id', 'Role.nom'), 'conditions' => array('Role.nom_code !=' => 'WEBMASTER'))));

	    $this->User->id = $id;
	    if (!$this->User->exists()) {
	        $this->Session->setFlash(__('L\'utilisateur n\'existe pas'), "failure");
	    }

	    if ($this->request->is('post') || $this->request->is('put')) {

	        if (!!($this->request->data['User']['pass1'])){
	            if ($this->request->data['User']['pass1'] == $this->request->data['User']['pass2']) {
	                $this->request->data['User']['password'] = $this->request->data['User']['pass1'];
	                $this->request->data['User']['nbConnexion'] = 0;
	            }
	            else{
	                $this->Session->setFlash(__('Les mots de passes ne sont pas identiques'), "failure");
					return 0;
	            }
	        }
	        if ($this->User->save($this->request->data)) {
	            $this->Session->setFlash(__('L\'utilisateur est modifié'), "success");
	            return $this->redirect(array('action' => 'gestion'));
	        } else {
	            $this->Session->setFlash(__('L\'utilisateur n\'a pas été sauvegardé. Merci de réessayer.'), "failure");
	        }
	    } else {
	        $this->request->data = $this->User->read(null, $id);
	    }
	}

	/**
	 * Supprimer un utilisateur
	 *
	 * @param int $id id de l'utilisateur à supprimer.
	 * @param bool $force sauter la vérifiction d'atelier ou actualité lié à l'utilisateur à supprimer.
	 * @return callback redirige vers la page de gestion en cas de réussite.
	 */
	public function remove($id = null, $force = null){
		$this->Session->write('active', 'gestion_des_utilisateurs');

		$this->User->id = $id;
		if (!$this->User->exists()) {
			$this->Session->setFlash(__('L\'utilisateur n\'existe pas'), "failure");
			return $this->redirect(array('action' => 'gestion'));
		}

		if ($force == null) {
			//Vérifier si des ateliers ou des atualités sont liés a cette utilisateur
			$atelierFound = $this->User->Atelier->findAllByUserId($id);
			$AtualiteFound = $this->User->Actualite->findAllByUserId($id);

			if (count($atelierFound) > 0 || count($AtualiteFound) > 0) {
				$this->set(compact('id'));
				$this->set(compact('atelierFound'));
				$this->set(compact('AtualiteFound'));
				return;
			}
		}

		if ($this->User->delete()) {
			$this->Session->setFlash(__('L\'utilisateur est supprimé'), "success");
			return $this->redirect(array('action' => 'gestion'));
		}
		$this->Session->setFlash(__('L\'utilisateur n\'a pas été supprimé'), "failure");
		return $this->redirect(array('action' => 'gestion'));
	}

	/**
	 * Première connexion de l'utilisateur
	 *
	 * @return callback redirige vers la page de gestion en cas de réussite.
	*/
	public function firstConnexion(){
		if ($this->request->is('post') || $this->request->is('put')){

		    $this->request->data['User']['nbConnexion'] = 1;
		    if ($this->User->save($this->request->data)){
		        return $this->redirect(array('action' => 'logout'));
		    } else {
		        $this->Session->setFlash(__('Erreur, le mot de passe n\'est pas enregistré'), "failure");
		    }
		} else {
			$this->request->data = $this->User->read(null, $this->Session->read('Auth.User.id'));
			unset($this->request->data['User']['password']);
		}
	}

	/**
	 * Première connexion de au site
	 *
	 * @return callback redirige vers la page de gestion en cas de réussite.
	*/
	public function firstStart(){
		if ($this->User->find('count') > 0) {
		    return $this->redirect(array('action' => 'logout'));
		}

		if ($this->request->is('post') || $this->request->is('put')){

		    $this->request->data['User']['nbConnexion'] = 1;
		    $this->request->data['User']['id'] = 1;
		    $this->request->data['User']['role_id'] = 1;
		    $this->request->data['User']['division_id'] = 1;
		    if ($this->User->save($this->request->data)){
		        $this->genererPermission(true);
		        $this->Session->setFlash(__('WebMaster créer avec succès et génération des permissions réussite'), "success");
		        return $this->redirect(array('controller' => 'parametres', 'action' => 'setModulesUsed'));
		    } else {
		        $this->Session->setFlash(__('Erreur, l\'utilisateur WebMaster n\'est pas enregistrée'), "failure");
		    }
		}
	}

	/**
	 * Importer des utilisateurs
	 *
	 * @return callback redirige vers la page de gestion ou d'export en cas de réussite.
	 */
	public function import(){
	    $this->Session->write('active', 'gestion_des_utilisateurs');
	    $divisions = $this->User->Division->find('list', array('fields' => array('Division.id', 'Division.nom')));
	    $roles = $this->User->Role->find('list', array('fields' => array('Role.id', 'Role.nom_code'), 'conditions' => array('Role.nom_code !=' => 'WEBMASTER')));

	    if ($this->request->is('post')){

	        $tmpname = $this->data['importUser']['csv']['tmp_name'];
	        if ($tmpname == '') {
	            $this->Session->setFlash('Pas de fichier importé', "failure");
	            return;
	        }

	        $genereLogin = (isset($this->data['importUser']['genereLogin'])) ? $this->data['importUser']['genereLogin'] : 0 ;
	        $genereMpd = (isset($this->data['importUser']['genereMotdepasse'])) ? $this->data['importUser']['genereMotdepasse'] : 0 ;

	        $delim = ';'; $enc = '"'; $line = "\n";
	        $x = 0;
	        $utilisateurs_final = array();
	        foreach( str_getcsv ( file_get_contents( $tmpname ), $line ) as $row ){
	            $csv[] = str_getcsv( $row, $delim, $enc );

	            if ($genereLogin == '0' && $genereMpd == '0') {
	                $compte = $csv[$x][3];
	                $password = $csv[$x][4];
	                $nom_divi = $csv[$x][5];
	                $nom_role = $csv[$x][6];
	            }
	            elseif ($genereLogin == '0' && $genereMpd == '1') {
	                $compte = $csv[$x][3];
	                $password = $csv[$x][4][0].$csv[$x][4][1].$csv[$x][4][3].$csv[$x][4][4].$csv[$x][4][8].$csv[$x][4][9];
	                $nom_divi = $csv[$x][5];
	                $nom_role = $csv[$x][6];
	            }
	            elseif ($genereLogin == '1' && $genereMpd == '0') {
	                $compte = trim(substr($csv[$x][1], 0, 7).substr($csv[$x][2], 0, 1), " ");
	                $compte = str_replace(' ','',$compte);
	                $password = $csv[$x][3];
	                $nom_divi = $csv[$x][4];
	                $nom_role = $csv[$x][5];
	            }
	            else {
	                $compte = trim(substr($csv[$x][1], 0, 7).substr($csv[$x][2], 0, 1), " ");
	                $compte = str_replace(' ','',$compte);
	                $password = $csv[$x][3][0].$csv[$x][3][1].$csv[$x][3][3].$csv[$x][3][4].$csv[$x][3][8].$csv[$x][3][9];
	                $nom_divi = $csv[$x][4];
	                $nom_role = $csv[$x][5];
	            }

	            if ($nom_divi == "") {
	                $nom_divi = $divisions[1];
	            }

	            $id_divi = array_search($nom_divi, $divisions);
	            $id_role = array_search($nom_role, $roles);

	            if ($id_role === false) {
	                $this->Session->setFlash(__('Le rôle %s n\'existe pas.', h($nom_role)), "failure");
	                return;
	            }

	            if ($id_divi === false) {
	                //Création de la classe manquante.
	                $this->Division->create();
	                if(!$this->Division->save(array('Division' => array('nom' => $nom_divi)))){
	                    $this->Session->setFlash(__('Impossible de créer la classe %s.', h($nom_divi)), "failure");
	                    return;
	                }

	                //Rechargement du tableau
	                $divisions = $this->User->Division->find('list', array('fields' => array('Division.id', 'Division.nom')));
	                $id_divi = array_search($nom_divi, $divisions);
	            }

	            //vérifier que le pseudo n'est pas déjà pris
	            $username = $this->User->findByUsername($compte);

	            while(count($username) > 0){
	                $compte .= date('s');
	                $username = $this->User->findByUsername($compte);
	            }

	            //Enregistrement pour un éventuelle export
	            $utilisateurs_final[$x] = array(
	             'civilite' => $csv[$x][0],
	             'password' => $password,
	             'nom' => $csv[$x][1],
	             'prenom' => $csv[$x][2],
	             'username' => $compte,
	             'role_id' => $id_role,
	             'division_id' => $id_divi
	            );

	            $this->User->create();
	            if(!$this->User->save(array('User' => $utilisateurs_final[$x]))){
					$messageErreur = "";
					$champsErrors = $this->User->invalidFields();
					foreach ($champsErrors as $key => $value) {
						$messageErreur .= $this->User->validationErrors[$key][0].",";
					}
					$messageErreur  = substr($messageErreur, 0, -1);

					$this->Session->setFlash(__('Impossible d\'ajouter l\'utilisateur %s (%s), vérifier le format du fichier csv', h($utilisateurs_final[$x]['nom']), $messageErreur), "failure");

					return;
	            }
	            $x++;
	        }

	        if ($genereLogin == '1' || $genereMpd == '1') {
	            $this->Session->write('liste.user', $utilisateurs_final);
	            $this->Session->setFlash(__('Les utilisateurs sont correctement importés, vous pouvez récuperer la liste des comtptes en cliquant sur exporter'), "success");
	            return $this->redirect(array('action' => 'gestion'));
	        }
	        else{
	            $this->Session->setFlash(__('Les utilisateurs sont correctement importés'), "success");
	            return $this->redirect(array('action' => 'gestion'));
	        }
	    }
	}

	/**
	 * Export les utilisateurs
	*/
	public function export(){
		$utilisateurs_final = $this->Session->read('liste.user');
		$this->Session->delete('liste.user');
		$this->response->download("export.csv");
		$this->set(compact('utilisateurs_final'));
		$this->layout = 'ajax';
	}

	/**
	 * Recharche les permissions pendant une maintenance
	 *
	 * @param string $hash Tocken pour que seule un utilisateur le connaissant puisse lancer la fonction.
	 * @return affiche un retour.
	 */
	public function reloadPermission($hash=''){
		$parametre = $this->Parametre->find('first');

		$hash_serveur = md5(md5("redstone".date("Y-m-d"))."ongap");

		$msg = "KO";
		if($parametre['Parametre']['maintenance'] == 1 && $hash == $hash_serveur){
			$this->genererPermission();
			$msg = "OK";
		}

		if($this->request->accepts('application/json')){
			$this->set(compact('msg'));
			$this->set('_serialize', array('msg'));			
		} else {
			$this->Session->setFlash(__('Permissions rechargées en fonction des modules choisis'), "success");
			return $this->redirect('/');
		}

	}

	/**
	 * Configuration du module
	*/
	public function configuration()	{
		$this->Session->write('active', 'gestion_des_parametres');

		if ($this->request->is(array('put'))) {
			$this->Parametre->id = 1;

			// Formatage des données
			$parametres = array("Parametre" => $this->request->data);
			$parametres['Parametre']['id'] = 1;
			$champs = array('needPhone');
			foreach ($champs as $key => $champ) {
				if (!isset($parametres['Parametre'][$champ])) {
					$parametres['Parametre'][$champ] = '0';
				}
			}

			if($this->Parametre->save($parametres)){
				$this->Session->setFlash(__('Configuration sauvegardé.'), "success");
				return $this->redirect(array('action' => 'configuration'));
			}
			$this->Session->setFlash(__('Configuration non sauvegardé.'), "failure");
		}

		$parametre = $this->Parametre->find('first');
		$this->request->data = $parametre;

		$this->Session->write('menu.parametres.active', 'Utilisateurs');
	}

	public function phone()
	{
		$this->Session->write('active', 'profil');

		if ($this->request->is(array('put'))) {
			$this->request->data['User']['id'] = $this->Session->read('Auth.User.id');

			if($this->User->save($this->request->data)){
				$this->Session->setFlash(__('Profil mis à jour.'), "success");
				$this->Session->write('Auth.User.phone', $this->request->data['User']['phone']);
				return $this->redirect('/');
			}
			$this->Session->setFlash(__('le profil n\'est pas enregistré, champ(s) incorrect(s).'), "failure");
		}
	}

	/**
	 * Changement de classe pour la rentrée
	*/
	public function changeClasse(){
		$this->Session->write('active', 'gestion_des_parametres');

		$retourBDD = $this->Division->find('list', array(
			'fields' => array('Division.nom'),
			'order' => array('Division.nom DESC'),
		));

		$this->set(compact('retourBDD'));

		$classes = array_merge(array(0 => "Ne rien faire"), $retourBDD);
		$classes[] = "Vider la classe";

		if ($this->request->is('post')) {
			foreach ($this->request->data['Division'] as $key => $value) {
				//On n'ajoute pas les classes non affecté
				if ($classes[$value] != "Ne rien faire") {
					$resultat[$retourBDD[$key]] = $classes[$value];
				}
			}

			//Construire une chaine
			//Recherche des débuts de chaine
			$debut = array();
			$chaine = array();
			foreach ($resultat as $key => $value) {
				if(array_search($key, $resultat) === false){
					$debut[] = $key;
				}
			}

			foreach ($debut as $key => $value) {
				$continu = true;
				$chaine[$key][] = $value;
				$prochaineCle = $resultat[$value];
				while ($continu) {
					$chaine[$key][] = $prochaineCle;
					$prochaineCle = (isset($resultat[$prochaineCle])) ? $resultat[$prochaineCle] : 'Vider la classe' ;
					if ($prochaineCle == 'Vider la classe') {
						$continu = false;
					}
				}
			}

			//On vide les classe marqué comme tels
			foreach ($resultat as $key => $value) {
				if ($value == "Vider la classe") {
					$this->User->deleteAll(array('Division.nom' => $key));
				}
			}

			//On inverse les classes
			foreach ($chaine as $key => $value) {
				$classesAinverser=array_reverse($value);
				for ($i=0; $i < count($classesAinverser)-1; $i++) { 
					$cleNouvelle = array_search($classesAinverser[$i], $retourBDD);
					$cleAncienne = array_search($classesAinverser[$i+1], $retourBDD);
					$this->User->UpdateAll(
						array('User.division_id' => $cleNouvelle),
						array('User.division_id' => $cleAncienne)
					);
					$this->User->deleteAll(array('Division.nom' => $classesAinverser[$i+1]));
				} 
			}

			$this->Session->setFlash(__('Les classes ont été transférés'), "success");
		}
		else{
			$prediction = array();
			$index = 1;
			foreach ($retourBDD as $key => $value) {
				$prediction[$key] = (isset($classes[$index+1])) ? ($index+1) : count($classes) ;
				$index++;
			}
			$this->request->data = array('Division' => $prediction);
		}
	}


	/**
	 * Permet de supprimer tous les utilisateurs avec le role $role
	 * @param string $role le role concerné.
	 * @return callback redirige vers la page des paramètres.
	*/
	public function supprimeCompte($role) {

		//Vérifier si le rôle existe
		$roleCourant = $this->User->Role->findByNomCode($role);

		if (count($roleCourant) == 0) {
			$this->Session->setFlash(__('Le rôle n\'existe pas'), "failure");
			return $this->redirect(array('action' => 'configuration'));
		}

		if ($this->User->deleteAll(array('Role.nom_code' => $role))) {
			$this->Session->setFlash(__('Les utilisateurs sont supprimé'), "success");
			return $this->redirect(array('action' => 'configuration'));
		}
		$this->Session->setFlash(__('Les utilisateurs n\'ont pas été supprimé'), "failure");
		return $this->redirect(array('action' => 'configuration'));
	}

}
