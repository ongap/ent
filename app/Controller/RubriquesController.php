<?php

/**
 * La classe rubrique permet la gestion des rubriques. C'est un module optionnel
 */
class RubriquesController extends AppController {

	/**
	 * Retourne une liste compléte des rubriques. Permet de générer le menu
	 * @return array Tableau des enregistrements rubrique en base
	 */
	public function getAllRubriques(){
		return $this->Rubrique->find('all', array('order' => array('Rubrique.position', 'Rubrique.nom')));
	}

	/**
	 * Configuration du module rubrique dans la page de paramétre
	 */
	public function configuration(){
		$this->Session->write('active', 'gestion_des_parametres');
		$this->Session->write('menu.parametres.active', 'Rubriques');

		$rubriquesBDD = $this->Rubrique->find('all', array('order' => array('Rubrique.position', 'Rubrique.nom')));

		//Rotation du tableau (+90°)
		$rubriques = array(
			'id' 						=> array(),
			'position' 					=> array(),
			'nom' 						=> array(),
			'label_gestion' 			=> array(),
			'label_gestion_inscription' => array(),
			'label_suivi' 				=> array(),
			'label_inscription' 		=> array(),
			'label_recapitulation' 		=> array(),
		);
		foreach ($rubriquesBDD as $key => $rubrique) {
			$rubriques['id'][] 							= $rubrique['Rubrique']['id'];
			$rubriques['position'][] 					= $rubrique['Rubrique']['position'];
			$rubriques['nom'][] 						= $rubrique['Rubrique']['nom'];
			$rubriques['label_gestion'][] 				= $rubrique['Rubrique']['label_gestion'];
			$rubriques['label_gestion_inscription'][] 	= $rubrique['Rubrique']['label_gestion_inscription'];
			$rubriques['label_suivi'][] 				= $rubrique['Rubrique']['label_suivi'];
			$rubriques['label_inscription'][] 			= $rubrique['Rubrique']['label_inscription'];
			$rubriques['label_recapitulation'][] 		= $rubrique['Rubrique']['label_recapitulation'];
		}

		$btnAjouter = $this->Acl->check('WebMaster', 'controllers/Rubriques/ajouter');

		$this->set(compact('btnAjouter'));
		$this->set(compact('rubriques'));
	}

	/**
	 * Ajouter une rubrique
	 * @param callback redirection vers la page de gestion
	 */
	public function ajouter() {
		$this->Session->write('active', 'gestion_des_parametres');

		if ($this->request->is('post')) {
			$this->Rubrique->create();
			
			$search  = array(' ', 'é', 'è', 'à', 'ç', 'â', 'ê', 'ŷ', 'û', 'î', 'ô');
			$replace = array('_', 'e', 'e', 'a', 'c', 'a', 'e', 'y', 'u', 'i', 'o');

			$this->request->data['Rubrique']['nom_canonique'] = str_replace($search, $replace, strtolower($this->request->data['Rubrique']['nom']));

			if ($this->Rubrique->save($this->request->data)) {
				$this->Session->setFlash(__('La rubrique est sauvegardé'), "success");
				return $this->redirect(array('action' => 'configuration'));
			}
			$this->Session->setFlash(__('La rubrique n\'a pas été sauvegardé. Merci de réessayer.'), "failure");
		}
	}

	/**
	 * Modifier une rubrique
	 *
	 * Cette fonction permet de modifier une rubrique
	 *
	 * @param int $id id de la rubrique à modifier.
	 * @return callback redirige vers la page de configuration en cas de réussite.
	 */
	public function modifier($id = null) {
		$this->Session->write('active', 'gestion_des_parametres');

		$this->Rubrique->id = $id;
		if (!$this->Rubrique->exists()) {
			$this->Session->setFlash(__('Aucune rubrique correspondante n\'a été trouvée'), "failure");
			return $this->redirect(array('action' => 'configuration'));
		}

		if ($this->request->is(array('post', 'put'))) {
			$search  = array(' ', 'é', 'è', 'à', 'ç', 'â', 'ê', 'ŷ', 'û', 'î', 'ô');
			$replace = array('_', 'e', 'e', 'a', 'c', 'a', 'e', 'y', 'u', 'i', 'o');

			$this->request->data['Rubrique']['nom_canonique'] = str_replace($search, $replace, strtolower($this->request->data['Rubrique']['nom']));

			if ($this->Rubrique->save($this->request->data)) {
				$this->Session->setFlash(__('La rubrique a été mise à jour'), "success");
				return $this->redirect(array('action' => 'configuration'));
			}
			$this->Session->setFlash(__('La modification de la rubrique n\'a pas été sauvegardée. Merci de réessayer.'), "failure");
		}
		else{
			$this->request->data = $this->Rubrique->read(null, $id);
		}
	}

	/**
	 * Supprimer une rubrique
	 *
	 * Cette fonction permet de supprimer une rubrique
	 *
	 * @param int $id id de la rubrique à supprimer.
	 * @param bool $forcer la suppression de la rubrique.
	 * @return callback redirige vers la page de configuration.
	 */
	public function supprimer($id = null, $forcer = false) {
		$this->Session->write('active', 'gestion_des_parametres');

		//Vérification de l'existance de la rubrique
		$rubriqueCourante = $this->Rubrique->findById($id);
		if (count($rubriqueCourante) == 0) {
			$this->Session->setFlash(__('Aucune rubrique trouvé'), "failure");
			return $this->redirect(array('action' => 'configuration'));
		}

		if ($forcer == false) {
			if (count($rubriqueCourante['Atelier']) > 0) {
				$ateliers = $this->Rubrique->Atelier->find (
					'all',
					array ('conditions' => array('Rubrique.id' => $id))
				);
				$this->set(compact('ateliers'));
				$this->set(compact('rubriqueCourante'));
				return;
			}
		}

		if ($this->Rubrique->delete($id, true)) {
			$this->Session->setFlash(__('La rubrique %s a été supprimé.', h($rubriqueCourante['Rubrique']['nom'])), "success");
			return $this->redirect(array('action' => 'configuration'));
		}

		$this->Session->setFlash(__('La rubrique %s n\'a pas été supprimé.', h($rubriqueCourante['Rubrique']['nom'])), "failure");
		return $this->redirect(array('action' => 'configuration'));

	}

}