<?php

class Salle extends AppModel {

	public $validate = array(
		'nom' => array(
			'unique' => array(
				'rule' => 'isUnique',
				'message' => 'Ce nom de salle est déjà pris'
			),
			'vide' => array(
				'rule' => 'notEmpty',
				'message' => 'Le nom de la salle n\'est pas définie'
			),
		)
	);
}