<?php

App::uses('AppModel', 'Model');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');


class User extends AppModel {

	public $belongsTo = array('Division', 'Role');

	public $hasMany = array(
		'Creditation' => array(
			'className' => 'Creditation',
			'foreignKey'=>'user_id',
			'dependent'=>true),
		'Inscription' => array(
			'className' => 'Inscription',
			'foreignKey'=>'user_id',
			'dependent'=>true),
		'Actualite' => array(
			'className' => 'Actualite',
			'foreignKey'=>'user_id',
			'dependent'=>true),
		'Atelier' => array(
			'className' => 'Atelier',
			'foreignKey'=>'user_id',
			'dependent'=>true),
	);

	public $actsAs = array('Acl' => array('type' => 'requester', 'enabled' => false));

	public $validate = array(
		'civilite' => array(
			'vide' => array(
				'rule' => 'notEmpty',
				'message' => 'La civilité n\'est pas définie'
			),
		),
		'prenom' => array(
			'vide' => array(
				'rule' => 'notEmpty',
				'message' => 'Le prénom n\'est pas définie'
			),
		),
		'nom' => array(
			'vide' => array(
				'rule' => 'notEmpty',
				'message' => 'Le nom n\'est pas définie'
			),
		),
		'username' => array(
			'unique' => array(
				'rule' => 'isUnique',
				'message' => 'Cette identifiant d\'utilisateur a déjà été choisi'
			),
			'vide' => array(
				'rule' => 'notEmpty',
				'message' => 'L\'identifiant n\'est pas définie'
			),
		),
		'password' => array(
			'rule'    => array('minLength', 6),
			'message' => 'Le mot de passe doit comporter au moins 6 caractères'
		),
		'phone' => array(
			'rule'    => array('phone', '/((\d{2}.\d{2}.\d{2}.\d{2}.\d{2}),?)+/'),
			'message' => 'Le numéro de téléphone doit avoir le format 00.00.00.00.00'
		)
		//Faire Role et admin
	);

	public $cascade = true;

	public function beforeSave($options = array()) {
		if (isset($this->data[$this->alias]['password'])) {
			$passwordHasher = new SimplePasswordHasher();
			$this->data[$this->alias]['password'] = $passwordHasher->hash($this->data[$this->alias]['password']);
		}
		return true;
	}

	public function parentNode() {
		if (!$this->id && empty($this->data)) {
			return null;
		}
		if (isset($this->data['User']['role_id'])) {
			$roleId = $this->data['User']['role_id'];
		}
		else {
			$roleId = $this->field('role_id');
		}
		if (!$roleId) {
			return null;
		}
		return array('Role' => array('id' => $roleId));
	}

	public function bindNode($user) {
		return array('model' => 'Role', 'foreign_key' => $user['User']['role_id']);
	}

}
