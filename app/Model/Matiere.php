<?php

class Matiere extends AppModel {

	public $validate = array(
		'nom' => array(
			'unique' => array(
				'rule' => 'isUnique',
				'message' => 'Ce nom de matière est déjà pris'
			),
			'vide' => array(
				'rule' => 'notEmpty',
				'message' => 'Le nom de la matière n\'est pas définie'
			),
		)
	);
}