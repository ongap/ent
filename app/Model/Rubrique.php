<?php

App::uses('AppModel', 'Model');


class Rubrique extends AppModel {

	public $hasMany = array (
		'Atelier' => array(
			'className' => 'Atelier',
			'foreignKey'=>'rubrique_id',
			'dependent'=>true),
	);

	public $validate = array (
		'nom' => array(
			'rule'    => array('minLength', 3),
			'message' => 'Le nom de la rubrique doit comporter au moins 3 caractères'
		),
		'position' => array (
			'rule' => array('comparison', '>=', 1),
			'message' => 'La position doit être minimun à 1'
		),
		'label_gestion' => array (
			'rule' => 'notEmpty',
			'message' => 'Le label de gestion doit être renseigné'
		),
		'label_gestion_inscription' => array (
			'rule' => 'notEmpty',
			'message' => 'Le label de la gestion des inscriptions doit être renseigné'
		),
		'label_suivi' => array (
			'rule' => 'notEmpty',
			'message' => 'Le label de la gestion des suivis doit être renseigné'
		),
		'label_inscription' => array (
			'rule' => 'notEmpty',
			'message' => 'Le label de l\'inscription renseigné'
		),
		'label_recapitulation' => array (
			'rule' => 'notEmpty',
			'message' => 'Le label de la récapitulation doit être renseigné'
		)
	);

}

