<?php

App::uses('AppModel', 'Model');


class Periode extends AppModel {

	public $hasMany = array (
		'Atelier' => array(
			'className' => 'Atelier',
			'foreignKey'=>'periode_id',
			'dependent'=>true),
	);

	public $validate = array (
		'nom' => array (
			'minimun' => array (
				'rule'    => array ( 'minLength', 3 ),
				'message' => 'Le nom de la période doit comporter au moins 3 caractères'
			),
			'unique' => array (
				'rule'    => 'isUnique',
				'message' => 'Ce nom de période est déjà pris'
			),
		),
		'debut' => array (
			'format' => array ( 
				'rule' => 'date',
				'message' => 'La date de début n\'est pas valide',
				'allowEmpty' => false
			),
			'coherence' => array ( 
				'rule' => array ( 'coherenceDate'),
				'message' => 'La date de début doit être avant la date de fin',
			)
		),
		'fin' => array(
			'rule' => 'date',
			'message' => 'La date de fin n\'est pas valide',
			'allowEmpty' => false
		),
	);

	/**
	 * Vérifier que la date de fin est après la date de début
	 */
	public function coherenceDate($field){
		if ($field['debut'] > $this->data[$this->name]['fin']) {
			return false;
		}
		return true;
	}

	/**
	 * Pour que la date de fin soit considéré comme une journée compléte
	 */
	public function beforeSave($options = array()){
		$this->data[$this->name]['debut'] .= ' 00:00:00';
		$this->data[$this->name]['fin']   .= ' 23:59:59';
		return true;
	}

}

