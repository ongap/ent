<?php

class Division extends AppModel {
	public $hasMany = 'User';

	public $validate = array(
		'nom' => array(
			'unique' => array(
				'rule' => 'isUnique',
				'message' => 'Ce nom de classe est déjà pris'
			),
			'vide' => array(
				'rule' => 'notEmpty',
				'message' => 'Le nom de la classe n\'est pas définie'
			),
		)
	);
}
