<?php

class Horaire extends AppModel {

	/**
	 * @var Relation vers la table Atelier
	 */
	public $hasMany = 'Atelier';

	/**
	 * @var Régles de validation
	 */
	public $validate = array(
		'heureDebut' => array(
			'rule1' => array(
				'rule'    => 'notEmpty',
				'message' => 'Le début de la plage horaire doit être définie'
			),
			'rule2' => array(
				'rule'    => '/^([0-9]|0[0-9]|1[0-9]|2[0-3])(:|h)[0-5][0-9]$/',
				'message' => 'Le format de l\'heure doit être **h**'
			),
		),
		'heureFin' => array(
			'rule1' => array(
				'rule'    => 'notEmpty',
				'message' => 'La fin de la plage horaire doit être définie'
			),
			'rule2' => array(
				'rule'    => '/^([0-9]|0[0-9]|1[0-9]|2[0-3])(:|h)[0-5][0-9]$/',
				'message' => 'Le format de l\'heure doit être **h**',
				'last'    => true
			),
			'rule3' => array ( 
				'rule'    => array ('coherenceTime'),
				'message' => "L'heure de fin doit être après celle du début",
			)
		),
	);

	/**
	 * Verifier que l'heure de fin est après l'heure du début
	 */
	public function coherenceTime($field){
		if (preg_match_all('/^([0-9]|0[0-9]|1[0-9]|2[0-3])(:|h)[0-5][0-9]$/', $this->data[$this->name]['heureDebut'])) {
			$heureDebut = new DateTime(str_replace('h', ':', $this->data[$this->name]['heureDebut']));
			$heureFin   = new DateTime(str_replace('h', ':', $field['heureFin']));
			$interval   = $heureDebut->diff($heureFin);			
			if ($interval->format('%R') == '+') {
				return true;
			}
		}
		return false;
	}

	/**
	 * Changement du format de l'heure pour l'enregistrement en base
	 */
	public function beforeSave($options = array()){
		$this->data['Horaire']['heureDebut']  = str_replace('h', ':', $this->data['Horaire']['heureDebut']);
		$this->data['Horaire']['heureDebut'] .= ':00';
		$this->data['Horaire']['heureFin']    = str_replace('h', ':', $this->data['Horaire']['heureFin']);
		$this->data['Horaire']['heureFin']   .= ':00';

		return true;
	}
}
