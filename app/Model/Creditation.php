<?php

App::uses('AppModel', 'Model');


class Creditation extends AppModel {

	public $name = 'Creditation';

    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        ),
        'Atelier' => array(
            'className' => 'Atelier',
            'foreignKey' => 'atelier_id'
        )
    );

    public $validate = array(
        'valid' => array(
            'rule'    => 'notEmpty',
            'message' => 'La validation ne peut pas être vide'
        ),
        'commentaire' => array(
            'rule' => 'notEmpty',
            'message' => 'Vous devez écrire un commentaire'
        )
    );
}