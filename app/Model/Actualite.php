<?php

class Actualite extends AppModel {

	public $belongsTo = 'User';

	public $validate = array(
		'titre' => array(
			'vide' => array(
				'rule' => 'notEmpty',
				'message' => 'Le titre n\'est pas définie'
			),
		),
		'contenu' => array(
			'vide' => array(
				'rule' => 'notEmpty',
				'message' => 'Le contenu n\'est pas définie'
			),
		),
	);
}