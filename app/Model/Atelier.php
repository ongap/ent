<?php

App::uses('AppModel', 'Model');


class Atelier extends AppModel {

	public $actsAs = array(
	    'CsvExport' => array(
	        'delimiter'  => ';', //The delimiter for the values, default is ;
	        'enclosure' => '"', //The enclosure, default is "
	        'max_execution_time' => 360, //Increase for Models with lots of data, has no effect is php safemode is enabled.
	        'encoding' => 'utf8' //Prefixes the return file with a BOM and attempts to utf_encode() data
	    )       
	);

	public $belongsTo = array('User', 'Matiere', 'Salle', 'Horaire', 'Periode', 'Rubrique') ;

	public $hasMany = array(
		'Creditation' => array(
			'className' => 'Creditation',
			'foreignKey'=>'atelier_id',
			'dependent'=>true),
		'Inscription' => array(
			'className' => 'Inscription',
			'foreignKey'=>'atelier_id',
			'dependent'=>true),
		'Atelier' => array(
			'className' => 'Atelier',
			'foreignKey'=>'atelier_id',
			'dependent'=>true)
	);

    public $hasAndBelongsToMany = array(
        'Division' =>
            array(
                'className' => 'Division',
                'joinTable' => 'ateliers_divisions',
                'foreignKey' => 'atelier_id',
                'associationForeignKey' => 'division_id',
                'unique' => true
            ),
    );

	public $validate = array(
		'nom' => array(
			'rule'    => array('minLength', 3),
			'message' => 'Le nom de l\'atelier doit comporter au moins 3 caractères'
		),
		'description' => array(
			'rule' => 'notEmpty',
			'message' => 'Vous devez écrire une description'
		),
		'Division' => array(
			'rule' => array('multiple', array('min' => 1)),
			'message' => 'Vous devez choisir au moins une classe'
		),
		'jour' => array(
			'rule' => 'date',
			'message' => 'Entrez une date valide',
			'allowEmpty' => false
		),
		'nombre_place' => array(
			'rule' => array('comparison', '>=', 1),
			'message' => 'Vous devez avoir au moins 1 place.'
		),
		'jour_debut' => array(
			'rule' => 'notEmpty',
			'message' => 'La date de début doit être renseigné'
		),
		'jour_fin' => array(
			'rule' => 'notEmpty',
			'message' => 'La date de fin doit être renseigné'
		)
	);

	public function beforeSave($options = array()){
		foreach (array_keys($this->hasAndBelongsToMany) as $model){
			if(isset($this->data[$this->name][$model])){
				$this->data[$model][$model] = $this->data[$this->name][$model];
				unset($this->data[$this->name][$model]);
			}
		}
		return true;
	}

}

